package it.polimi.ingsw.DargenioDeSilva;

import static org.junit.Assert.*;


import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import exceptions.NotMapException;
import abstractObjects.Card;
import abstractObjects.Sector;



public class ModelTest {

	@Before
	public void testCreateMatch() {

	} 
	/**
	 * This method tests the limit case of the border of the map
	 * to be sure that sectors as A0 W15 are not included in the list
	 * of near sectors
	 * @throws NotMapException
	 */
	@Test
	public void testBorderSector() throws NotMapException{
		MapManager mg=MapManager.getInstance();
		MatchMap map= new MatchMap("Galilei");
		Sector s=map.get("A1");
		for (Sector temp: map.getNearSectors(s))
			assertFalse(temp.getX()=='A' && temp.getY()==0);
	} 

	@Test
	public void testNotIsNearSector() throws NotMapException{

		MatchMap map= new MatchMap("Galilei");

		Sector s=map.get("D8");
		assertFalse (map.getNearSectors(s).contains(map.get("D14")));
	} 


	@Test
	public void testCreateMap() throws NotMapException{
		MatchMap map= new MatchMap("Galilei"); 

		Sector s=map.get("A14"); 
		assertTrue(s.getX()=='A' && s.getY()==14); 	
	} 

	@Test
	public void testIsNearSectors() throws NotMapException{
		MatchMap map= new MatchMap("Galilei");

		Sector s=map.get("D8");
		assertTrue (map.getNearSectors(s).contains(map.get("D9")));

	} 
	/***********************Human and Alien move tests*******************************/
	/**
	 * This method tests if the method that checks a valid change of position
	 * returns the right result
	 * @throws NotMapException
	 */
	@Test
	public void testHumanValidMove() throws NotMapException{
		HumanMove hm=new HumanMove();
		MatchMap map=new MatchMap("Galilei"); 
		Sector s=map.get("F5");
		Sector destination=map.get("F6"); 
		assertTrue(hm.validMove(map, s, destination)); 
	}

	@Test
	public void testHumanInvalidMove() throws NotMapException{
		HumanMove hm=new HumanMove();
		MatchMap map=new MatchMap("Galilei"); 

		Sector s=map.get("F5");
		Sector destination=map.get("F7"); 
		assertFalse(hm.validMove(map, s, destination)); 
		assertFalse(hm.validMove(map, s, s)); 
	}


	@Test
	public void testAlienValidMove()throws NotMapException{
		AlienMove am=new AlienMove(); 
		MatchMap map=new MatchMap("Galilei"); 
		Sector s=map.get("N2");
		Sector destination=map.get("P2"); 
		assertTrue(am.validMove(map, s, destination)); 


	}

	@Test
	public void testAlienNotMovement()throws NotMapException{
		AlienMove am=new AlienMove(); 
		MatchMap map=new MatchMap("Galilei"); 
		Sector s=map.get("M5");
		Sector destination=map.get("M5"); 
		assertFalse(am.validMove(map, s, destination)); 

	}

	@Test
	public void testAlienInvalidMove()throws NotMapException{
		AlienMove am=new AlienMove(); 
		MatchMap map=new MatchMap("Galilei");  
		Sector s=map.get("C4");
		Sector destination=map.get("E5"); 
		Sector nearEscape=map.get("B1");
		Sector escapeHatch=map.get("B2");
		assertFalse(am.validMove(map, s, destination)); 
		assertFalse(am.validMove(map, s,s));
		assertFalse(am.validMove(map, nearEscape, escapeHatch));
	}
	/*********************end move tests****************************************/


	@Test
	public void testEscapeHatchMove() throws NotMapException {
		AlienMove am=new AlienMove(); 
		MatchMap map=new MatchMap("Galilei"); 
		Sector s=map.get("U2"); 
		Sector escapeHatch=map.get("V2"); 
		assertFalse(am.validMove(map, s, escapeHatch)); 

	}

	/**
	 * This method tests if the deck regenerates itself after
	 * that all the cards have been drawn.
	 */
	@Test
	public void testDeck(){
		Deck deck=DeckFactory.deckFactory();
		ArrayList<Card> cards=new ArrayList<Card>();
		for (int i=0; i<35; i++) 
			cards.add(deck.takeFromDeck()); 
		assertTrue(cards.get(30)!=null);
	}

	@Test
	public void testBlockedSector() throws NotMapException{
		MatchMap map=new MatchMap("Galilei"); 
		Sector s=map.get("O3"); 
		assertTrue(s.isBlocked());
	}

	@Test
	public void testStartSector() throws NotMapException{
		MapManager mapManager=MapManager.getInstance(); 
		MatchMap map=new MatchMap("Galilei"); 
		assertTrue(map.startSector("alien")instanceof AlienSector); 
		assertTrue(map.startSector("human")instanceof HumanSector); 

	}


	/**
	 * Tests if after a player drawn a card, it is possible too do noise
	 * in different sectors.
	 * @throws NotMapException
	 */
	@Test 
	public void testAnyCard() throws NotMapException{
		Card noiseAnyCard=new NoiseAnyCard(); 
		MatchMap map=new MatchMap("Galilei"); 
		Sector anySector=map.get("D3"); 
		Sector anySectorBlocked=map.get("W7"); 
		Sector mine=map.get("A4"); 
		assertTrue(noiseAnyCard.isValidNoise(mine, anySector));
		assertFalse(noiseAnyCard.isValidNoise(mine, anySectorBlocked)); 
	}

	@Test
	public void testYoursCard() throws NotMapException{
		Card noiseYoursCard=new NoiseYoursCard(); 
		MatchMap map=new MatchMap("Galilei"); 
		Sector mine=map.get("J4"); 
		Sector anySector=map.get("D3"); 	
		Sector yoursSector=map.get("J4");
		assertTrue(noiseYoursCard.isValidNoise(mine, yoursSector)); 
		assertFalse(noiseYoursCard.isValidNoise(mine, anySector)); 
	}

	@Test
	public void testSilenceCard() throws NotMapException{
		Card silenceCard=new SilenceCard(); 
		MatchMap map=new MatchMap("Galilei"); 
		Sector mine=map.get("J4"); 
		Sector anySector=map.get("D3"); 	
		assertFalse(silenceCard.isValidNoise(mine, anySector)); 

	}
	@Test 
	public void testIsValidSector(){

		assertFalse(Sector.isValid("A0")); 
		assertFalse(Sector.isValid("A15"));
		assertFalse(Sector.isValid("W34"));
		assertFalse(Sector.isValid("!?"));
		assertFalse(Sector.isValid("test"));
		assertTrue(Sector.isValid("A1")); 
		assertTrue(Sector.isValid("W14")); 

	}

}

	
	
	