package it.polimi.ingsw.DargenioDeSilva;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;



public class LoginTest {
	
	@BeforeClass
	public static void onlyOnce() throws RemoteException{
		
		Registry registry=LocateRegistry.createRegistry(ConfigurationClass.getPortRegistry());
		Login loginInstance;
		MatchManager mg=MatchManager.getInstance();
		MapManager mm=MapManager.getInstance(); 
		loginInstance=Login.getInstance(); 

		loginInstance.login("ernesto", "1234", "Galilei");
		loginInstance.login("fabio", "viriale", "Galilei");
		loginInstance.login("rinaldo", "rimini", "Galilei");
		loginInstance.login("ruggiero", "5678", "Galilei");
		loginInstance.login("leonardo", "palermo", "Galilei");
		loginInstance.login("tomas", "ragusa", "Galilei");
		loginInstance.login("antonio", "avellino", "Galilei");
		loginInstance.login("lucio", "lamezia", "Galilei");
	
		
		MatchController controller=mg.getMatchController(1);
		controller.start();
		
	}
	

	@Test
	public void testCreateMatch() {
		MatchManager mg=MatchManager.getInstance();
		assertTrue(mg.getMatchController(1)!=null);
	} 
	
	@Test
	public void testWrongLogin(){
		Login loginInstance=Login.getInstance(); 
		Map<String, String> loginResult=loginInstance.login("giulia", "passworderrata", "Galilei");
		assertTrue(loginResult.get("action").equalsIgnoreCase("wrongPassword"));
		loginResult=loginInstance.login("notExistentUser", "1234", "Galilei"); 
		assertTrue(loginResult.get("action").equalsIgnoreCase("notExistentUser")); 
		
	}
	
	
	@Test
	public void testSearchUser() throws RemoteException, IOException{
		Login loginInstance; 
		String [] data; 
		loginInstance=Login.getInstance(); 
		data=loginInstance.searchUser("ernesto");
		assertTrue(data[0].equals("ernesto")&&data[1].equals(loginInstance.getHashOf("1234")));
	} 

	@Test
	public void testInvalidUsername() throws IOException, RemoteException{
		Login loginInstance; 
		loginInstance=Login.getInstance();
		assertTrue(loginInstance.signUp("ernesto", "1234").equalsIgnoreCase("invalidUsername"));

	}	
	
	@Test
	public void testAlreadyLogged(){
		Login loginInstance; 
		loginInstance=Login.getInstance(); 
		String result=loginInstance.login("ernesto", "1234","Galielei").get("action"); 
		assertTrue(result.equalsIgnoreCase("alreadyLogged")); 
	}
	
	@Test
	public void testRanking(){
		Login loginInstance; 
		loginInstance=Login.getInstance(); 
		Map<String,String> rank=loginInstance.getRanking(); 
		int victoriesFirst=Integer.parseInt(rank.get("param0").split(",")[1]);  
		int victoriesSecond=Integer.parseInt(rank.get("param1").split(",")[1]);
		assertTrue(victoriesFirst>=victoriesSecond); 
	}
	
}

