package it.polimi.ingsw.DargenioDeSilva;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import abstractObjects.Sector;
import abstractObjects.StartFinishSector;
import exceptions.NotMapException;

public class GamingTest {
	static MatchController mc;
	@Before
	public void onlyOnce() {
		mc=new MatchController(1);
		mc.getMatch().setMap("Galilei");
		List<Player> players=createPlayers();
		for(Player p:players){
			Player.players.put(p.getUsername(), p);
			mc.getMatch().getPlayers().add(p);
		}
		
		mc.start();
		
	}

	

	private static List<Player>createPlayers(){
		List<Player> players=new ArrayList<Player>();
		String data[]=new String[5];
		data[0]="ernesto";
		data[2]="0";
		data[3]="0";
		data[4]="0";
		
		players.add(new Player(data));
		
		data[0]="ruggiero";
		players.add(new Player(data));
		
		data[0]="fabio";
		players.add(new Player(data));
		
		data[0]="lucio";
		players.add(new Player(data));
		
		data[0]="francesca";
		players.add(new Player(data));
		
		data[0]="giulia";
		players.add(new Player(data));
		
		data[0]="leonardo";
		players.add(new Player(data));
		
		data[0]="tomas";
		players.add(new Player(data));
	
		return players;
	}
	
	@Test 
	public void testIsMyPlayer(){
		Player playerOfMatch=mc.getCurrentPlayer(); 
		assertTrue(mc.getMatch().isMyPlayer(playerOfMatch.getUsername())); 
	}
	
	@Test 
	public void getSectorOf(){ 
		List<Player> players=mc.getMatch().getPlayers();
		Turn turn=new Turn(players);
		assertTrue( turn.getSectorOf(players.get(0)) instanceof StartFinishSector); 
	}
	
	@Test 
	public void testGiveMeCharacter(){
		Player p=mc.getCurrentPlayer();
		String character=mc.giveMeCharacter(p.getUsername()); 
		assertTrue(character.equalsIgnoreCase("Alien")||character.equalsIgnoreCase("Human")); 
	}
	
	/**
	 * When the timeout expires, if a setPosition has been called, the position is restored to the previous one
	 * This method tests this situation.
	 * @throws NotMapException
	 */
	@Test
	public void testReverseTurn() throws NotMapException{
		MatchMap map=new MatchMap("Galilei"); 
		Sector s=map.get("L5"); 
		while (!(mc.getCurrentPlayer().getCharacter() instanceof Alien)){
			Action action=new ActionChangeTurn(mc, mc.getCurrentPlayer().getUsername());
			action.execute();
		}
		Player p=mc.getCurrentPlayer();
		mc.getCurrentPlayer().getCharacter().setPosition(map, s); 
		mc.reverseTurn();
		assertTrue(p.getCharacter().getSector() instanceof AlienSector); 
	}
	
	/**
	 * This method tests that the the currentPlayer of a match changes after the changeTurn method.
	 */
	@Test
	public void testNextTurn(){
		Player oldPlayer=mc.getCurrentPlayer();
		Action action=new ActionChangeTurn(mc, mc.getCurrentPlayer().getUsername());
		action.execute();
		assertFalse(mc.changeTurn(oldPlayer.getUsername())); //it isn't his turn
		Action change=new ActionChangeTurn(mc, mc.getCurrentPlayer().getUsername());
		change.execute();
		Player currentPlayer=mc.getCurrentPlayer();
		assertFalse(oldPlayer.getUsername().equals(currentPlayer.getUsername()));
	}
	
	@Test
	public void testActionChangeTurn(){
		Player oldPlayer=mc.getCurrentPlayer(); 
		mc.changeTurn(oldPlayer.getUsername());
		Action actionChangeTurn=new ActionChangeTurn(mc, oldPlayer.getUsername()); 
		assertFalse(actionChangeTurn.check());
	}
	
	/**
	 * A noise can be sent only if the player is in a dangerous sector.
	 * This method tests that a noise can't be sent if the player is in a secure sector.
	 */
	@Test
	public void testActionNoise(){
		Player currentPlayer=mc.getCurrentPlayer(); 
		Sector secureSector=mc.getMatch().getMap().get("A4");
		Action actionNoiseFalse=new ActionNoise(mc, currentPlayer.getUsername(), secureSector.toString());
		assertFalse(actionNoiseFalse.check());
		assertTrue(actionNoiseFalse.execute().equalsIgnoreCase("false"));
		
	}
	
	/**
	 * This method tests that only an alien can attack. 
	 * @throws IOException
	 */
	@Test
	public void testAttack() throws IOException {

		if (mc.getCurrentPlayer().getCharacter() instanceof Human) {
			assertFalse(mc.attack(mc.getCurrentPlayer().getUsername())); 
			
			while(mc.getCurrentPlayer().getCharacter() instanceof Human){
				Action action=new ActionChangeTurn(mc, mc.getCurrentPlayer().getUsername());
				action.execute();
			}
		}
		assertTrue(mc.attack(mc.getCurrentPlayer().getUsername())); 
	

	}
	
	
	@Test
	public void testSetPosition(){
	
		if(mc.getCurrentPlayer().getCharacter() instanceof Human){
			assertTrue(mc.setPosition(mc.getCurrentPlayer().getUsername(), "K8")); 
			assertFalse(mc.setPosition(mc.getCurrentPlayer().getUsername(), "L5"));
		}	
		else {
			assertTrue(mc.setPosition(mc.getCurrentPlayer().getUsername(), "L5")); 
			assertFalse(mc.setPosition(mc.getCurrentPlayer().getUsername(), "K8")); 
		}
			
	}
	
	/**
	 * This method tests that a player can draw a card only if is a dangerous sector.
	 * The drawCard method can return 3 types of card. THis method tests if a noise is valid
	 * according to the drawn card.
	 * @throws NotMapException
	 * @throws RemoteException
	 */
	@Test
	public void testDrawCard() throws NotMapException, RemoteException {
		MatchMap map=mc.getMatch().getMap();
		Sector s=map.get("K8"); //dangerous sector
		
		while (!(mc.getCurrentPlayer().getCharacter() instanceof Human)){
			Action action=new ActionChangeTurn(mc, mc.getCurrentPlayer().getUsername());
			action.execute();
		} 
		
		String usernameCurrentPlayer=mc.getCurrentPlayer().getUsername(); 
		mc.setPosition(usernameCurrentPlayer, s.toString());	
		String resultCard=mc.drawCard(usernameCurrentPlayer);
		assertTrue(resultCard.equalsIgnoreCase("noiseYoursCard")
				||resultCard.equalsIgnoreCase("noiseAnyCard")
				||resultCard.equalsIgnoreCase("silenceCard"));
		
		if (resultCard.equalsIgnoreCase("noiseYoursCard")) {
				assertTrue(mc.noise(usernameCurrentPlayer, "K8"));
				assertFalse(mc.noise(usernameCurrentPlayer, "K9")); 
			}
		if (resultCard.equalsIgnoreCase("noiseAnyCard")) {
			assertTrue(mc.noise(usernameCurrentPlayer, "Q8"));
		}
		if (resultCard.equalsIgnoreCase("silenceCard")) {
			assertFalse(mc.noise(usernameCurrentPlayer, "K8"));
			assertFalse(mc.noise(usernameCurrentPlayer, "Q8"));
			String newPlayer=mc.getCurrentPlayer().getUsername();
			assertFalse(newPlayer.equalsIgnoreCase(usernameCurrentPlayer)); // drawn silence card => change turn
		}
		
		//draw in secure sector, check=false; 
		while (!(mc.getCurrentPlayer().getCharacter() instanceof Human)){
			Action action=new ActionChangeTurn(mc, mc.getCurrentPlayer().getUsername());
			action.execute();
		} 
		mc.setPosition(mc.getCurrentPlayer().getUsername(), map.get("K9").toString());
		ActionDrawCard drawInSecureSector=new ActionDrawCard(mc, mc.getCurrentPlayer().getUsername()); 
		assertFalse(drawInSecureSector.check());
		assertFalse(Boolean.parseBoolean(drawInSecureSector.execute())); 
	}
		
		
	@Test
	public void testWin() throws NotMapException{
		
		MatchMap map=mc.getMatch().getMap();
		Sector start=map.get("U2"); 
		Sector escapeHatch=map.get("V2"); 
		
		while (!(mc.getCurrentPlayer().getCharacter() instanceof Human)){
			Action action=new ActionChangeTurn(mc, mc.getCurrentPlayer().getUsername());
			action.execute();
		}
		
		mc.getCurrentPlayer().getCharacter().setSector(start);
		mc.setPosition(mc.getCurrentPlayer().getUsername(), escapeHatch.toString());
		assertTrue(mc.getMatch().getWinners().contains(mc.getCurrentPlayer()));
		}
	
	
	@Test
	public void testCheckEndGame(){
		Player p=mc.getCurrentPlayer();
		while (mc.getMatch().getNumberPlayers()!=1){
				mc.delPlayer(mc.getCurrentPlayer());
				Action action=new ActionChangeTurn(mc, p.getUsername());
				action.execute();
		}
		assertTrue(mc.checkEndGame()); 
	}
	

	@After
	public void after(){
		mc.matchTerminated();
	}
}