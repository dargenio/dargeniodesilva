package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import Server.networking.rmi.RemoteLogin;
import Server.networking.socket.SocketServer;
import it.polimi.ingsw.DargenioDeSilva.ConfigurationClass;
import it.polimi.ingsw.DargenioDeSilva.Login;
import it.polimi.ingsw.DargenioDeSilva.MapManager;
import it.polimi.ingsw.DargenioDeSilva.MatchManager;

/**
 * @author Ruggiero Dargenio
 * This is the main thread of the server
 *
 */
public class GameServer extends Thread{
	private int registryPort=ConfigurationClass.getPortRegistry();
	private static final Logger log=Logger.getLogger("log");
	
	
	
	public static void main(String[] args) throws IOException,NotBoundException{

			GameServer server=new GameServer();
			server.start();
		
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 * 
	 * Create all it is needed for the RMI and the socket communication
	 */
	@Override
	public void run(){
		log.setLevel(Level.FINEST);
		Registry registry = null;
		
		
		String name = "Connect";
		
		       
        try {
            
        	RemoteLogin login=Login.getInstance();
        	RemoteLogin stub= (RemoteLogin) UnicastRemoteObject.exportObject(login,0);
             
            registry = LocateRegistry.createRegistry(registryPort);            
            registry.bind(name, stub);
            
    		
            System.out.println("RMI registry created");
            
        } catch (RemoteException e) {
            log.log(Level.SEVERE,"Error in creating RMI registry");
            
        }catch(AlreadyBoundException ABe){
        	log.log(Level.SEVERE, "AlreadyBpundException. Message:"+ABe.getMessage());
        }
		
		//Starting socket server
		SocketServer server = new SocketServer();
		System.out.println("Starting the socket server...");
		Thread t = new Thread(server);
		t.start();
		System.out.println("Server started. Status: " + server.getStatus()
				+ ". Port: " + server.getPort());
		
		MatchManager matchManager=MatchManager.getInstance();
		MapManager mapManager=MapManager.getInstance();
		boolean finish = false;

		while (!finish) {
			String read = readLine("Press Q to exit\n");
			if (read.equals("Q")) {
				finish = true;
			}
		}
		
		try{
			t.interrupt();
			server.endListening();
			if (registry != null)
				registry.unbind(name);
		}catch(IOException IOe){
			log.log(Level.SEVERE,"Error in end listening"+IOe.getStackTrace());
		}catch(NotBoundException NBe){
			log.log(Level.SEVERE,"Error in end listening"+NBe.getStackTrace());
		}
	}
	
	private static String readLine(String format, Object... args){
		if (System.console() != null) {
			return System.console().readLine(format, args);
		}
		System.out.print(String.format(format, args));

		BufferedReader br = null;
		InputStreamReader isr = null;
		String read = null;

		isr = new InputStreamReader(System.in);
		br = new BufferedReader(isr);
		try{
			read = br.readLine();
		}catch(IOException IOe){
			System.err.println("Not possible to read line");
		}

		return read;
	}
}