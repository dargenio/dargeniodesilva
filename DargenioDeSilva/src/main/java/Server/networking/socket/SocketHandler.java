package Server.networking.socket;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import Server.networking.socket.commands.Command;
import Server.networking.socket.commands.CommandFactory;
import exceptions.NotCommandException;
import exceptions.NotValidCommandFormatException;

public class SocketHandler extends Thread{
	private Socket socket;

	private ObjectOutputStream oos;
	private ObjectInputStream ios;
	private boolean stop;
	private Logger log=Logger.getLogger("log");

	public SocketHandler(Socket socket) {
		super();
		stop = false;
		this.socket = socket;
		try{
			
			oos=new ObjectOutputStream(socket.getOutputStream());
			oos.flush();
			ios=new ObjectInputStream(socket.getInputStream());
			
		
		}catch(IOException ex){
			log.log(Level.SEVERE, "Error in openining input and output stream");
		}
	}
			
		
	

	public Socket getSocket() {
		return socket;
	}
	
	@Override
	public void run() {

		Map<String,String> toSend;
		while(!stop){
			try{
				@SuppressWarnings("unchecked")
				Map<String,String> input=(Map<String,String>)ios.readObject();
				
				try{
					Command command=CommandFactory.commandFactory(input.get("action"));
					toSend=command.action(input);
					if(toSend!=null){
						oos.writeObject(toSend);
						oos.flush();
					}
				}catch(NotCommandException NCe){
					log.log(Level.SEVERE,NCe.getMessage());
				}catch(NotValidCommandFormatException NVCFe){
					log.log(Level.SEVERE, NVCFe.getMessage());
				}



			}catch(IOException ex){
				
				log.log(Level.WARNING, "SocketHandler: Connection closed by a client");
				closeHandler();
				
				stop=true;
			}catch(ClassNotFoundException NCex){
				log.log(Level.SEVERE,"ClassNotFoundException Message:"+NCex.getMessage());
			}
		}
	}



	public void closeHandler() {
		stop = true;
		try{
		
			ios.close();
			oos.close();
			socket.close();
		}catch(IOException IOe){
			log.log(Level.SEVERE, "Error in closing socket handler");
		}
	}
	
	
}

