package Server.networking.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ruggiero Dargenio
 * Class that implements a SocketServer.
 * The server accepts incoming connection, instantiates and starts
 * a LoginHandler for each connection.
 */
public class SocketServer implements Runnable {

	private int port;
	private String address;
	private ServerSocket server;
	private boolean listening;
	private String status; 
	private List<SocketHandler> handlers;
	private Logger log=Logger.getLogger("log");
	
	/**
	 * Default constructor.
	 * The address is set to localhost; the port is set to 4000
	 */
	public SocketServer() {
		port = 4000;
		address = "127.0.0.1";
		listening = false;
		status = "Created";
		handlers = new LinkedList<SocketHandler>();
	}
	
	/**
	 * Field constructor
	 * @param port on which the server must listen
	 * @param address the address at which the server is reachable
	 */
	public SocketServer(int port, String address) {
		super();
		this.port = port;
		this.address = address;
		listening = false;
		status = "Created";
		handlers = new LinkedList<SocketHandler>();
	}
	
	public String getStatus() {
		return status;
	}

	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	/**
	 * Method that put the server in a Listening state, 
	 * preparing it to accept incoming connections.
	 * @throws IOException if the server cannot be instantiated
	 */
	public void startListening() throws IOException {
		if(!listening){
			
			//Creating the server
			server = new ServerSocket(port);
			status = "Listening...";
			listening = true;
			
			while(listening){
				//Listening loop
				try{
					Socket s = server.accept();
					//a connection has been accepted
					SocketHandler sh = new SocketHandler(s);
					handlers.add(sh);
					//Start the socket handler
					sh.start();
				} catch (IOException ex){ 
					log.log(Level.SEVERE, "Error in starting the listener (server.accept), socket clients cannot be handled");
				}
			}
		}
	}
	
	/**
	 * Method that stops the server from listening.
	 * It closes all the open sockets
	 * @throws IOException if some socket cannot be closed.
	 */
	public void endListening() throws IOException{
		if(listening){
			listening = false;
			for(SocketHandler sh : handlers)
				sh.closeHandler();			
			
			server.close();
			status = "Closed";
		}
	}

	@Override
	public void run() {
		try{
			startListening();		
		} catch (IOException ex){
			status = "Error in starting the listening: "+ex.getMessage();
		}
	}
}

