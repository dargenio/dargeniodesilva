package Server.networking.socket.commands;

import it.polimi.ingsw.DargenioDeSilva.MatchController;

import java.util.HashMap;
import java.util.Map;

import exceptions.NotValidCommandFormatException;

/**
 * @author Ruggiero Dargenio
 * This class is called when a player with a socket client asks to finish his turn.
 */
public class ChangeTurn extends Command {
	/* (non-Javadoc)
	 * @see Server.networking.socket.commands.Command#action(java.util.Map)
	 * Map requires key "username"
	 * 				key "ID"
	 */
	@Override
	public Map<String, String> action(Map<String, String> param)
			throws NotValidCommandFormatException {
		if(param.get("username")==null || param.get("ID")==null)
			throw new NotValidCommandFormatException();
		
		Map<String,String> result=new HashMap<String,String>();

		MatchController matchController=correctAssociation(Integer.parseInt(param.get("ID")),param.get("username"));

		if(matchController==null || !matchController.changeTurn(param.get("username"))){
			result.put("result", "false");
			return result;
		}
		else{
			result.put("result", "true");
			return result;
		}


	}

}
