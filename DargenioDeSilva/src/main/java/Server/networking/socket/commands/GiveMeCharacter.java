package Server.networking.socket.commands;

import it.polimi.ingsw.DargenioDeSilva.MatchController;

import java.util.HashMap;
import java.util.Map;

import exceptions.NotValidCommandFormatException;

/**
 * @author Ruggiero Dargenio
 * This class is called when a player with a socket client asks for the type of the character.
 * A client must ask for it or it will be not able to take part of the match correctly.
 *
 */
public class GiveMeCharacter extends Command{

	/* (non-Javadoc)
	 * @see Server.networking.socket.commands.Command#action(java.util.Map)
	 * Map requires key "username"
	 * 				key "ID"
	 */
	@Override
	public Map<String, String> action(Map<String, String> param)
			throws NotValidCommandFormatException {
	
		
		MatchController matchController=correctAssociation(Integer.parseInt(param.get("ID")),param.get("username"));
		String character=matchController.giveMeCharacter(param.get("username"));
		Map<String,String> result=new HashMap<String,String>();
		result.put("result", character);
		return result;
	}

}
