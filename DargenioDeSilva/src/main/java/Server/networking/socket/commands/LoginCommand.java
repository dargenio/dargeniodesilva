package Server.networking.socket.commands;

import it.polimi.ingsw.DargenioDeSilva.Login;

import java.util.Map;

import exceptions.NotValidCommandFormatException;

/**
 * @author Ruggiero Dargenio
 * This class is called when a player asks to login.
 *
 */
public class LoginCommand extends Command {
	/* (non-Javadoc)
	 * @see Server.networking.socket.commands.Command#action(java.util.Map)
	 * Map requires key "username"
	 * 				key "password"
	 * 				key "preferredMap"
	 */
	@Override
	public Map<String,String> action(Map<String,String> param)
					throws NotValidCommandFormatException {
		if (param.get("username")==null || param.get("password")==null || param.get("preferredMap")==null)
			throw new NotValidCommandFormatException();
		
		Login login=Login.getInstance();

		Map<String,String> result=login.login(param.get("username"), param.get("password"), param.get("preferredMap"));
		
		
		return result;


	}
}
