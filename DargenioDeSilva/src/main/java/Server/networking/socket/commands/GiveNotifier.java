package Server.networking.socket.commands;

import it.polimi.ingsw.DargenioDeSilva.Login;

import java.util.HashMap;
import java.util.Map;

import exceptions.NotValidCommandFormatException;
/**
 * @author Ruggiero Dargenio
 * This class is called when a player with a socket client asks for a Notifier.
 * A client must ask for it or it will be not able to take part of the match correctly.
 *
 */
public class GiveNotifier extends Command {
	/* (non-Javadoc)
	 * @see Server.networking.socket.commands.Command#action(java.util.Map)
	 * Map requires key "username"
	 * 				key "address"
	 */
	@Override
	public Map<String, String> action(Map<String,String> param)
			throws NotValidCommandFormatException {
		if(param.get("username")==null || param.get("address")==null)
			throw new NotValidCommandFormatException();
		
		Login login=Login.getInstance();
		Map<String,String> result=new HashMap<String,String>();
		
		login.linkNotifier(param.get("address"),param.get("username"), "socket");
		
		result.put("result", "true");
		
		return null;
		
		
	}

}
