package Server.networking.socket.commands;

import it.polimi.ingsw.DargenioDeSilva.MatchController;
import it.polimi.ingsw.DargenioDeSilva.MatchManager;

import java.util.Map;

import exceptions.NotValidCommandFormatException;

/**
 * @author Ruggiero Dargenio
 * This is the abstract class for the command. 
 * All the socket requests from clients are interpreted as commands
 * This class has the common methods and force its subclasses to implement
 * a method action related to the specific action of the command.
 *
 */
public abstract class Command {

	
	/**
	 * This method performs the action related to the command
	 * @param param Map<String,String> with all the fields of the command
	 * @return a Map<String,String> with the result
	 * @throws NotValidCommandFormatException if the Map<String,String> does not have
	 * 			all the needed fields
	 */
	public abstract Map<String,String> action(Map<String,String> param)
			throws NotValidCommandFormatException;
	
	/**
	 * This method checks if the player with the username passed as parameter
	 * is playing in the match with ID id
	 * @param ID of the match
	 * @param username of the player
	 * @return the matchController if the parameters are correct, null otherwise
	 */
	public MatchController correctAssociation(int ID, String username){
		MatchManager mg=MatchManager.getInstance();
		MatchController mc =mg.getMatchController(ID);
		if (mc.getMatch().isMyPlayer(username))
			return mc;
		else return null;
	}
}
