package Server.networking.socket.commands;

import it.polimi.ingsw.DargenioDeSilva.MatchController;

import java.util.HashMap;
import java.util.Map;

import exceptions.NotValidCommandFormatException;

/**
 * @author Ruggiero Dargenio
 *  This class is called when a player with a socket client asks to change position
 *
 */
public class PositionCommand extends Command {
	/* (non-Javadoc)
	 * @see Server.networking.socket.commands.Command#action(java.util.Map)
	 * Map requires key "username"
	 * 				key "ID"
	 * 				key "sector"
	 */
	@Override
	public Map<String,String> action(Map<String,String> param) throws NotValidCommandFormatException{
		
		if(param.get("username")==null || param.get("ID")==null|| param.get("sector")==null)
			throw new NotValidCommandFormatException();
		
		MatchController matchController=correctAssociation(Integer.parseInt(param.get("ID")),param.get("username"));
		
		Map<String,String> result=new HashMap<String,String>();
		
		if (matchController!=null){
			if (param.get("sector")!=null){

				
				if(matchController.setPosition(param.get("username"),param.get("sector")))
					result.put("result", "true");
				else
					result.put("result", "false");

			}
			else
				throw new NotValidCommandFormatException();

		}else
			result.put("result","false");
		
		
		return result;
	}
	

}
