package Server.networking.socket.commands;

import it.polimi.ingsw.DargenioDeSilva.MatchController;
import java.util.HashMap;
import java.util.Map;

import exceptions.NotValidCommandFormatException;

/**
 * @author Ruggiero Dargenio
 * This class is called when a player with a socket client wants to attack.
 */
public class AttackCommand extends Command {

	/* (non-Javadoc)
	 * @see Server.networking.socket.commands.Command#action(java.util.Map)
	 * Map requires key "username"
	 * 				key "ID"
	 */
	@Override
	public Map<String,String> action(Map<String,String> param) throws NotValidCommandFormatException {
		if(param.get("username")==null || param.get("ID")==null)
			throw new NotValidCommandFormatException();
		
		MatchController matchController=correctAssociation(Integer.parseInt(param.get("ID")),param.get("username"));
		Map<String,String> result=new HashMap<String,String>();
		
		if(matchController==null){
			result.put("result", "false");
			return result;
		}
		
		
		if(matchController.attack(param.get("username")))
			result.put("result", "true");
		else
			result.put("result", "false");
		
		return result;
		
		
	}

}
