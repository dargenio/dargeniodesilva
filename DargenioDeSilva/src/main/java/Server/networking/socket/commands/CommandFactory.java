package Server.networking.socket.commands;

import exceptions.NotCommandException;

/**
 * @author Ruggiero Dargenio
 * Factory for the command. Accepted commands:
 * setPostion
 * drawCard
 * attack
 * login
 * signUp
 * showScores
 * giveMeNotifier
 * changeTurn
 * typeCharacter
 * noise
 *
 */
public class CommandFactory {
	/**
	 * Accepted commands:
	 * setPostion
	 * drawCard
	 * attack
	 * login
	 * signUp
	 * showScores
	 * giveMeNotifier
	 * changeTurn
	 * typeCharacter
	 * noise
	 * @param command you want to create
	 * @return the created command
	 * @throws NotCommandException if the command is not one of the allowed one
	 */
	public static Command commandFactory(String command)throws NotCommandException{

		switch(command){
		case("setPosition"):
			return new PositionCommand();

		case("drawCard"):
			return new DrawCardCommand();

		case("attack"):
			return new AttackCommand();

		case("login"):
			return new LoginCommand();

		case("signUp"):
			return new SignUpCommand();

		case ("showScores"):
			return new RankingCommand();

		case("giveMeNotifier"):
			return new GiveNotifier();

		case("changeTurn"):
			return new ChangeTurn();

		case("typeCharacter"):
			return new GiveMeCharacter();

		case("noise"):
			return new NoiseCommand();


		}
		throw new NotCommandException(command);

	}
}






