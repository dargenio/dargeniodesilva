package Server.networking.socket.commands;

import it.polimi.ingsw.DargenioDeSilva.Login;

import java.util.Map;

import exceptions.NotValidCommandFormatException;

/**
 * @author Ruggiero Dargenio
 * This class is called when a player asks to see the global ranking.
 */
public class RankingCommand extends Command {

	/* (non-Javadoc)
	 * @see Server.networking.socket.commands.Command#action(java.util.Map)
	 */
	@Override
	public Map<String, String> action(Map<String, String> param)
			throws NotValidCommandFormatException {
		
		Login login=Login.getInstance();
		return login.getRanking();
		
	}

}
