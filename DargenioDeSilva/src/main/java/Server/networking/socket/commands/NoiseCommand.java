package Server.networking.socket.commands;

import it.polimi.ingsw.DargenioDeSilva.MatchController;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import exceptions.NotValidCommandFormatException;

/**
 * @author Ruggiero Dargenio
 * This class is called when a player with a socket client asks to do noise in a sector.
 */
public class NoiseCommand extends Command {
	private static final Logger log=Logger.getLogger("log");
	/* (non-Javadoc)
	 * @see Server.networking.socket.commands.Command#action(java.util.Map)
	 * Map requires key "username"
	 * 				key "ID"
	 * 				key "sector"
	 */
	@Override
	public Map<String,String> action(Map<String,String> param)
			throws NotValidCommandFormatException {
		
		
		if(param.get("username")==null || param.get("ID")==null|| param.get("sector")==null)
			throw new NotValidCommandFormatException();
		
		MatchController matchController=correctAssociation(Integer.parseInt(param.get("ID")),param.get("username"));
		
		Map<String,String> result=new HashMap<String,String>();
		try{
			if(matchController.noise(param.get("username"), param.get("sector")))
				result.put("result", "true");
				
			else	
				result.put("result", "false");
		}catch(RemoteException ex){
			log.log(Level.SEVERE,ex.getMessage());
			result.put("result", "false");
		}
		
		return result;
		
		
	}

}
