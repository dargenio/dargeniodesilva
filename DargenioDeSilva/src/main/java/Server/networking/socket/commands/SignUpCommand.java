package Server.networking.socket.commands;

import it.polimi.ingsw.DargenioDeSilva.Login;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import exceptions.NotValidCommandFormatException;

/**
 * @author Ruggiero Dargenio
 * This class is called when a new player asks to sign up.
 *
 */
public class SignUpCommand extends Command{
	private static final Logger log =Logger.getLogger("log");
	/* (non-Javadoc)
	 * @see Server.networking.socket.commands.Command#action(java.util.Map)
	 * Map requires key "username"
	 * 				key "password"
	 */
	@Override
	public Map<String,String> action(Map<String,String> param)
			throws NotValidCommandFormatException {
		
		Login login=Login.getInstance();
		Map<String,String> result=new HashMap<String,String>();
		try{
			result.put("result", login.signUp(param.get("username"), param.get("password")));
			return result;
		}catch(RemoteException ex){
			log.log(Level.SEVERE, "Remote exception in sign up command:"+ex.getMessage());
			result.put("result", "invalidUsername");
			return result;
		}
		
		
	}

}
