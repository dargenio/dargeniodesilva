package Server.networking.notifier;


import it.polimi.ingsw.DargenioDeSilva.MatchController;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

import exceptions.DisconnectedUserException;

/**
 * @author Ruggiero Dargenio
 * Socket implementation for the notifier
 *
 */
public class ServerToClientSocket extends ServerToClientNotifier{
	private int port;
	private Socket s;
	private String username;
	private ObjectOutputStream oos;
	private String address="127.0.0.1";
	private Logger log=Logger.getLogger("logger");


	/**
	 * Constructor
	 * @param address of the client
	 * @param port for the notifier
	 * @param username of the player
	 */
	public ServerToClientSocket(String address,int port,String username){
		this.port=port;
		this.username=username;
		this.address=address;
	}	


	/* (non-Javadoc)
	 * @see Server.networking.notifier.ServerToClientNotifier#startConnection()
	 */
	@Override
	public synchronized void startConnection(){
		try{
			connect();
		}catch(IOException IOe){
			log.log(Level.SEVERE, IOe.getMessage());
		}
		
	}



	private boolean connect() throws IOException  {
		boolean go=false;
		while(!go){
			try {
				//Create a socket and connect it to the server.
				s = new Socket(address, port);
				go=true;
			} catch (UnknownHostException e) {
				log.log(Level.WARNING, "Socket notifier tried to connect to "+username+" but it was not successfull "+e.getMessage());
				return false;
			} catch (IOException e) {
				try{
					Thread.sleep(1000);
				}catch(InterruptedException ex){
					log.log(Level.WARNING, "Socket notifier tried to connect to "+username+" but it was not successfull "+ex.getMessage());
				}
				log.log(Level.WARNING, "Failed to create socket toward the client");
			}
		}

		try {
			//Get the output stream to write to the client.
			oos=new ObjectOutputStream(s.getOutputStream());
			oos.flush();


		} catch (IOException e) {			
			log.log(Level.SEVERE, "Not possible to open ObjectPutputStream for "+username);
			s.close();
			return false;
		}


		return true;
	}

	/* (non-Javadoc)
	 * @see Server.networking.notifier.ServerToClientNotifier#closeNotifier()
	 */
	@Override
	public void closeNotifier(){
		try{
			/*Map<String,String> close=new HashMap<String,String>();
			close.put("action", "closeConnection");
			this.update(null,close);*/
			oos.close();
			s.close();
		}catch(IOException ex){
			log.log(Level.FINE, "Error in closing socket notifier: user "+username+" or notifier already closed");
		}
	}


	/* (non-Javadoc)
	 * @see Server.networking.notifier.ServerToClientNotifier#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public synchronized void update(Observable match, Object observerParams) throws DisconnectedUserException{
		@SuppressWarnings("unchecked")
		Map<String,String> params=(Map<String,String>)observerParams;
		try{
			//Write the map with what it is happened in the socket
			oos.writeObject (params);
			oos.flush();
		}catch(IOException ex){
			log.log(Level.WARNING, "Client "+username+" disconnected, connection with it has been closed");
			MatchController matchController=(MatchController)match;
			matchController.disconnectUser(username);
			closeNotifier();
			throw new DisconnectedUserException(username);

			//log.log(Level.WARNING, "Client "+username+" disconnected, connection with it has been closed");

		}

	}
}
