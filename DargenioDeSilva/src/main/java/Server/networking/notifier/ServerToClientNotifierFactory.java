package Server.networking.notifier;

import exceptions.NotNetworkInterfaceException;

/**
 * @author Ruggiero Dargenio
 * Create the correct notifier depending on if the player chose the
 * socket or the RMI communication.
 *
 */
public class ServerToClientNotifierFactory {
	
	/**
	 * Return a socket or RMI notifier
	 * @param address of the client
	 * @param networkInterface chosen by the client: "socket" or "RMI"
	 * @param port for the communication towards the client
	 * @param username of the player
	 * @return the notifier
	 * @throws NotNetworkInterfaceException if the networkInterface is different from "RMI" or "socket"
	 */
	public static ServerToClientNotifier getNotifier(String address,String networkInterface,int port,String username)throws NotNetworkInterfaceException{
		switch(networkInterface){
			case "socket":
				return new ServerToClientSocket(address,port,username);
			case "RMI":
				return new ServerToClientRMI(address,port,username);
		}
		
		throw new NotNetworkInterfaceException();
	}
}
