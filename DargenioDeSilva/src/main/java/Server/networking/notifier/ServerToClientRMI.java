package Server.networking.notifier;

import it.polimi.ingsw.DargenioDeSilva.MatchController;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

import exceptions.DisconnectedUserException;
import Client.networking.rmi.RemoteNotifier;

/**
 * @author Ruggiero Dargenio
 * RMI implementation for the notifier
 *
 */
public class ServerToClientRMI extends ServerToClientNotifier {
	private int port;
	private String address;
	private String username;
	private RemoteNotifier notifier;
	private Logger log=Logger.getLogger("log");
	
	/**
	 * Constructor
	 * @param address of the client
	 * @param port for the notifier
	 * @param username of the player
	 */
	public ServerToClientRMI(String address,int port,String username){
		this.port=port;
		this.username=username;
		this.address=address;
	}
	

	/* (non-Javadoc)
	 * @see Server.networking.notifier.ServerToClientNotifier#startConnection()
	 */
	@Override
	public synchronized void startConnection() {
		String name = "Notifier";
		Registry registry=foundRegistry();
        try {
        	//Get the oven reference
			notifier = (RemoteNotifier) registry.lookup(name);
			//Register the client to the server.
		} catch (AccessException e) {
			log.log(Level.SEVERE,":Error in RMI - "+e.getMessage());
			
			
		} catch (RemoteException e) {
			log.log(Level.SEVERE,":Error in RMI notifier -"+e.getMessage());
			
			
		} catch (NotBoundException e) {
			log.log(Level.SEVERE,":Error in RMI notifier -"+e.getMessage());
		
		}
	}
	
	private Registry foundRegistry(){
	
        Registry registry;
        while(true){
        	try {
        		//Get the Registry
        		registry = LocateRegistry.getRegistry(address,port);
        		return registry;
        	}catch (RemoteException e) {
        		try{
        			Thread.sleep(10);
        		}catch(InterruptedException ex){
        			
        			log.log(Level.WARNING,":Error in the sleep of RMI notifier: interrupted. Message:"+ex.getMessage());
        			
        		}
        		log.log(Level.SEVERE,"Registry not found, not possible to get notifier");
        	}
        }
	}
	
	/* (non-Javadoc)
	 * @see Server.networking.notifier.ServerToClientNotifier#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public synchronized void update(Observable match, Object observerParams) throws DisconnectedUserException {
		@SuppressWarnings("unchecked")
		Map<String,String> params=(Map<String,String>)observerParams;
		try{
			notifier.notifyMessage(params);
		}catch(RemoteException ex){
			MatchController matchController=(MatchController)match;
			matchController.disconnectUser(username);
			closeNotifier();
			log.log(Level.WARNING,":RMI notifier tried to send a message but it was not successfull: user "+username+" has been disconnected");
			throw new DisconnectedUserException(username);
		}
		

		
	}


	/* (non-Javadoc)
	 * @see Server.networking.notifier.ServerToClientNotifier#closeNotifier()
	 */
	@Override
	public void closeNotifier() {

		
	}
		
}
