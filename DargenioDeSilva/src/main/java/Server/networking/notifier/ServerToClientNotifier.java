package Server.networking.notifier;


import java.util.Observable;


import exceptions.DisconnectedUserException;

/**
 * @author Ruggiero Dargenio
 * Abstract class for notifiers. A notifier is that object which duty is to
 * inform the client of the changes in the match.
 */
public abstract class ServerToClientNotifier {
	
	
	/**
	 * This method updates the observer sending to the client the Map<String,String>
	 * observerParams
	 * @param match to update
	 * @param observerParams Map<String,String> to send to the client
	 * @throws DisconnectedUserException if the client disconnects during the match
	 */
	public abstract void update(Observable match, Object observerParams) throws DisconnectedUserException;
	
	/**
	 * Close the notifier, after having called this method it is not possible
	 * other messaged to the client
	 */
	public abstract void closeNotifier();
	
	/**
	 * Initialize the notifier
	 */
	public abstract void startConnection();


}
