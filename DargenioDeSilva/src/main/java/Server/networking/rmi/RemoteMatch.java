package Server.networking.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Ruggiero Dargenio
 * This is the remote interface for one match and included all the remote methods callable
 */
public interface RemoteMatch extends Remote{
	
	/**
	 * The method checks if the desired move is valid and if so the character is moved
	 * to the new position.
	 * @param username of the player
	 * @param sector where you want to move
	 * @return if it was possible or not
	 * @throws RemoteException
	 */
	public boolean setPosition(String username,String sector) throws RemoteException;
	
	/**
	 * This method controls that the player can attack (he has the right to do it)
	 * and if so, it performs the attack
	 * @param username of the player who wants to attack
	 * @return if the attack is possible
	 * @throws RemoteException
	 */
	public boolean attack(String username) throws RemoteException;
	
	/**
	 * This method controls that the sector is a valid one according to the card 
	 * of the player of this turn
	 * @param username of the player
	 * @param sector where the player will be performed
	 * @return
	 * @throws RemoteException
	 */
	public boolean noise(String username,String sector) throws RemoteException;
	
	/**
	 * This method is used to draw a card if you have to
	 * @param username of the player
	 * @return the type of card drawn
	 * @throws RemoteException
	 */
	public String drawCard(String username) throws RemoteException;
	
	
	/**
	 * This method should be called if the player can change the turn
	 * @param username of the player
	 * @return  true if it was possible to shift the turn
	 * @throws RemoteException
	 */
	public boolean changeTurn(String username) throws RemoteException;
	
	/**
	 * This method returns "alien" or "human" depending on the type of character
	 * of the player for this match
	 * @param username of the player
	 * @return the type of character
	 * @throws RemoteException
	 */
	public String giveMeCharacter(String username) throws RemoteException;
}
