package Server.networking.rmi;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;


/**
 * @author Ruggiero Dargenio
 * Remote interface that collects all the methods for the login and not
 * included in a specific match
 *
 */
public interface RemoteLogin extends Remote {
	/**
	 * This method is called by the RMI client to log in the system.
	 * Return a Map<String,String> with this format:
	 * key:"action" value:"logged" if logged successfully
	 * 				value:"alreadyLogged"
	 * 				value"notExistentUser"
	 * key: "port  value: port for the communication towards the client
	 * key: "matchID" value: id of the match
	 * @param username of the player
	 * @param password of the player
	 * @param preferredMap of the player. This map is taken under consideration for the
	 * choice of the map used in the match
	 * @return the result of the login request
	 * @throws RemoteException
	 */
	public Map<String,String> login(String username,String password,String preferredMap)throws RemoteException;
	

	/**
	 * This method link a notifier for the client
	 * @param address of the client
	 * @param username of the player
	 * @throws RemoteException
	 */
	public void giveMeNotifier(String address,String username) throws RemoteException;
	
	/**
	 * @param username of the new player
	 * @param password of the new player
	 * @return result of the signUp: "created" if the sign uo was successfull, invalidUsername if the username was already present
	 * @throws RemoteException
	 * @throws IOException
	 */
	public String signUp(String username,String password) throws RemoteException;
	
	/**
	 * @return a Map<String,String> with the 10 best players of the entire system
	 * @throws RemoteException
	 */
	public Map<String,String> getRanking() throws RemoteException;

}
