package it.polimi.ingsw.DargenioDeSilva;

/**
 * @author Ruggiero Dargenio
 * This class contains all the data of the past matches of the players
 *
 */
public class Score {
	private String username; 
	private int matches;
	private int victories;
	private long playedTurns;//total number of turns played( sum of the turn of matches played)
	
	/**
	 * Constructor
	 * @param data. Expected format: data[2] number of matches, data[3] number of victories
	 * data[4] number of played turns
	 */
	Score(String data[]){
		this.matches=Integer.parseInt(data[2]);
		this.victories=Integer.parseInt(data[3]);
		this.playedTurns=Long.parseLong(data[4]);
	
	}
	
	/**
	 * @param username of the player
	 * @param data . Expected format: data[0] number of matches, data[1] number of victories
	 * data[2] number of played turns
	 */
	Score(String username, String[] data){
		this.username=username; 
		this.matches=Integer.parseInt(data[0]); 
		this.victories=Integer.parseInt(data[1]); 
		this.playedTurns=Integer.parseInt(data[2]); 
	}
	
	/**
	 * @return the username of the player
	 */
	public String getUsername(){
		return username; 
	}
	/**
	 * @return the number of matches played by the player
	 */
	public int getMatches() {
		return matches;
	}
	
	/**
	 * @return the number of victories
	 */
	public int getVictories() {
		return victories;
	}
	
	/**
	 * @return the number of played turns
	 */
	public long getPlayedTurns() {
		return playedTurns;
	}
	
	/**
	 * Increment the matches played by the player
	 */
	public void incrementMatches() {
		this.matches ++;
	}

	/**
	 * Increment the number of victories of the player
	 */
	public void incrementVictories() {
		this.victories++;
	}

	/**
	 * Increment the number of turn played by the player adding the number passed as parameter
	 * @param playedTurns
	 */
	public void incrementPlayedTurns(int playedTurns) {
		this.playedTurns = this.playedTurns+playedTurns;
	}
	

}
