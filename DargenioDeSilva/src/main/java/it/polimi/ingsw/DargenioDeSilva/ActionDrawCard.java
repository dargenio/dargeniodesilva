package it.polimi.ingsw.DargenioDeSilva;

import java.util.HashMap;
import java.util.Map;

import abstractObjects.Card;
import abstractObjects.Sector;


/**
 * @author Ruggiero Dargenio
 * This class has all the methods needed to check if it is possible to draw a card for
 * the player and in that case performs this action.
 */
public class ActionDrawCard extends Action {



	/**
	 * Constructor
	 * @param mc instance of matchController
	 * @param username of the player
	 */
	public ActionDrawCard(MatchController mc,String username){
		this.username=username;
		this.matchController=mc;
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.DargenioDeSilva.Action#check()
	 */
	@Override
	public boolean check() {
		Player p=Player.players.get(username);
		Sector s=matchController.getMatch().getMap().get(matchController.getTurns()[getTurnIndex()].getPositionOf(p));
		if(isMyTurn(username)&&s!=null && s instanceof DangerousSector){
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see it.polimi.ingsw.DargenioDeSilva.Action#execute()
	 */
	@Override
	public String execute() {
		if(check()){
			Card card=matchController.getMatch().getDeck().takeFromDeck();


			matchController.getTurns()[getTurnIndex()].setCurrentPlayerCard(card);

			Map<String,String> params=new HashMap<String,String>();

			params.put("username", username);
			params.put("action", "notifyCard");

			matchController.notifyObservers(params);

			//Change the turn because there is nothing else to do
			if ((matchController.getTurns()[getTurnIndex()].getCard() instanceof SilenceCard)){
				Action changeTurn=new ActionChangeTurn(matchController,username);
				changeTurn.execute();
			}

			return card.toString();
		}
		return "false";
	}

}





