package it.polimi.ingsw.DargenioDeSilva;


import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


import java.util.logging.Level;
import java.util.logging.Logger;

import Server.networking.notifier.ServerToClientNotifier;
import Server.networking.rmi.RemoteMatch;
import exceptions.MatchPlayerException;


/**
 * This class is responsible for managing matches. It is a singleton 
 * and is unique in the server.
 * @author Ruggiero Dargenio
 *
 */
public class MatchManager {
	private Map<Integer,MatchController> matches=new HashMap<Integer,MatchController>();
	private Logger log=Logger.getLogger("log");
	private boolean timeout=false;

	private MatchController nextMatchController;
	private static int numberMatches=0;
	private static MatchManager instance=null;
	
	
/********************CONSTRUCTOR*****************************************/	
	/**
	 * private constructor . Create the first match
	 */
	private MatchManager(){
		nextMatchController=createAndAddMatch();
	}
	
	/**
	 * @return the unique instance of the matchManager
	 */
	public static MatchManager getInstance(){
		if (instance==null) instance=new MatchManager();
		return instance;
	}
	
/*************************END CONSTRUCTOR**********************************/
	
	
/***************************GETTERS AND SETTERS******************************/
	/**
	 * @param id of the match
	 * @return the matchController with ID id
	 */
	public MatchController getMatchController(int id) {
		return matches.get(id);
	}
	
/***************************END GETTERS AND SETTERS******************************/
	
	
/**************************NOTIFIERS METHODS**************************************/

	
	/**
	 * Register the notifier in the matchManager
	 * @param username of the player who asked for a notifier
	 * @param notifier to add
	 */
	public void addObserverNotifier(String username,ServerToClientNotifier notifier){
		try{
			MatchController matchController=getMyMatch(username);
		
			matchController.addObserverNotifier(username,notifier);
		
			if ((matchController.getNumberNotifiers()==Match.getMaxPlayers()&&!timeout)
				||(timeout && matchController.getNumberNotifiers()>=2)){
				
				startMatch(matchController);
			}
		}catch(MatchPlayerException ex){
			System.err.println(ex.getMessage());
		}

		
	}

/*************************END NOTIFIERS METHODS***********************************/

/**************************MATCH METHODS*******************************************/

	
	/**
	 * If there are no matches or all the existent matches are full 
	 * a new match is created.
	 * Otherwise the method returns the last match
	 * 
	 * @param player who asked for the method
	 * @param preferredMap map used if the player is the first for the match
	 * @return the match controller of the match
	 */
	public MatchController getMatch(Player player,String preferredMap){
		//If this is the first player who asked for a match
		int currentNumberPlayers=nextMatchController.getMatch().getPlayers().size();

		if(currentNumberPlayers==0){
			nextMatchController.getMatch().setMap(preferredMap);
		}
		
		
		//if the last match is full creates a new match	

		if ((currentNumberPlayers==Match.getMaxPlayers()&&!timeout)
				||(timeout && currentNumberPlayers>=ConfigurationClass.getNumberAfterTimeout())){

			nextMatchController=createAndAddMatch();
			nextMatchController.getMatch().setMap(preferredMap);
			nextMatchController.getMatch().addPlayer(player);
	
			return nextMatchController;
		}else{

			currentNumberPlayers++;
			nextMatchController.getMatch().addPlayer(player);
	
			return nextMatchController;
		}
		
	}


	
	/**
	 * This method start the matchController passed as a parameter
	 * @param matchController
	 */
	public void startMatch(MatchController matchController){
		
		matchController.start();
		
		
	}
	

	/**
	 * Increment the number of matches (numberMatches)and 
	 * create a new match with ID=numberMatches
	 * @param player
	 * @param preferredMap
	 * @return the new Match
	 */
	private MatchController createAndAddMatch(){
		numberMatches++;
		
		
		MatchController matchController=new MatchController(numberMatches);
		RMIMatchRegistration(matchController,numberMatches);
		nextMatchController=matchController;
		MatchTimeout matchTimeout=new MatchTimeout(nextMatchController);
		matchTimeout.start();
		
		matches.put(numberMatches,nextMatchController);
	
		
		return nextMatchController;
	}
	
	/**
	 * Register the match with ID id on the RMI registry
	 * @param id
	 */
	private void RMIMatchRegistration(MatchController mc,int id){
		Registry registry = null;
		String name = Integer.toString(id);
		
		try {
            RemoteMatch match=mc;
        	RemoteMatch stub= (RemoteMatch) UnicastRemoteObject.exportObject(match,id+10000);
                    
            registry = LocateRegistry.getRegistry(ConfigurationClass.getPortRegistry());            
            registry.bind(name, stub);
            
            System.out.println("RMI registry created for the match with id="+Integer.toString(id));
        } catch (RemoteException e) {
            System.err.println("Error in creating RMI registry");
            
            registry = null;
        }
        catch(AlreadyBoundException ABe){
        	log.log(Level.SEVERE,"AlreadyBound Exception "+ABe.getMessage());
        }
	}

	

	/**
	 * This method returns the matchController of the player passed as a parameter
	 * @throws MatchPlayerException if the player does not belong to none of the matches
	 * in the matchManager
	 * @param username of the player whom you want to know the matchController
	 * @return
	 * @throws MatchPlayerException
	 */
	private MatchController getMyMatch(String username)throws MatchPlayerException{
		Collection<MatchController> c=matches.values();
		for(MatchController matchController:c){
			
			Match m=matchController.getMatch();
			
			
			if(m.isMyPlayer(username))
				return matchController;
		}
		throw new MatchPlayerException(username);
	}
	
	/**
	 * Unbind the match from the registry
	 * @param ID
	 */
	void unbindMatch(int ID){
		Registry registry=null;
		
	    
	    try {
	    	registry = LocateRegistry.getRegistry(ConfigurationClass.getPortRegistry());
			registry.unbind(Integer.toString(ID));
		} catch (AccessException e) {
			log.log(Level.SEVERE, e.getMessage()+e.getStackTrace());
			
		} catch (RemoteException e) {
			log.log(Level.SEVERE, e.getMessage()+e.getStackTrace());
		} catch (NotBoundException e) {
			log.log(Level.SEVERE, e.getMessage()+e.getStackTrace());
		}
	}
	
/***********************MATCH TIMEOUT INNER CLASS**************************/	
	
	/**
	 * @author Ruggiero Dargenio
	 * This class is used to manage the timer for the starting of the match.
	 * If after waitForMatch milliseconds the match did not reach the max number
	 * of players per match, the match starts if the current number of players is 
	 * >= of numberAfterTimeout players, variable that you can find in the 
	 * ConfigurationClass
	 */
	class MatchTimeout extends Thread{
		private long waitForMatch=ConfigurationClass.getWaitForMatch(); //5 minutes
		MatchController mc;
		private Logger log=Logger.getLogger("log");
		
		public MatchTimeout(MatchController mc){
			this.mc=mc;
		
			timeout=false;
		}
		
		@Override
		public void run(){
			try{
				sleep(waitForMatch);
				if(!mc.isPlaying() &&
						mc.getMatch().getNumberPlayers()>=ConfigurationClass.getNumberAfterTimeout() && !mc.isEnded()){
						
						mc.start();
						nextMatchController=createAndAddMatch();
					
				}
				else
					timeout=false;
			}catch(InterruptedException IEe){
				log.log(Level.SEVERE, "MatchTimeout interrupted!! "+IEe.getMessage());
			}
		}
	}
	

}
