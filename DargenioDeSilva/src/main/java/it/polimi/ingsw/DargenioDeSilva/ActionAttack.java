package it.polimi.ingsw.DargenioDeSilva;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Ruggiero Dargenio
 * This class manages the attack. It checks if it is possible for the player to attack
 * and in this case performs it.
 *
 */
public class ActionAttack extends Action {
	Player attacker;

	/**
	 * Constructor 
	 * @param mc instance of matchController
	 * @param username of the player who wants to perform the action
	 */
	public ActionAttack(MatchController mc,String username){
		this.username=username;
		matchController=mc;
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.DargenioDeSilva.Action#check()
	 */
	@Override
	public boolean check() {
		attacker=Player.players.get(username);

		//If the attacker can attack(he is an Alien)
		if (isMyTurn(username)&&
				attacker.getCharacter().getMove().validAttack()&&
				hasMoved()){
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see it.polimi.ingsw.DargenioDeSilva.Action#execute()
	 * 
	 */
	@Override
	public String execute() {
		if(check()){
			List<Player> temp=new ArrayList<Player>();
			for (int i=0; i<matchController.getMatch().getNumberPlayers();i++)
				temp.add(i, matchController.getMatch().getPlayers().get(i));

			for(Player p:temp){
				if(p.getCharacter().getSector()==attacker.getCharacter().getSector()){
					if(p.getCharacter()!=attacker.getCharacter())
						matchController.delPlayer(p);
				}
			}


			//Notify that the player attacked, independently if he hits someone
			Map<String,String> params=new HashMap<String,String>();
			params.put("action", "attack");
			params.put("username", username);
			params.put("sector", attacker.getCharacter().getSector().toString());
			matchController.notifyObservers(params);
			if(!matchController.checkEndGame()){
				//change player, next Turn
				Action changeTurn=new ActionChangeTurn(matchController,username);
				changeTurn.execute();
			}
			return "true";
		}else
			return "false";
	}

	/**
	 * @return true if the player has already moved, false otherwise
	 */
	private boolean hasMoved(){
		if (getTurnIndex()==0||matchController.getTurns()[getTurnIndex()].getPositionOf(attacker)!=
				matchController.getTurns()[getTurnIndex()-1].getPositionOf(attacker))
			return true;
		return false;
	}

}
