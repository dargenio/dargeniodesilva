package it.polimi.ingsw.DargenioDeSilva;

import abstractObjects.MoveStrategy;
import abstractObjects.Sector;

/**
 * @author Ruggiero Dargenio
 * This class is the strategy implementation of the move for the human
 *
 */
public class HumanMove extends MoveStrategy {

	/* (non-Javadoc)
	 * @see abstractObjects.MoveStrategy#validMove(it.polimi.ingsw.DargenioDeSilva.MatchMap, abstractObjects.Sector, abstractObjects.Sector)
	 */
	@Override
	public boolean validMove(MatchMap m, Sector start,Sector finish){
		if (start==finish)
			return false; 
		if (m.getNearSectors(start).contains(finish) && checkConditions(finish)) 			
			return true;
		else 
			return false; 
	}

	/* (non-Javadoc)
	 * @see abstractObjects.MoveStrategy#validAttack()
	 */
	@Override
	public boolean validAttack(){
		return false;
	}

}
