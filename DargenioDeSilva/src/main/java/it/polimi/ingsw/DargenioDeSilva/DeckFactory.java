package it.polimi.ingsw.DargenioDeSilva;


/**
 * @author Ruggiero Dargenio
 * Factory of the deck
 *
 */
public class DeckFactory {



	/**
	 * Create a deck following the game rules
	 * @return deck created
	 */
	public static Deck deckFactory(){
		int noyseAny=10;
		int noyseYours=10;
		int silence=5;

		Deck deck= new Deck();
		for(int i=0;i<noyseAny;i++){
			NoiseAnyCard card=new NoiseAnyCard();
			deck.addCard(card);
		}
		for(int i=0;i<noyseYours;i++){
			NoiseYoursCard card=new NoiseYoursCard();
			deck.addCard(card);
		}
		for(int i=0;i<silence;i++){
			SilenceCard card=new SilenceCard();
			deck.addCard(card);
		}
		return deck;
	}

}