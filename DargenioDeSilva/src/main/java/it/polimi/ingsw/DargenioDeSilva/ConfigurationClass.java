package it.polimi.ingsw.DargenioDeSilva;

public class ConfigurationClass {

	//Max number of players per match
	private static int maxNumberPlayers = 8;
	//Max number of turns in a match
	private static int maxNumberTurn = 39;
	// Number of player required after the timeout (<=maxNumberPlayers)
	private static int numberAfterTimeout = 2;
	//Max time in milliseconds after which the the max number of players for the match
	//is set to numberAfterTimeout
	private static int waitForMatch = 200000;
	//Max time in milliseconds after which the turn is lost
	private static int maxLentghTurn=100000;
	//Port used to register the RMI register
	private static int portRegistry = 5999;
	//Port numeration for notifiers starts from this number
	private static int startReverseCommunication = 6000;
	//address of the server
	private static String serverAddress="localhost";
	//address of the client
	private static String clientAddress="localhost";
	
	public static String getServerAddress() {
		return serverAddress;
	}

	public static String getClientAddress() {
		return clientAddress;
	}

	public static int getMaxLentghTurn() {
		return maxLentghTurn;
	}

	public static int getMaxNumberPlayers() {
		return maxNumberPlayers;
	}

	public static int getWaitForMatch() {
		return waitForMatch;
	}

	public static int getNumberAfterTimeout() {
		return numberAfterTimeout;
	}

	public static int getMaxNumberTurn() {
		return maxNumberTurn;
	}

	public static int getPortRegistry() {
		return portRegistry;
	}

	public static int getStartReverseCommunication() {
		return startReverseCommunication;
	}



}
