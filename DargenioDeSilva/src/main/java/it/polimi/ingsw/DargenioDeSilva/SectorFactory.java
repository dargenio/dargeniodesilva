package it.polimi.ingsw.DargenioDeSilva;

import abstractObjects.Sector;

/**
 * @author Ruggiero Dargenio
 * This class is the factory for the sectors. It is used to create the map starting
 * from the file where all the types of sectors are specified
 */
public class SectorFactory {
	/**
	 * Type description:
	 * S secure sector
	 * B blocked sector
	 * D dangerous sector
	 * A alien sector
	 * E escape hatch sector
	 * 
	 * @param x, first coordinate of the sector (letter from "A" to "W")
	 * @param y second coordinate of the sector (number from 1 to 14)
	 * @param type 
	 * @return the sector created according to the type and coordinates
	 */
	public static Sector sectorFactory(char x,int y,char type){
		
		switch(type){
		//Secure Sector
		case 'S':
			return (new SecureSector(x,y));
		//Blocked Sector
		case 'B':
			return new SecureSector(x,y,true);
		//Dangerous Sector
		case 'D':
			return new DangerousSector(x,y);
		//Alien Sector
		case 'A':
			return new AlienSector(x,y);
		//Human Sector
		case 'H':
			return new HumanSector(x,y);
		//Escape Sector
		case 'E':
			return new EscapeHatchSector(x,y);
		}
		return new SecureSector(x,y);
	}
}