package it.polimi.ingsw.DargenioDeSilva;

import java.util.logging.Logger;

/**
 * @author Ruggiero Dargenio
 * This thread performs a timeout. When it is over it reverse the turn.
 * If the player performs his turn within the max time the thread is interrupted 
 *
 */
public class TurnTimeout extends Thread {
	private MatchController matchController;
	final private long timeout=ConfigurationClass.getMaxLentghTurn();
	private Logger log=Logger.getLogger("logger");
	private String username;
	
	public TurnTimeout(MatchController matchController){
		this.matchController=matchController;
		username=matchController.getCurrentPlayer().getUsername();
	}
	
	@Override
	public void run(){
		try{
			sleep(timeout);
			
			matchController.reverseTurn();
		
		}catch(InterruptedException ex){
		}
	}
	
}
