package it.polimi.ingsw.DargenioDeSilva;

import java.util.HashMap;
import java.util.Map;
import abstractObjects.GameCharacter;

/**
 * @author Ruggiero Dargenio
 * An instance of this class represents a player to which is associated a character
 * during one match
 */
public class Player {
	public static Map<String,Player> players=new HashMap<String,Player>();
	private String username;
	private GameCharacter character;
	protected Score scores;
	
	
	/**
	 * @return the score of the player
	 */
	public Score getScores() {
		return scores;
	}

	/**
	 * Increments the scores of the player according to the match result
	 * @param winner true if the player won, false otherwise
	 * @param turnPlayed number of turn played
	 */
	public void setIncrementScore(boolean winner,int turnPlayed){
		scores.incrementMatches();
		scores.incrementPlayedTurns(turnPlayed);

		if (winner)
			scores.incrementVictories();



	}

	/**
	 * Constructor
	 * @param data expected format: data[0] username, data[2] number of matches, 
	 * data[3] number of victories, data[4] number of played turns
	 */
	Player(String[] data){
		this.username=data[0];
		scores=new Score(data);
		players.put(username, this);
	}

	/**
	 * @return the username of the player
	 */
	public String getUsername() {
		return username;
	}


	/**
	 * @return the character of the player
	 */
	public GameCharacter getCharacter() {
		return character;
	}

	/**
	 * @param character of this player
	 */
	public void setCharacter(GameCharacter c) {
		this.character = c;
	}
}
	



