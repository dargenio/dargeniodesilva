package it.polimi.ingsw.DargenioDeSilva;

import abstractObjects.GameCharacter;
import abstractObjects.Sector;

/**
 * @author Ruggiero Dargenio
 * An instance of this class represents a human character
 *
 */
public class Human extends GameCharacter {

	Human(Sector sector){
		this.move=new HumanMove(); 
		this.sector=sector;
	}

}
