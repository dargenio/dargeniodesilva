package it.polimi.ingsw.DargenioDeSilva;

import abstractObjects.GameCharacter;
import abstractObjects.Sector;



/**
 * @author Ruggiero Dargenio
 * An instance of this class represents an alien character
 */
public class Alien extends GameCharacter {



	/**
	 * Constructor
	 * @param sector the initial sector
	 */
	Alien(Sector sector) {
		this.move=new AlienMove(); 
		this.sector=sector;
	}

}


