package it.polimi.ingsw.DargenioDeSilva;

import abstractObjects.StartFinishSector;

/**
 * @author Ruggiero Dargenio
 * An instance of this class represents the sector where all the humans
 * are when the match starts
 *
 */
public class HumanSector extends StartFinishSector {

	/**
	 * @param x, first coordinate of the sector (letter from "A" to "W")
	 * @param y second coordinate of the sector (number from 1 to 14)
	 */
	public HumanSector(char x,int y){
		super(x,y);
	}


}
