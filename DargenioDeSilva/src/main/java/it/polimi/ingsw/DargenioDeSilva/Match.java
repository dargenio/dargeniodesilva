package it.polimi.ingsw.DargenioDeSilva;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import exceptions.NotMapException;




/**
 * This class collect all the elements of a match. It represents the state of a match.
 * @author Ruggiero Dargenio
 *
 */
public class Match {
	private static int maxPlayers=ConfigurationClass.getMaxNumberPlayers();
	private int ID;
	private List<Player> players;
	private MatchMap map;
	private Deck deck;
	private List<Player> winners;


	/**
	 * Constructor
	 * @param id of the match
	 */
	public Match(int id){
		ID=id;
		
		
		this.deck=DeckFactory.deckFactory();
		players=new ArrayList<Player>();
		winners=new ArrayList<Player>();
	}
	
	/***********************GETTERS AND SETTERS*****************************/
	public int getNumberPlayers(){
		return players.size();
	}
	
	public static int getMaxPlayers() {
		return maxPlayers;
	}

	void setMap(String name) {
		try{
			map=new MatchMap(name);
		}catch(NotMapException NMe){
			System.err.println(NMe.getMessage());
			try{
				map=new MatchMap("Galilei");
			}catch(NotMapException ex){
				System.err.println(ex.getMessage());
			}
		}
	}
	

	void setWinner (Player player){
		winners.add(player);
	}
	
	List<Player> getWinners(){
		return winners;
	}
	public Deck getDeck() {
		return deck;
	}
	public List<Player> getPlayers() {
		return players;
	}
	
	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}
	
	/************************END GETTERS AND SETTERS****************************/



	
	public synchronized boolean addPlayer(Player p){
		if (players.size() <maxPlayers){
			players.add(p);
			return true;
			}
		else 
			return false;
	}
	
	public synchronized void delPlayer(Player player){
		players.remove(player);
	}
	
	public synchronized boolean isMyPlayer(String username){
		Player player=Player.players.get(username);
		for(Player p:players){
			if(player==p)
				return true;
		}
		
		return false;
	}



	
	/**
	 * @return the map of this match
	 */
	public MatchMap getMap() {
		return map;
	}
	
	void start(){
		Collections.shuffle(players);
	}

}
