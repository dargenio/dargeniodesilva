package it.polimi.ingsw.DargenioDeSilva;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ruggiero Dargenio
 * This class manages all the maps that can be chosen in a match.
 *
 */
public class MapManager {
	private static MapManager instance=null;
	private Map<String,String> file =new HashMap<String,String>();
	
	public Map<String, String> getFile() {
		return file;
	}
	
	public static MapManager getInstance(){
		if(instance==null) instance=new MapManager();
			return instance;
	}

	private MapManager(){
		file.put("Galilei", "file/Galilei.txt");
		file.put("Galvani", "file/Galvani.txt");
		file.put("Fermi", "file/Fermi.txt");
	}
	

}
