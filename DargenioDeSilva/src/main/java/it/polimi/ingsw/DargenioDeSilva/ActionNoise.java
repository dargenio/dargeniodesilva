package it.polimi.ingsw.DargenioDeSilva;

import java.util.HashMap;
import java.util.Map;

import abstractObjects.Sector;


/**
 * 
 * @author Ruggiero Dargenio
 * This class has all the methods needed to check if it is possible to do noise in
 * sector specified by the player and in that case performs this action.
 */
public class ActionNoise extends Action {
	private String sector;


	/**
	 * Constructor
	 * @param mc instance of matchControlelr
	 * @param username of the player
	 * @param sector string that represents a valid sector
	 */
	public ActionNoise(MatchController mc,String username,String sector){
		matchController=mc;
		this.username=username;
		this.sector=sector;
	}

	/* (non-Javadoc)
	 * @see it.polimi.ingsw.DargenioDeSilva.Action#check()
	 */
	@Override
	public boolean check() {
		if(!Sector.isValid(sector))
			return false;

		if(isMyTurn(username)&&matchController.getTurns()[getTurnIndex()].isCardTaken() && 
				(matchController.getTurns()[getTurnIndex()].getCard() instanceof NoiseAnyCard)
				||matchController.getTurns()[getTurnIndex()].getCard() instanceof NoiseYoursCard){
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see it.polimi.ingsw.DargenioDeSilva.Action#execute()
	 */
	@Override
	public String execute() {
		if(check()){
			Player p=Player.players.get(username);

			Sector noyseSector=matchController.getMatch().getMap().get(sector);
			Sector mySector=p.getCharacter().getSector();

			if (matchController.getTurns()[getTurnIndex()].getCard().isValidNoise(mySector,noyseSector)){

				Map<String,String> params=new HashMap<String,String>();
				params.put("username", username);
				params.put("action", "noise");
				params.put("sector", sector);
				matchController.notifyObservers(params);

				Action changeTurn=new ActionChangeTurn(matchController,username);
				changeTurn.execute();




				return "true";
			}
		}
		return "false";

	}
}


