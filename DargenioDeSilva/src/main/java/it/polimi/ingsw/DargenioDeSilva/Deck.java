package it.polimi.ingsw.DargenioDeSilva;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import abstractObjects.Card;

/**
 * @author Ruggiero Dargenio
 *	An instance of this class represents a deck.
 */
public class Deck {
	private int totalCardsIndex=0;//index in the deck, the ones out of index are already picked out
	private int totalCards=0; //real total number of cards
	private List<Card> cards= new ArrayList<Card>();


	/**
	 * Take a card from the desk randomly and delete that card from the deck
	 * @return a card randomly chosen
	 */
	public Card takeFromDeck(){
		if (totalCardsIndex==0){
			regenerateDeck();
		}

		Random generator = new Random();

		int index=0;
		index = generator.nextInt( totalCardsIndex+1 );

		Card temp = cards.get(index);
		cards.set(index, cards.get(totalCardsIndex));

		cards.set(totalCardsIndex,temp);

		totalCardsIndex--;

		return temp;

	}	

	/**
	 * In case all the card of the deck are drawn, this methods
	 * used all the cards drawn, shuffle them, to regenerate the deck
	 */
	private void regenerateDeck (){
		totalCardsIndex=totalCards-1;
	}
	/**
	 * This method add one card to the deck
	 * @param card to add
	 */
	public void addCard(Card card){
		cards.add(totalCardsIndex,card);
		totalCards++;
		totalCardsIndex=totalCards-1;
	}
}