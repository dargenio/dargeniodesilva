package it.polimi.ingsw.DargenioDeSilva;

import java.util.HashMap;
import java.util.Map;

import abstractObjects.GameCharacter;
import abstractObjects.Sector;


/**
 * @author Ruggiero Dargenio
 * This class has all the methods needed to check if it is possible to change position in
 * sector specified by the player and in that case performs this action.
 */
public class ActionPosition extends Action {
	String sector;

	/**
	 * @param mc instance of matchController
	 * @param username of the player
	 * @param sector where the player wants to move
	 */
	public ActionPosition(MatchController mc,String username,String sector){
		this.username=username;
		this.sector=sector;
		matchController=mc;
	}

	/* (non-Javadoc)
	 * @see it.polimi.ingsw.DargenioDeSilva.Action#check()
	 */
	@Override
	public boolean check() {
		if(!Sector.isValid(sector))
			return false;
		GameCharacter c=Player.players.get(username).getCharacter();
		if(isMyTurn(username)&& 
				c.setPosition(matchController.getMatch().getMap(),matchController.getMatch().getMap().get(sector))){
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see it.polimi.ingsw.DargenioDeSilva.Action#execute()
	 */
	@Override
	public String execute() {
		if (check()){
			GameCharacter c=Player.players.get(username).getCharacter();
			matchController.getTurns()[getTurnIndex()].setNewSector(c.getSector());
			//c.getSector().action();

			if(c.getSector() instanceof SecureSector)
				matchController.getTurns()[getTurnIndex()].setPossibleEnd();

			//The player is necessarily a human if this condtion is true
			if(c.getSector() instanceof EscapeHatchSector){
				Player winner =Player.players.get(username);
				matchController.getMatch().getWinners().add(winner);
				matchController.matchTerminated();
				return "true";
			}else{

				//Notify everybody
				Map<String,String> params=new HashMap<String,String>();

				params.put("username", username);
				params.put("action", "notifySetPosition");

				matchController.notifyObservers(params);
				return "true";
			}
		}
		return "false";

	}

}
