package it.polimi.ingsw.DargenioDeSilva;


import java.util.ArrayList;
import java.util.List;

import abstractObjects.Card;
import abstractObjects.Sector;


/**
 * This class contains the methods used to manage a turn in a match
 * @author Ruggiero Dargenio
 *
 */
public class Turn{
	private static int maxTurn=ConfigurationClass.getMaxNumberTurn();
	private TurnMove playerMoves[]=new TurnMove[Match.getMaxPlayers()];
	private int currentMove;
	private boolean played=false;
	//This list is coherent with the one of the match
	private List<Player> players;
	//This list is used to store all the players at the beginning of the turn
	private List<Player> playersData;
	private List<Player> playersWhoPlayed=new ArrayList<Player>();

	
/********************CONSTRUCTOR*****************************************/
	/**
	 * Create and initialize the turn
	 * @param players
	 */
	Turn(List<Player> players){
		
		currentMove=0;
		playerMoves[0]=new TurnMove(players.get(currentMove));
		playersWhoPlayed.add(getCurrentPlayer());
		played=true;
		this.players=players;
		playersData=new ArrayList<Player>();
		for(Player p:players)
			playersData.add(p);
		
	}
	
/**************************GETTERS AND SETTERS************************************/
	/**
	 * @return the max number of turns for the match
	 */
	static int getMaxTurn(){
		return maxTurn;
	}

	/**
	 * @return the list of players of this match
	 */
	List<Player> getPlayers(){
		List<Player> list=new ArrayList<Player>();
		for(Player p: playersData)
			list.add(p);
		
		return list;
	}	
	
	/**
	 * @return if the turn has been played or not.
	 */
	boolean isPlayed(){
		return played;
	}
	
/**************************END GETTERS AND SETTERS ***********************************/
	
/*************************CURRENT PLAYER GETTERS AND SETTERS**************************/	
	
	
	/**
	 * @return the current player the is performing his moves at the moment
	 * when this method is called
	 */
	Player getCurrentPlayer() {
		return playerMoves[currentMove].getPlayer();
	}
	
	
	/**
	 * Set the card drawn by the current Player
	 * @param card
	 */
	void setCurrentPlayerCard(Card card){
		playerMoves[currentMove].card=card;
	}
	
	
	/**
	 * @return the card drawn by the current player of the match. Return null
	 * if the player has not drawn a card yet.
	 */
	Card getCard() {
		return playerMoves[currentMove].card;
	}

	
	
	/**
	 * @return if the current player has already moved or not in this turn
	 */
	boolean isPlayerMoved(){
		if(playerMoves[currentMove].turnSector==null)
			return false;
		else
			return true;
	}
	
	/**
	 * This method should be called when match is in a status
	 * in which the current player can decide to shift the turn.
	 */
	void setPossibleEnd(){
		playerMoves[currentMove].possibleEndTurn=true;
	}
	
	
	/**
	 * @return if the match is in a status in which is possible to shift turn
	 */
	boolean isPossibleEnd(){
		return playerMoves[currentMove].possibleEndTurn;
	}
	
	/**
	 * Memorize that the player has attacked
	 */
	void setAttack(){
		playerMoves[currentMove].attack=true;
	}
	
	/**
	 * Set the new sector when a player changed his position
	 * @param sector 
	 */
	void setNewSector(Sector sector){
		playerMoves[currentMove].turnSector=sector;
	}
	
	/**
	 * @return if the player has drawn the card
	 */
	boolean isCardTaken(){
		if(playerMoves[currentMove].card==null)
			return false;
		
		return true;
	}
	
/*************END CURRENT PLAYER GETTERS AND SETTERS****************************/
	
	
	/**
	 * @param player
	 * @return the sector of the player 
	 * 
	 */
	Sector getSectorOf(Player player){
		int i=0;
		while(playerMoves[i]!=null){
			if (playerMoves[i].player==player)
				return playerMoves[i].turnSector;
			i++;
		}
		return null;
			
	}
	
	/**
	 * Shift turn to the next player of this turn
	 */
	void nextPlayer(){
		
		int index=players.indexOf(getCurrentPlayer());
		index++;
		
		currentMove++;
		if(index!=-1){
			playerMoves[currentMove]=new TurnMove(players.get(index));
			playersWhoPlayed.add(getCurrentPlayer());
		}else{
			playersWhoPlayed.add(getCurrentPlayer());
			playerMoves[currentMove]=new TurnMove(players.get(currentMove));
	
		}
	}
	


	
	
	/**
	 * @param p, the player whom you want to know the position
	 * @return
	 */
	String getPositionOf(Player p){
		Sector sector=playerMoves[currentMove].turnSector;
		if(sector!=null)
			return sector.toString();
		else return null;
	}

	




	/**
	 * @return true if there are players who have not played this turn yet.
	 */
	boolean isThereMove(){
		if(!playersWhoPlayed.containsAll(players)){
			return true;
		}else{
			return false;
		}
	
	}
	
/*************************INNER CLASS TURN MOVE*******************************/
	
	/**
	 * 
	 * This class manages the turn of one player in this turn
	 * @author Ruggiero Dargenio
	 *
	 */
	class TurnMove {
		Player player;
		Sector turnSector;
		boolean attack;
		Card card;
		boolean possibleEndTurn=false;


		TurnMove(Player player){
			//Variables initialization
			this.player=player;
			
			attack=false;
			card=null;
			turnSector=player.getCharacter().getSector();
			
		}
		
		public Player getPlayer() {
			return player;
		}
		
	
	}
}




