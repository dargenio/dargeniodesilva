package it.polimi.ingsw.DargenioDeSilva;

import abstractObjects.Card;
import abstractObjects.Sector;

/**
 * @author Ruggiero Dargenio
 *
 */

public class NoiseAnyCard extends Card {
	
	public NoiseAnyCard(){
		
	}

	@Override
	public boolean isValidNoise(Sector mine,Sector noyse) {
		if(!noyse.isBlocked()){
			return true;
		}
		return false;
	}

}
