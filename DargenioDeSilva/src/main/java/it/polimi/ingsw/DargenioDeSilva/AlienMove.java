package it.polimi.ingsw.DargenioDeSilva;

import java.util.ArrayList;
import java.util.List;

import abstractObjects.MoveStrategy;
import abstractObjects.Sector;

/**
 * @author Ruggiero Dargenio
 * This class is the strategy implementation of the move for the alien
 *
 */
public class AlienMove extends MoveStrategy {
	
	/* (non-Javadoc)
	 * @see abstractObjects.MoveStrategy#validMove(it.polimi.ingsw.DargenioDeSilva.MatchMap, abstractObjects.Sector, abstractObjects.Sector)
	 */
	@Override
	public boolean validMove(MatchMap m, Sector start,Sector finish){
		List<Sector> tempSectors=new ArrayList<Sector>(); 
		if((start==finish)|| (finish) instanceof EscapeHatchSector )
			return false;
		if (m.getNearSectors(start).contains(finish) && checkConditions(finish) && !(finish instanceof EscapeHatchSector))
			return true;
		else {
			for (Sector s: m.getNearSectors(start)) {
				
				if (!s.isBlocked())
				tempSectors.add(s); 
				}
			
			for (Sector s:tempSectors){
				if(m.getNearSectors(s).contains(finish) && checkConditions(finish)&& !(finish instanceof EscapeHatchSector))
					return true;
			}
			return false; 
		} 
	}
	
	/* (non-Javadoc)
	 * @see abstractObjects.MoveStrategy#validAttack()
	 */
	@Override
	public boolean validAttack(){
		return true;
	}
}
