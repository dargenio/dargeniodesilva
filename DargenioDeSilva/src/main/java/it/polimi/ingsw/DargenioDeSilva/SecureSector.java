package it.polimi.ingsw.DargenioDeSilva;

import abstractObjects.GameSector;

/**
 * @author Ruggiero Dargenio
 * An instance of this class represents a secure sector
 *
 */
public class SecureSector extends GameSector {
	
	
	/**
	 * @param x, first coordinate of the sector (letter from "A" to "W")
	 * @param y second coordinate of the sector (number from 1 to 14)
	 * @param b, true if blocked, false otherwise
	 */
	public SecureSector(char x,int y,boolean b){
		super(x,y,b);
	}
	
	/**
	 * @param x, first coordinate of the sector (letter from "A" to "W")
	 * @param y second coordinate of the sector (number from 1 to 14)
	 */
	public SecureSector(char x,int y){
		super(x,y);
	}
	

	
}
