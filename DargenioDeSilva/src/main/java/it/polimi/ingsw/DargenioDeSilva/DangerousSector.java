package it.polimi.ingsw.DargenioDeSilva;


import abstractObjects.Sector;

/**
 * @author Ruggiero Dargenio
 * An instance of this class represents a dangerous sector
 *
 */
public class DangerousSector extends Sector {
	

	
	/**
	 * @param x, first coordinate of the sector (letter from "A" to "W")
	 * @param y second coordinate of the sector (number from 1 to 14)
	 */
	public DangerousSector(char x,int y){
		super(x,y);
	}
	


}
