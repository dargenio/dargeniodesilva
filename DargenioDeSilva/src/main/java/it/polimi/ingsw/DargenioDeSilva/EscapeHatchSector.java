package it.polimi.ingsw.DargenioDeSilva;

import abstractObjects.StartFinishSector;

/**
 * @author Ruggiero Dargenio
 * An instance of this class represents an escape hatch sector
 *
 */
public class EscapeHatchSector extends StartFinishSector {

	/**
	 * @param x, first coordinate of the sector (letter from "A" to "W")
	 * @param y second coordinate of the sector (number from 1 to 14)
	 */
	public EscapeHatchSector(char x,int y){
		super(x,y);
	}

}
