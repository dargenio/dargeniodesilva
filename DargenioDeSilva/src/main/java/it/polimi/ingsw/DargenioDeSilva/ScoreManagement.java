package it.polimi.ingsw.DargenioDeSilva;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ruggiero Dargenio
 * This class includes all the methods for mananging the score
 *
 */
public class ScoreManagement {
	private Logger log=Logger.getLogger("log");
	
	
	/**
	 * This method stores in the PlayersData the new score of the player
	 * @param username of the player to store
	 */
	void storeScores(String username){
		Player player=Player.players.get(username);
		int index=getIndex(username);
		try{
			
		    RandomAccessFile ra = new RandomAccessFile("file/PlayersData.txt", "rw");
			
			for(int i=0;i<index;i++){
				ra.readLine();
			
			}
			ra.writeBytes(username+","+Integer.toString(player.getScores().getMatches())+
					","+Integer.toString(player.getScores().getVictories())+","
					+Long.toString(player.getScores().getPlayedTurns())+",");

			
			ra.close();
		}catch (IOException ex){
			log.log(Level.SEVERE, "Not possible to open the file writer to store scores");
		}
	}
	
	/**
	 * This methods is an utility used to find the position of
	 * the player in the file of the scores
	 * @param username
	 * @return the index in the file where is the player
	 */
	private int getIndex(String username){
		try{
			File file=new File("file/PlayersData.txt");
			Scanner in=new Scanner(file);
			

			String line;
			int index=0;
			while(in.hasNextLine()){
				line=(in.nextLine()); 
				String[] parts=line.split(",");
				
				if(parts[0].equals(username)){
				
					in.close();
					return index;
				}
				
				index++;
			}
			
			in.close();
		}catch(FileNotFoundException ex){
			log.log(Level.SEVERE, "File not found:"+ex.getMessage());
		}
		return -1;
	}
	
	public Map<String, String> getRanking(){
		try{
			File file = new File("file/PlayersData.txt");
			Scanner in = new Scanner(file);
			String line;
			Map<String,String> rank=new HashMap<String, String>(); 			
			String username;
			List<Score> scores = new ArrayList<Score>();
			int i=0; 

			while (in.hasNextLine()) {
				line = in.nextLine();
				String[] data={"0","0","0"};
				username = line.split(",")[0];
				data[0] = line.split(",")[1];
				data[1] = line.split(",")[2];
				data[2] = line.split(",")[3];

				scores.add(new Score(username, data));
			}
			Collections.sort(scores, new ScoreChainedComparator(new VictoriesComparator(), new TurnsComparator()));
			for (Score s:scores){
				if (i<10) {
				rank.put("param"+Integer.toString(i), s.getUsername()+","+s.getVictories()+","+s.getPlayedTurns()+","+s.getMatches());
				i++;
				}
			}
			in.close();
			return rank; 
			}catch(FileNotFoundException e){
				log.log(Level.SEVERE,"Error in opening file"+e.getMessage());
				return null; 
			}
			
		}

	 class ScoreChainedComparator implements Comparator<Score> {

		private List<Comparator<Score>> listComparators;

		@SafeVarargs
		public ScoreChainedComparator(Comparator<Score>... comparators) {
			this.listComparators = Arrays.asList(comparators);
		}

		@Override
		public int compare(Score s1, Score s2) {
			for (Comparator<Score> comparator : listComparators) {
				int result = comparator.compare(s1, s2);
				if (result != 0) 
					return result;
				
			}
			return 0;
		
		}
		}

	class VictoriesComparator implements Comparator<Score> {

		@Override
		public int compare(Score s1, Score s2) {
			return s2.getVictories()- s1.getVictories();
		}
	}

	class TurnsComparator implements Comparator<Score> {

		@Override
		public int compare(Score s1, Score s2) {
			return (int) (s2.getPlayedTurns() - s1.getVictories() );
		}
	}
}
