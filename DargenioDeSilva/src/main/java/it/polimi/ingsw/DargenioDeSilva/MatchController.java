package it.polimi.ingsw.DargenioDeSilva;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import exceptions.DisconnectedUserException;
import abstractObjects.GameCharacter;
import abstractObjects.Sector;
import Server.networking.notifier.ServerToClientNotifier;
import Server.networking.rmi.RemoteMatch;

/**
 * This class is the controller of the single match. Its goal is to get players 
 * requests, check them if they are valid and perform them.
 * It implements the RemoteMatch used in RMI communication and extends
 * Observable, notifiers know when something has changed in the match status
 * and inform all the players of the change.  
 * 
 * @author Ruggiero Dargenio
 *
 */
public class MatchController extends Observable implements RemoteMatch{
	private Match match;
	private Turn turns[]=new Turn[Turn.getMaxTurn()];
	private int turnIndex;
	private boolean playing;
	private boolean ended=false;
	private int numberPlayers;
	private TurnTimeout timeout;
	private Action action;
	Map<String,ServerToClientNotifier> notifiers=new HashMap<String,ServerToClientNotifier>();
	
	/*************************CONSTRUCTOR***************************************/
	MatchController(int ID){
		match=new Match(ID);
		//RMIMatchRegistration(ID);
		playing=false;
	}
	
	/************************GETTERS AND SETTERS ********************************/
	public TurnTimeout getTimeout() {
		return timeout;
	}
	
	/**
	 * @return if the game is playing at the moment
	 */
	public boolean isPlaying(){
		return playing;
	}
	/**
	 * @return if the match is ended
	 */
	public boolean isEnded() {
		return ended;
	}

	/**
	 * @return the index of the current turn
	 */
	int getTurnIndex() {
		return turnIndex;
	}
	
	/**
	 * @param timeout
	 */
	public void setTimeout(TurnTimeout timeout) {
		this.timeout = timeout;
	}

	/**
	 * @return the match of this matchController
	 */
	public Match getMatch() {
		return match;
	}

	/**
	 * @return the array of turns
	 */
	public Turn[] getTurns() {
		return turns;
	}
	/**
	 * @param match to set
	 */
	public void setMatch(Match match) {
		this.match = match;
	}
	

	/**
	 * increment the turn index. It should be used when all the players 
	 * played their turn
	 */
	public void incrementTurnIndex() {
		turnIndex++;
	}
	
	/**
	 * @return the number of notifiers of this match
	 */
	int getNumberNotifiers(){
		return notifiers.size();
	}
	
	/**
	 * @return the current player
	 */
	Player getCurrentPlayer(){
		return turns[turnIndex].getCurrentPlayer();
	}
	
	/********************END GETTERS AND SETTERS ***********************************/

	
	
	/*********************ACTIONS CALLED BY THE CLIENTS*******************************/
	
	/* (non-Javadoc)
	 * @see Server.networking.rmi.RemoteMatch#setPosition(java.lang.String, java.lang.String)
	 */
	@Override
	public synchronized boolean setPosition(String username,String sector){
		action=new ActionPosition(this,username, sector);
		
		return Boolean.parseBoolean(action.execute());
	}
	
	
	/* (non-Javadoc)
	 * @see Server.networking.rmi.RemoteMatch#noise(java.lang.String, java.lang.String)
	 */
	@Override
	public synchronized boolean noise(String username, String sector) throws RemoteException {
		action=new ActionNoise(this,username,sector);
		return Boolean.parseBoolean(action.execute());
	}

	/* (non-Javadoc)
	 * @see Server.networking.rmi.RemoteMatch#drawCard(java.lang.String)
	 */
	@Override
	public synchronized String drawCard(String username){
		action=new ActionDrawCard(this, username);
		
		return action.execute();
		
	}
	
	/* (non-Javadoc)
	 * @see Server.networking.rmi.RemoteMatch#attack(java.lang.String)
	 */
	@Override
	public synchronized boolean attack(String username) {
		action=new ActionAttack(this,username);
		
		return Boolean.parseBoolean(action.execute());
		
	}
	
	/* (non-Javadoc)
	 * @see Server.networking.rmi.RemoteMatch#changeTurn(java.lang.String)
	 */
	@Override
	public synchronized boolean changeTurn(String username){
		action=new ActionChangeTurn(this,username);
		if(action.check())
			return Boolean.parseBoolean(action.execute());
		else 
			return false;
	}
	
	
	/**********************END ACTIONS *******************************************/
	
	/**********************START AND TERMINATE MATCH****************************/
	
	/**
	 * This method performs all the necessary actions to start the match
	 * A Map<String,String> is sent to clients:
	 * key: "action", value: "startGame"
	 * key:"param"+i, value: username of the player
	 * where i starts from 1
	 */
	public void start(){
		playing=true;
		//All Notifier start
		Collection<ServerToClientNotifier> c=notifiers.values();
		for(ServerToClientNotifier notifier:c)
			notifier.startConnection();
		
		match.start();
		numberPlayers=match.getNumberPlayers();
		//create and assign characters to the players randomly 
		assignCharacters();
		//create the first turn
		turnIndex=-1;
		
		
		//Send to notifiers that the match started with all the usernames of the match
		Map<String,String> startGame=new HashMap<String,String>();
		startGame.put("action", "startGame");
		
		Integer i=1;
		for(Player p:match.getPlayers()){
			startGame.put("param"+i.toString(), p.getUsername());
			i++;
			
		}
		
		notifyObservers(startGame);
		
		//Start the first turn
		Action changeTurn=new ActionChangeTurn(this,match.getPlayers().get(0).getUsername());
		changeTurn.execute();
	}
	

	
	
	/**
	 * This method performs all the necessary actions to terminate the match,
	 * close all the notifiers, and disconnect all the users of the match.
	 * The following data in a Map<String,String> is sent to the players:
	 * key: "action", value "matchEnd"
	 * key "param"+i, value username of the i winner
	 * key "turn"+i value username1=position&username2=position&....
	 * where i is a number starting from 1
	 */
	synchronized void matchTerminated(){
		timeout.interrupt();
		playing=false;
		ended=true;
		//Notify the client that the match is ended, sending also the winners
		Map <String,String> matchEnd =new HashMap<String,String>();
		matchEnd.put("action", "matchEnd");
		matchEnd.put("username", "system");
		Integer i=1;
		Iterator<Player> iterator=match.getWinners().iterator();
		while (iterator.hasNext()){
			matchEnd.put("param"+i.toString(),iterator.next().getUsername());
			i++;
		}
		
		//Send the data of the match
		for(int j=0;j<turns.length;j++){
			if(turns[j]==null)
				break;
			
			List<Player> turnPlayers=turns[j].getPlayers();
			String positions="";
			for(Player p:turnPlayers){
				String sectorName;
				Sector sector=turns[j].getSectorOf(p);
				if(sector==null)
					sectorName="noPosition";
				else
					sectorName=sector.toString();
				
				positions=positions+p.getUsername()+"="+sectorName+"&";
			}
			matchEnd.put("turn"+Integer.toString(j+1), positions);
		}
		
		notifyObservers(matchEnd);
		
		//delete Observers as the match is terminated
		this.deleteObservers();
		Collection<ServerToClientNotifier> c=notifiers.values();
		Set<String> usernames=notifiers.keySet();
		List<String> names=new ArrayList<String>();
		for(String username:usernames)
			names.add(username);
		
		for(ServerToClientNotifier notifier: c){
			notifier.closeNotifier();
			
		}
		
		storeScores();
		//Logout players from the game
		for(String username:names){
			notifiers.remove(username);
			match.delPlayer(Player.players.get(username));
			Player.players.remove(username);
		}
	}
	
	
	/**
	 * @return if the current status of the match is compatible with the 
	 * victory of one or more players
	 * 
	 */
	public boolean checkEndGame(){
		int aliens=0;
		int humans=0;
		for(Player p:match.getPlayers()){
			if(p.getCharacter() instanceof Alien)
				aliens++;
			if(p.getCharacter() instanceof Human)
				humans++;
		}
		if(aliens+humans==1){
			match.getWinners().add(match.getPlayers().get(0));
			
			matchTerminated();
			return true;
		}
		if(humans==0||aliens==0){
			for(Player p:match.getPlayers())
				match.getWinners().add(p);
			
			matchTerminated();
			return true;
		}
		return false;
	}
	
	/*************************END START AND TERMINATE MATCH**********************/
	
	/**********************METHODS RELATED TO PLAYERS***************************/
	
	/**
	 * This method is called when a player do not perform his turn within
	 * the turn timeout and restore his position to the one of the previous turn 
	 *  
	 */
	public synchronized void reverseTurn(){
		Sector reverseSector;
		if(turnIndex!=0){
			reverseSector=turns[turnIndex-1].getSectorOf(getCurrentPlayer());
		}else{
			reverseSector=match.getMap().startSector(getCurrentPlayer().getCharacter().getClass().getSimpleName());
		}
		getCurrentPlayer().getCharacter().setSector(reverseSector);
		turns[turnIndex].setNewSector(reverseSector);
		Map<String,String> reverse=new HashMap<String,String>();
		reverse.put("action", "reverseTurn");
		reverse.put("username", getCurrentPlayer().getUsername());
		notifyObservers(reverse);
		
		action=new ActionChangeTurn(this,getCurrentPlayer().getUsername());
		action.execute();
	}
	
	
	@Override
	public String giveMeCharacter(String username){
		Player player=Player.players.get(username);
		return player.getCharacter().toString();
	}
	
	/**
	 * This method assigns characters to the players according
	 * to the game rules
	 */
	private void assignCharacters(){
		List<GameCharacter> characters=new ArrayList<GameCharacter>();
		int nPlayers=numberPlayers;

		if ((nPlayers%2)==1){
			characters.add(new Alien(match.getMap().startSector("alien")));
			nPlayers--;
		}
		
		for(int i=0; i<(nPlayers/2);i++){
			characters.add(new Human(match.getMap().startSector("human")));
			characters.add(new Alien(match.getMap().startSector("alien")));
		}
		
		for(Player p: match.getPlayers()){
			Collections.shuffle(characters);
			p.setCharacter(characters.get(0));
			characters.remove(0);
		}
	}
	 //Overloading: call disconnectUser passing the current player
	public synchronized boolean disconnectUser(){
		disconnectUser(getCurrentPlayer().getUsername());
		return true;
	}
	
	/**
	 * This method disconnects one user. It is called when the notifier
	 * of the player is not able to send messages to him
	 * @param username of the player that will be disconnected
	 */
	public synchronized void disconnectUser(String username){

		Player player=Player.players.get(username);
		
		
		notifiers.remove(username);
		delPlayer(player);
		
	}
	
	/**
	 * 
	 * Delete the character killed form the match  and notify the client the all client
	 * @param player
	 */
	 void delPlayer(Player player){
		 
		 player.setIncrementScore(false, turnIndex+1);
		 
		 Login login=Login.getInstance();
		 login.storeScores(player.getUsername());
		 
		match.delPlayer(player);
		numberPlayers--;
		//Notify that the player was deleted from the match
		
		Map<String,String> params=new HashMap<String,String>();
		params.put("username", player.getUsername());
		params.put("action", "deleted");
		notifyObservers(params);
		
	}


	
	/**********************END PLAYERS METHODS************************************/
	
	
	/**
	 * This method is used to implement the Observer pattern
	 * 
	 * @param notifiers
	 */
	public void addObserverNotifier(String username,ServerToClientNotifier notifier){
		
		this.notifiers.put(username, notifier);
	}
	
	
	@Override
	public synchronized void notifyObservers(Object param) {
		Collection<ServerToClientNotifier> c=notifiers.values();
		List<ServerToClientNotifier> aux=new ArrayList<ServerToClientNotifier>();
		for(ServerToClientNotifier n : c){
			aux.add(n);
		}
		for(ServerToClientNotifier o : aux){
			try{
				o.update(this, param);
			}catch(DisconnectedUserException DUe){
				if(checkEndGame())
					break;
			}
		}		

	}

	/**
	 * Store the scores of the players when the match is over
	 */
	private void storeScores(){
		Login login=Login.getInstance();
		for(Player p:match.getPlayers()){
			if (match.getWinners().contains(p))
				p.setIncrementScore(true, turnIndex+1);
			else
				p.setIncrementScore(false, turnIndex+1);
			
			login.storeScores(p.getUsername());
		}
	}
	



}
