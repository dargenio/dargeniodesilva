package it.polimi.ingsw.DargenioDeSilva;


/**
 * @author Ruggiero Dargenio
 * This is an abstract class. Actions of the game inherits from it.
 */
public abstract class Action {
	protected String username;
	protected MatchController matchController;


	/**
	 * This method checks if at the moment when the player wants to perform
	 * the action, the turn is in the correct status
	 * @return true if it is possible to perform the action, false otherwise
	 */
	protected abstract boolean check();

	/**
	 * @return true if it has been possible to perform the action, false if
	 * one problem occured
	 */
	public abstract String execute();

	/**
	 * @param username
	 * @return if the current turn is of player with username : username
	 */
	protected boolean isMyTurn(String username){
		if (matchController.getCurrentPlayer().getUsername().equals(username))
			return true;
		return false;
	}

	/**
	 * @return the index of the current turn
	 */
	protected int getTurnIndex(){
		return matchController.getTurnIndex();
	}
}
