package it.polimi.ingsw.DargenioDeSilva;

import java.io.File;
import java.io.BufferedWriter; 
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import exceptions.NotNetworkInterfaceException;
import exceptions.NotPlayerException;
import Server.networking.notifier.ServerToClientNotifier;
import Server.networking.notifier.ServerToClientNotifierFactory;
import Server.networking.rmi.RemoteLogin;


/**
 * This is the entity responsible for managing sign up,login and all the requests
 * before the match for the player starts.
 * @author Ruggiero Dargenio
 *
 */
public class Login implements RemoteLogin {
	private int port=ConfigurationClass.getStartReverseCommunication();
	private static Login instance=null;
	ScoreManagement score=new ScoreManagement();
	private Logger log=Logger.getLogger("log");
	
/*************************CONSTRUCTOR********************************************/
	/**
	 * @return the unique instance of Login
	 */
	public static Login getInstance(){
		if(instance==null) instance=new Login();
			return instance;
	}

	
/*******************METHODS CALLED BY THE CLIENT*******************************/
	/* (non-Javadoc)
	 * @see Server.networking.rmi.RemoteLogin#signUp(java.lang.String, java.lang.String)
	 */
	@Override
	public synchronized String signUp (String username, String password) throws RemoteException  {
	
		
		try{
			if (searchUser(username)==null) {
				writeUser(username,password); 
				return "created"; 
			}
			else 
				return "invalidUsername"; 
		}
		
		catch (RemoteException e)
		{	log.log(Level.SEVERE, "RemoteException in signUp. Message:"+e.getMessage());
			return null; 
			}
		catch(IOException ex){
			log.log(Level.SEVERE, "IOException in signUp. Message:"+ex.getMessage());
			return null;
		}
	}
	
	@Override
	public synchronized Map<String,String> login (String username, String password, String preferredMap) {
		Map<String,String> result=new HashMap<String,String>();

		try{
			if (searchUser(username)!=null) {
				String temp[] = searchUser(username); 
				String pw=temp[1]; 
				if (pw.equals(getHashOf(password))){

					if(Player.players.get(username)!=null){
						result.put("action", "alreadyLogged");
						return result;
					}
					else{
						try {
							loadPlayer(temp);
							String match=getMatch(username,preferredMap);
								
								
							String port=Integer.toString(getNewPort());
							MatchManager mg=MatchManager.getInstance();
							MatchController matchController=mg.getMatchController(Integer.parseInt(match));
							
							result.put("action", "logged");
							result.put("port",port);
							result.put("matchID", match);
							
							result.put("map", matchController.getMatch().getMap().toString());
							return result; 

						} catch (NotPlayerException e) {
							log.log(Level.SEVERE,":Player non found after the login");
							
						}
					}

				}
				else{
					result.put("action", "wrongPassword");
					return result; 
				}
			
			}else{
				result.put("action", "notExistentUser");
				return result;
			}

		}catch(IOException IOe){
			log.log(Level.SEVERE,IOe.getMessage()+IOe.getStackTrace());
			
			return result;
		}
		result.put("action", "notExistentUser");
		return result;
		
	}
	
	/**
	 * This method return a new match if the previous match is full or already started
	 * or add the player to the match that is waiting for players yet
	 * @param username of the new player
	 * @param preferredMap of the player
	 * @return the ID that will be used for RMI or socket to open the opposite communication
	 * @throws NotPlayerException if the player is not present in the active players
	 * which means that something went wrong during the login phase
	 */
	private String getMatch(String username,String preferredMap)throws NotPlayerException{
	MatchManager mg=MatchManager.getInstance();
	
	if(Player.players.get(username)==null)
		throw new NotPlayerException();
	
	MatchController match=mg.getMatch(Player.players.get(username), preferredMap);
	Integer id=match.getMatch().getID();
	
	
	return id.toString();
}
	
	/* (non-Javadoc)
	 * @see Server.networking.rmi.RemoteLogin#giveMeNotifier(java.lang.String, java.lang.String)
	 */
	@Override 
	public void giveMeNotifier(String address,String username){
		linkNotifier(address,username,"RMI");
	}
	
	/* (non-Javadoc)
	 * @see Server.networking.rmi.RemoteLogin#getRanking()
	 */
	@Override
	public Map<String,String> getRanking(){
		return score.getRanking();
	}
	/*********************END METHODS CALLED BY THE CLIENT*****************************/
	
	
	

/**********************UTILITIES FOR FILES***********************************/
	/**
	 * 
	 * @param username of the player to write
	 * @param password of the player
	 * @return true if the data has been successfully written in the file
	 * @throws IOException is problems about the file occur
	 */
	private boolean writeUser(String username, String password) throws IOException {

		try{
			File usersFile = new File("file/PlayersLogin.txt");
			FileWriter fw=new FileWriter(usersFile, true);
			BufferedWriter bw =new BufferedWriter(fw);

			bw.write("1,"+username+','+getHashOf(password)+'\n');
			bw.flush();
			bw.close(); 




			fw=new FileWriter(new File("file/PlayersData.txt"),true);
			bw =new BufferedWriter(fw);
			String toWrite=username+","+"0"+","+"0"+","+"0"+",";
			int length=toWrite.length();
			for(int i=0;i<30-length;i++)
				toWrite+=" ";
			
			toWrite+='\n';
			
			bw.write(toWrite);
			bw.flush();
			bw.close();

		}catch(IOException e){
			log.log(Level.SEVERE, "Error opening files:"+e.getStackTrace());
		}
		return false; 
	}



		/**
		 * Search the player with username: username in the file and return all the data
		 * @param username of the player to find
		 * @param password of the player
		 * @return data of the player (username,password,number matches,number victories,number played turns)
		 * @throws IOException 
		 */
	String[] searchUser(String username) throws IOException {
		try {
			int flag; 
			String line; 
			String result[]=new String[5];
			
			File usersFile = new File("file/PlayersLogin.txt");
			Scanner in=new Scanner (usersFile); 

			while(in.hasNextLine()) {

				line=(in.nextLine()); 
				String[] parts=line.split(",");
				flag=Integer.parseInt(parts[0]); 

				if (flag!=0){
					if (parts[1].equals(username)) {
						in.close();

						File scoreFile = new File("file/PlayersData.txt");
						Scanner readData=new Scanner (scoreFile);
						while(readData.hasNextLine()) {

							line=(readData.nextLine()); 
							String[] scores=line.split(",");
							if(scores[0].equals(username)){
								result[0]=username;
								result[1]=parts[2];
								result[2]=scores[1];
								result[3]=scores[2];
								result[4]=scores[3];
								

							}
						}
						readData.close();
						return result; 
					}

				}
			}
			in.close();
			return null;


		} 
		catch(IOException e){
			log.log(Level.SEVERE,"Errore in apertura del file"+e.getStackTrace());
			return null;
		}



	}
	
	
	/**
	 * Generate a new player and add him to the global list of players
	 * in the server
	 * 
	 * @param data of the player: data[0]=username , data[1]=password,following data contains the scores 
	 * 
	 */
	private void loadPlayer(String[] data){
		Player p=new Player(data);
		Player.players.put(data[0],p);
	}

	/************************END FILE UTILITIES****************************************/	

	/**
	 * @return the next port avaliable for the notifier communication
	 */
	private int getNewPort(){
		port++;
		return port;
	}
	
	
	/**
	 * Create and link a notifier with the choses network interface to the player
	 * @param address of the client
	 * @param username of the player
	 * @param networkInterface chosen for the communication
	 */
	public synchronized void linkNotifier(String address,String username,String networkInterface) {
		try{
			ServerToClientNotifier notifier=ServerToClientNotifierFactory.getNotifier(address,networkInterface, port,username);
			
			
			MatchManager mg=MatchManager.getInstance();
			mg.addObserverNotifier(username, notifier);
			
		}catch(NotNetworkInterfaceException ex){
			log.log(Level.SEVERE, ":Tried to create a network interface not existent. Message:"+ex.getMessage());
			
		}
		
	}
	


	/**
	 * This class calculate the SHA-256 hash of the string passed as a parameter
	 * @param input, original string
	 * @return SHA-256 hash of the string passed as a parameter
	 */
	public String getHashOf(String input)

    {
		try{
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.reset();
        byte[] buffer = input.getBytes();
        md.update(buffer);
        byte[] digest = md.digest();

        String hexStr = "";
        for (int i = 0; i < digest.length; i++) {
            hexStr +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
        }
    
        return hexStr;
		}catch(NoSuchAlgorithmException ex){
			return "";
		}
    }
	
	/**
	 * Store the scores of the player in the PlayersData.txt file
	 * @param username of the player
	 */
	synchronized void storeScores(String username){
		score.storeScores(username);
	}
	


}
