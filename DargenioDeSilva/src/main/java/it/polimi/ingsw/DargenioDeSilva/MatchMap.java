package it.polimi.ingsw.DargenioDeSilva;
import java.util.*;
import java.io.*;

import exceptions.NotMapException;
import abstractObjects.Sector;

/**
 * This class represents a map in the game. A map contains all the sectors
 * of a map
 * @author Ruggiero Dargenio
 *
 */
public class MatchMap {
	Map<String,Sector> map=new HashMap<String,Sector>();
	private String name;


	public Sector get(String sector){
		return map.get(sector);
	}
	
	public MatchMap(String name) throws NotMapException{
		this.name=name;
		MapManager mapManager=MapManager.getInstance();
		try{
			if (mapManager.getFile().get(name)==null)
				throw new NotMapException(name);
			
			inizializeMap(mapManager.getFile().get(name));
		}	
		catch(IOException e ){
			System.out.println(e.getStackTrace());
		}
	}

	@Override
	public String toString(){
		return name;
	}


	private synchronized void inizializeMap(String path) throws IOException{
		try{
			File fileMap = new File(path);
			Scanner in= new Scanner(fileMap);
			
			char x;
			Integer y;
			char type;
			
			while(in.next().equals(";")){
				Sector s;
				
				x=in.next().charAt(0);
				y=Integer.parseInt(in.next());
				type=in.next().charAt(0);
				
				
				s=SectorFactory.sectorFactory(x,y,type);
				String key=s.toString();
				
				map.put(key,s);
			}
			in.close();
		}
		catch(IOException e){
			System.out.println("Errore in apertura del file"+e.getStackTrace());
			
		}
		
	}

	/**
	 * This method returns the list of the sector not-blocked near the sector 
	 * passed as parameter.
	 * @param s instance of sector
	 * @return the list of the sectors
	 */
	List<Sector> getNearSectors (Sector s){
		
		int pariDispari;
		char callerX; 
		int callerY;  
		
		
		if (s.getX()%2==0)
			pariDispari=1;//pari
		else
			pariDispari=-1;//dispari
		
		List<Sector> nearSectors= new ArrayList<Sector>(); 
		
		for (int i=-1; i<=1; i=i+2){
			
			callerX=s.getX(); 
			callerY=s.getY(); 
			
			callerY=callerY+i; 
			String key=callerX+Integer.toString(callerY); 
			
			if ((callerY>=1) && (callerY<=14))//nel caso ci si trovi nella prima (ultima) riga
			nearSectors.add(this.get(key));	//scarta il settore con coordinata Y=0 (Y=15)
			
				
			}
		for (int i=-1; i<=1; i=i+2){
			
			callerX=s.getX(); 
			callerY=s.getY(); 
			
			callerX= (char) ( (int) (callerX+i) ) ;
			String key=callerX+Integer.toString(callerY);
			
			if ( (int) callerX >=65 &&  (int) callerX <=87 ) //nel caso ci si trovi nella prima (ultima) colonna
			nearSectors.add(this.get(key)); //scarta il settore con coordinata X<"A" (X>"W") 
				
			}
		for (int i=-1; i<=1; i=i+2){
			
			callerX=s.getX(); 
			callerY=s.getY(); 
			
			callerX= (char) ( (int) callerX+i); 
			callerY= callerY+pariDispari; 
			String key=callerX+Integer.toString(callerY); 
			

			if ( (int) callerX>=65 && ((int)callerX)<=87 && (callerY>=1) && (callerY<=14))
			nearSectors.add(this.get(key));
			}
		return nearSectors; 		
	} 
	
	
	/**
	 * Return the starting sector in the map for the character passed as parameter
	 * @param character: human or alien
	 * @return the starting sector in the map for the character passed as parameter
	 */
	public Sector startSector(String character){
		AlienSector as=null;
		HumanSector hs=null;
		
		Collection<Sector> c=map.values();
		for(Sector s: c){
			if(s instanceof AlienSector)
			as=(AlienSector)s;
	
		if(s instanceof HumanSector)
			hs=(HumanSector)s;
		}
		

		if(character.equalsIgnoreCase("human"))
			return hs;
		else
			return as;

	}

}
	
	
