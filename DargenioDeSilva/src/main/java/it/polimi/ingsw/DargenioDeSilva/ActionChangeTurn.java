package it.polimi.ingsw.DargenioDeSilva;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ruggiero Dargenio
 * This class is responsible for the change of the turn. Checks if it is possible to
 * shift turn and in that case performs it.
 */
public class ActionChangeTurn extends Action {

	private TurnTimeout timeout;

	/**
	 * Constructor
	 * @param mc instance of matchController
	 * @param username of the player
	 */
	ActionChangeTurn(MatchController mc,String username){
		this.matchController=mc;
		this.username=username;

	}

	/* (non-Javadoc)
	 * @see it.polimi.ingsw.DargenioDeSilva.Action#check()
	 */
	@Override
	protected boolean check() {
		if(isMyTurn(username)&&matchController.getTurns()[getTurnIndex()].isPossibleEnd())
			return true;

		return false;
	}

	/* (non-Javadoc)
	 * @see it.polimi.ingsw.DargenioDeSilva.Action#execute()
	 */
	@Override
	public String execute() {

		if(getTurnIndex()==Turn.getMaxTurn()-1 && !matchController.getTurns()[getTurnIndex()].isThereMove()){
			for (Player player: matchController.getMatch().getPlayers()){
				if (player.getCharacter() instanceof Alien)
					matchController.getMatch().setWinner(player);
			}
			matchController.matchTerminated();

		}else{
			if(getTurnIndex()==-1){

				matchController.incrementTurnIndex();
				matchController.getTurns()[getTurnIndex()]=new Turn(matchController.getMatch().getPlayers());
				this.timeout=new TurnTimeout(matchController);
				matchController.setTimeout(timeout);
				timeout.start();
			}else{

				matchController.getTimeout().interrupt();


				if (matchController.getTurns()[getTurnIndex()].isThereMove()){
					matchController.getTurns()[getTurnIndex()].nextPlayer();
					this.timeout=new TurnTimeout(matchController);
					matchController.setTimeout(timeout);
					timeout.start();
				}else{

					matchController.incrementTurnIndex();
					matchController.getTurns()[getTurnIndex()]=new Turn(matchController.getMatch().getPlayers());
					this.timeout=new TurnTimeout(matchController);
					matchController.setTimeout(timeout);
					timeout.start();

				}
			}
		}

		Map <String,String> changeTurn =new HashMap<String,String>();
		changeTurn.put("action", "changeTurn");
		changeTurn.put("username",matchController.getCurrentPlayer().getUsername() );
		matchController.notifyObservers(changeTurn);

		return "true";

	}

}
