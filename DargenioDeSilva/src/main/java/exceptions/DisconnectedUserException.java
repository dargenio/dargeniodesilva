package exceptions;

/**
 * @author Ruggiero Dargenio
 * This exception is thrown when a client disconnects during a match (voluntarily or 
 * by accident).
 *
 */
public class DisconnectedUserException extends Exception {


	private static final long serialVersionUID = -1330759569478546985L;

	public DisconnectedUserException(String username) {
		super(username+" disconnected during one match");
		
	}
	
}
