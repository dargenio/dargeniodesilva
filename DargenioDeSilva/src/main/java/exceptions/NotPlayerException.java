package exceptions;

/**
 * @author Ruggiero Dargenio
 * This exception should be thrown when someone looks for an association between 
 * an username not loaded in the system or not existent and his instance of player.
 *
 */
public class NotPlayerException extends Exception {

	private static final long serialVersionUID = -8787050787142238994L;

	public NotPlayerException(){
		super("Username inserted is not present in the list of players connected");
	}
}
