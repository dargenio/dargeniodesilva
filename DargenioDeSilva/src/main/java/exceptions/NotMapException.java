package exceptions;

/**
 * @author Ruggiero Dargenio
 * This exception should be thrown when someone tries to create a map with a name
 * that does not exists in the system.
 *
 */
public class NotMapException extends Exception {
	

	private static final long serialVersionUID = -2591994504695674855L;

	public NotMapException(String name){
		super("The map "+name+" has been looked up but there is no map with this name");
	}
}
