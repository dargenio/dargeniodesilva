package exceptions;


/**
 * @author Ruggiero Dargenio
 * This exception is thrown if a socket client asks for a command not supported.
 *
 */
public class NotCommandException extends Exception {

	private static final long serialVersionUID = 3466512462712709115L;

	public NotCommandException(String username,String command){
		super("Il giocatore "+username+" ha cercato di invocare il comando "+command+
				" ma non è presente tra quelli disponibili");
	}
	
	public NotCommandException(String command){
		super("Comando "+command+
				" ma non è presente tra quelli disponibili");
	}
}
