package exceptions;

/**
 * @author Ruggiero Dargenio
 * This exception should be thrown when a socket client asks for a command
 * without sending all the fields required by that specific command.
 *
 */
public class NotValidCommandFormatException extends Exception {
	
	private static final long serialVersionUID = -2936480034040542653L;

	public NotValidCommandFormatException(){
		super("Command with an invalid format has been sent by a client");
	}
}
