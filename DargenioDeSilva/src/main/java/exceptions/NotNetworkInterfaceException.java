package exceptions;

/**
 * @author Ruggiero Dargenio
 * This exception should be thrown whene an interface different from
 * the supported one (RMI and socket) is created.
 *
 */
public class NotNetworkInterfaceException extends Exception{

	private static final long serialVersionUID = -4376487816111477782L;

	public NotNetworkInterfaceException(){
		super("The ServerToClientNotifierFactory has received an interface not valid");
	}
}
