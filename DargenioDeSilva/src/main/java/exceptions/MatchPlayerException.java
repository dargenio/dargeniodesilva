package exceptions;

/**
 * @author Ruggiero Dargenio
 * This exception is thrown when a someone tries to know the match of a player but 
 * no match cointains this player.
 *
 */
public class MatchPlayerException extends Exception{

	private static final long serialVersionUID = -2913162533598628719L;

	public MatchPlayerException(String username){
		super("Player"+username+" asked to know his match "
				+ "but not match has as player the requier");
	}
}
