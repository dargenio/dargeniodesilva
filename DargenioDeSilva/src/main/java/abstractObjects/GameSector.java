package abstractObjects;


public abstract class GameSector extends Sector {

	
	public GameSector(char x,int y,boolean b){
		super(x,y,b);
	}
	
	public GameSector(char x,int y){
		super(x,y);
	}
}
