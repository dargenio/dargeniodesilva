package abstractObjects;


/**
 * @author Ruggiero Dargenio
 * An instance of this class is a sector of the map of a match
 */
public abstract class Sector{

	protected char x;
	protected int y;
	boolean blocked;

	public boolean isBlocked() {
		return blocked;
	}

	protected boolean locked=false;
	
	public Sector(char x,int y){

		this(x,y,false);
	}
	
	public Sector(char x,int y,boolean b){
		this.x=x;
		this.y=y;
		if (b==true)
			this.blocked=true;
		else
			this.blocked=false;
	}
	
	public static boolean isValid(String sector){
		if (sector.length()>3) return false;
		char x=sector.charAt(0);
		if (x<'A' || x>'W')
			return false;
		
		if (sector.length()==3){
			try{
				int y=Integer.parseInt(sector.substring(1, 3));
				if(y>=1 && y<=14)
					return true;
				else return false;
			}catch(NumberFormatException ex){
				return false;
			}
			
		}else{
			try{
				int y=(sector.charAt(1))-48;
				if (y>=1 && y<=9)
					return true;
			}catch(NumberFormatException ex){
				return false;
			}
		}
		return false;
	}
	

	@Override
	public String toString(){
		return x+Integer.toString(y);
	}


	public int getY() {
		return y;
	}
	
	
	public char getX() {
		return x; 
	}
	
	@Override
	public int hashCode(){
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object s){
		if (!(s instanceof Sector)) return false;
		
		Sector sector=(Sector)s;
		if(sector.getX()==this.getX()&&sector.getY()==this.getY()) return true;
		return false;
	}


}