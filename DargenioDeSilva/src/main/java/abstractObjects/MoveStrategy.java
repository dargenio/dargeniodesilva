package abstractObjects;

import it.polimi.ingsw.DargenioDeSilva.AlienSector;
import it.polimi.ingsw.DargenioDeSilva.HumanSector;
import it.polimi.ingsw.DargenioDeSilva.MatchMap;


public abstract class MoveStrategy {
	
	public abstract boolean validMove (MatchMap m, Sector start, Sector finish); 
	
	public abstract boolean validAttack();
	 
	protected boolean checkConditions(Sector sector){
		if (!sector.isBlocked() && !(sector instanceof HumanSector) && !(sector instanceof AlienSector))
			return true; 
		else return false; 
	
	}
}	
