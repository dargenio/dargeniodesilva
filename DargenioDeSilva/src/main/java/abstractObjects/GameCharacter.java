package abstractObjects;

import it.polimi.ingsw.DargenioDeSilva.MatchMap;


/**
 * @author Ruggiero Dargenio
 * This is an abstract that contains all the common methods of human and alien classes 
 *
 */
public abstract class GameCharacter {

	protected Sector sector;
	protected MoveStrategy move; 

	public MoveStrategy getMove() {
		return move;
	}

	/**
	 * @return Sector
	 */
	

	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}
	
	
	
	public boolean setPosition(MatchMap m, Sector s){
		if (this.move.validMove(m, this.getSector(), s)){
			setSector(s); 
			return true;
			}
			
		return false;		
			
	}
	
	
	@Override
	public String toString(){
		return this.getClass().getSimpleName().toLowerCase();
	}

	
}


