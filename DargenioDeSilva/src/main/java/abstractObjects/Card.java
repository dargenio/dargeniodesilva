package abstractObjects;
import it.polimi.ingsw.DargenioDeSilva.Turn;

public abstract class Card {
	public abstract boolean isValidNoise(Sector mine,Sector noyse);
	
	@Override
	public String toString(){
		return this.getClass().getSimpleName();

	}
}
