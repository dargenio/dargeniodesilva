package Client.command;

import java.util.Map;

/**
 * @author Ernesto De Silva
 * Abstract class with "action" method inherited by commands.
 */
public abstract class ClientCommand {
	public abstract void action(Map<String,String> param);
}
