package Client.command;

import exceptions.NotCommandException;

public class ClientCommandFactory {
	/**
	 * Factory method to generate the command indicated by the parameterized string
	 * @param action
	 * @throws NotCommandException
	 */
	public static ClientCommand clientCommandFactory(String action)throws NotCommandException{

		switch(action){
		
		case("startGame"):{
			return new StartGameCommand(); 
		}

		case ("noise"):{
			return new NoiseCommand();
			
		}

		case("deleted"): {
			return new PlayerDeletedCommand(); 
		}


		case ("notifySetPosition"): {
			return new PositionChangedCommand(); 
		}
		
		case ("notifyCard"): {
			return new CardCommand(); 
		}
		
		case("attack"):{
			return new AttackCommand(); 
		}

		case("changeTurn"):{
			return new ChangeTurnCommand(); 
		}
		
		case("matchEnd"):{
			return new EndMatchCommand();
		}
		
		case("reverseTurn"):{
			return new ReverseTurn();
		}
	
		
	}
		throw new NotCommandException(action); 
	
	}

}

