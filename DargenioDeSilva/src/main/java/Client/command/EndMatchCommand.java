package Client.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Client.controller.GameController;

/**
 * @author Ernesto De Silva
 * This command create one map containing the movements (key: turn, value: movements)
 * and a list of string containing the winners. It calls the endMatch method
 * from gameController.
 */
public class EndMatchCommand extends ClientCommand {

	@Override
	public void action(Map<String,String> param) {
		int i=1;
		List<String> winners= new ArrayList<String>();
		Map<String,String> movements= new HashMap<String,String>(); 
		GameController gameController=GameController.getInstance(); 
		while(param.get("param"+Integer.toString(i))!=null) {
			String key="param"+Integer.toString(i);
			winners.add(param.get(key));
			i++;
			
		}
		i=1; 
		while (param.get("turn"+Integer.toString(i))!=null) {
			String key="turn"+Integer.toString(i); 
			String value=param.get(key).replace("&", " "); 
			movements.put(key, value); 
			i++; 
		}
		
		gameController.endMatch(winners, movements);
	}

}
