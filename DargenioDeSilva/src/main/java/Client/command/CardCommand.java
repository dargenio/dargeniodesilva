package Client.command;


import java.util.Map;


import Client.controller.GameController;
/**
 * @author Ernesto De Silva 
 * This command calls the setMessage method from gameController and set the
 * text field of the the username (value contained in the map with key "username").  
 *
 */
public class CardCommand extends ClientCommand {

	@Override
	public void action(Map<String,String> param){
		GameController gameController=GameController.getInstance(); 
		gameController.setMessage(param.get("username"), "drawn a card", null);
	}
}
