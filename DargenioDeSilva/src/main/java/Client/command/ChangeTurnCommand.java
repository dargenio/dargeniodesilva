package Client.command;


import java.util.Map;

import Client.controller.GameController;

public class ChangeTurnCommand extends ClientCommand {
	
	/**
	 * @author Ernesto De Silva 
	 * This command calls the changeTurn method from gameController.
	 * Yhe param is the value contained in the map with key "username".  
	 *
	 */
	@Override
	public void action(Map<String,String> param) {
		GameController gameController=GameController.getInstance(); 
		gameController.changeTurn(param.get("username"));

	}

}
