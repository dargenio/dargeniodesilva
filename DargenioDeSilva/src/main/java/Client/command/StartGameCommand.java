package Client.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import Client.controller.GameController;

/**
 * @author Ernesto De Silva
 * This command calls the startMatch method from gameController.
 * The parameterized map container the name of the players for this match.
 */
public class StartGameCommand extends ClientCommand {

	@Override
	public void action(Map<String,String> param) {
		GameController gameController=GameController.getInstance();
		List<String> players= new ArrayList<String>();
		int i=1;
		while(param.get("param"+Integer.toString(i))!=null) {
			String key="param"+Integer.toString(i);
			players.add(param.get(key));
			i++;
			
		}
		gameController.startMatch(players);
		
		

	}

}
