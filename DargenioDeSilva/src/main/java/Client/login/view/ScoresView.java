package Client.login.view;


import java.awt.Color;

import java.awt.Container;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @author Ernesto De Silva
 * Class containing graphic elements of the view containing ranking.
 */
public class ScoresView extends DefaultTableCellRenderer {
	
	private static final long serialVersionUID = 3681744583608132133L;
	private JFrame frame=new JFrame("SCORES"); 
	private Container container=new Container();
	private JLabel bgScores=new JLabel(new ImageIcon("images/bgScore.jpg")); 


	Object[] columnNames= {"", "", "", ""}; 

	public ScoresView(){
		
	frame.setBounds(0,0,900, 675);
	bgScores.setBounds(0,0,900,675); //background scores view
	container.setBounds(0,0,900,675); 
	container.setLayout(null);
	frame.setResizable(false);
	frame.setVisible(false);
	}
	
	public void loadAndShowTable(final Object[][] data){
		SwingUtilities.invokeLater(new Runnable() {
			public void run(){	
			JTable scoreTable=new JTable (data, columnNames); //JTable containing scores
			scoreTable.setVisible(true);
			((DefaultTableCellRenderer)scoreTable.getDefaultRenderer(Object.class)).setOpaque(false);
			scoreTable.setOpaque(false);
			scoreTable.setForeground(Color.YELLOW);
			scoreTable.setFont(new Font("Bold", Font.BOLD,20));
			scoreTable.setBounds(50, 300, 950, 500);
			scoreTable.setRowHeight(30);
			scoreTable.setShowGrid(false);
			
			container.add(scoreTable);
			container.add(bgScores); 
			frame.add(container); 
			frame.setVisible(true); 
			}});
		}
	}
