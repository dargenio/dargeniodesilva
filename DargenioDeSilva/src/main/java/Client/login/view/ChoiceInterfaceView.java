package Client.login.view;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * @author Ernesto De Silva
 * Class containing graphic elements of the view where the client chooses the way to connect to
 * the server (RMI or Socket)
 */
public class ChoiceInterfaceView {
	protected JFrame frame=new JFrame("Network Interface");
	protected Container container=new Container(); 
	protected JLabel interfaceChoice = new JLabel("Please choose a Network Interface:");
	protected JButton rmi=new JButton ("RMI");
	protected JButton socket=new JButton("Socket"); 
	protected JLabel bg=new JLabel (new ImageIcon("images/bgNetwork.jpg")); 

	public ChoiceInterfaceView(){

		container.setLayout(null);
		container.setBounds(0,0,300,430);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(0,0,300, 420);
		frame.setResizable(false);

		bg.setBounds(0,0,300,420); //background of this view

		interfaceChoice.setBounds(35, 30, 250, 30); //choice RMI/Socket label
		interfaceChoice.setForeground(Color.WHITE);

		rmi.setBounds(70, 160, 60, 25); //Button RMI
		rmi.setVisible(true);
		socket.setBounds(140, 160, 90, 25); //Button Socket
		
		container.add(interfaceChoice);
		container.add(rmi);
		container.add(socket);
		container.add(bg);
		frame.add(container);

		frame.setVisible(true);
		
		}
		
		public void addRMIButtonListener(ActionListener listenForRMIButton){
			rmi.addActionListener(listenForRMIButton);
		}
		
		public void addSocketButtonListener(ActionListener listenForSocketButton){
			socket.addActionListener(listenForSocketButton);
		}
	
		public void setInvisible(){
			frame.setVisible(false);
		}
		public void setVisible(){
			frame.setVisible(true);
		}
	}
