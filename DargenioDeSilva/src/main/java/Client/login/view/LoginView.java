package Client.login.view;

import javax.swing.*;
import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionListener;

/**
 * @author Ernesto De Silva
 * Class containing graphic elements of the view where the client can log in or sign up.
 */
public class LoginView extends JFrame {

	private static final long serialVersionUID = 8560085470347206210L;
	private JFrame frame=new JFrame("Escape from Aliens: Login or Sign Up");
	private Container form=new Container(); 
	private JLabel username = new JLabel("Username: ");
	private JLabel usernameSignUp =new JLabel ("Username: "); 
	private JTextField tfUsername = new JTextField(15);
	private JPasswordField tfPassword = new JPasswordField (15); 
	private JTextField tfUsernameSignUp = new JTextField(15);
	private JPasswordField tfPasswordSignUp = new JPasswordField (15); 
	private JPasswordField tfConfirmPassword = new JPasswordField (15);
	private JLabel password = new JLabel ("Password: "); 
	private JLabel passwordSignUp = new JLabel ("Password: "); 
	private JLabel confirmPassword = new JLabel ("Confirm Password: ");
	private JComboBox<String> map=new JComboBox<String>(); 
	private JLabel mapchoice=new JLabel("Scegli una mappa"); 
	private JLabel bg=new JLabel (new ImageIcon("images/bgLogin.jpg")); 
	private JButton buttonLogin=new JButton(new ImageIcon("images/loginButton.png")); 
	private JButton buttonSignUp=new JButton(new ImageIcon("images/signUpButton.png")); 
	private JButton scoresButton=new JButton(new ImageIcon("images/highScoresButton.png")); 
	private JOptionPane messagePopUp=new JOptionPane();

	public LoginView(){

		form.setLayout(null);
		form.setBounds(0,0,300,430);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(0,0,600, 430);
		frame.setResizable(false);
		frame.setVisible(false);

		bg.setBounds(0,0,600,430); //background login view
		
		map.addItem("Galilei");
		map.addItem("Galvani"); 
		map.addItem("Fermi");

		username.setBounds(125,1,125,30); //labels
		username.setForeground(Color.WHITE);
		password.setBounds(125,55,125,30);
		password.setForeground(Color.WHITE);
		tfUsername.setBounds(50,25,200,30); //text field for username
		tfUsername.setBackground(new Color(30,27,27));
		tfUsername.setBorder(new LineBorder(new Color(37,19,129))); 
		tfUsername.setForeground(Color.WHITE);
		tfPassword.setBounds(50,80,200,30); //text field for password
		tfPassword.setBorder(new LineBorder(new Color(37,19,129)));
		tfPassword.setBackground(new Color(30,27,27));
		tfPassword.setForeground(Color.WHITE);
		mapchoice.setBounds(100,140,125,30); //combo box containing game's maps
		mapchoice.setForeground(Color.WHITE);
		map.setBounds(50,170,200,30);
		map.setBackground(Color.WHITE);
		buttonLogin.setBounds(100, 210, 100, 30);

		usernameSignUp.setBounds(425,1,125,30); //labels
		usernameSignUp.setForeground(Color.WHITE);
		passwordSignUp.setBounds(425,55,125,30);
		passwordSignUp.setForeground(Color.WHITE);
		confirmPassword.setBounds(400, 140, 125, 30);
		confirmPassword.setForeground(Color.WHITE);
		tfUsernameSignUp.setBounds(350,25,200,30); //text field for username in signUp
		tfUsernameSignUp.setBackground(new Color(30,27,27));
		tfUsernameSignUp.setBorder(new LineBorder(new Color(37,19,129)));
		tfUsernameSignUp.setForeground(Color.WHITE);
		tfPasswordSignUp.setBounds(350,80,200,30);
		tfPasswordSignUp.setBackground(new Color(30,27,27));
		tfPasswordSignUp.setBorder(new LineBorder(new Color(37,19,129)));
		tfPasswordSignUp.setForeground(Color.WHITE);
		tfConfirmPassword.setForeground(Color.WHITE); //text field for password in signUp
		tfConfirmPassword.setBounds(350,170,200,30);
		tfConfirmPassword.setBackground(new Color(30,27,27));
		tfConfirmPassword.setBorder(new LineBorder(new Color(37,19,129)));
		buttonSignUp.setBounds(400, 210, 125, 30);
		
		scoresButton.setBounds(250, 170, 90, 90); 
		scoresButton.setOpaque(false);
		scoresButton.setContentAreaFilled(false);
		scoresButton.setBorderPainted(false);
		
		form.add(messagePopUp);
		form.add(username);
		form.add(usernameSignUp);
		form.add(tfUsername); 
		form.add(tfUsernameSignUp);
		form.add(password);
		form.add(passwordSignUp);
		form.add(tfPassword);
		form.add(tfPasswordSignUp);
		form.add(confirmPassword);
		form.add(tfConfirmPassword);
		form.add(mapchoice);
		form.add(map); 
		form.add(buttonLogin);
		form.add(buttonSignUp);
		form.add(scoresButton); 
		form.add(bg);

		frame.add(form);

	} 

	public String getMapChoice(){
		return (String)map.getSelectedItem();  
	}

	public  String getUsername(){
		return tfUsername.getText(); 
	}

	@SuppressWarnings("deprecation")
	public String getPassword(){
		return tfPassword.getText(); 
	}

	public String getUsernameSignUp(){
		return tfUsernameSignUp.getText(); 
	}
	
	@SuppressWarnings("deprecation")
	public String getPasswordSignUp(){
		return tfPasswordSignUp.getText(); 
	}
	
	@SuppressWarnings("deprecation")
	public String getConfirmPassword() {
		return tfConfirmPassword.getText(); 
	}
	public void addButtonLoginListener (ActionListener listenForLoginButton){
		buttonLogin.addActionListener(listenForLoginButton);
	}
	
	public void addButtonSignUpListener (ActionListener listenForSignUpButton){
		buttonSignUp.addActionListener(listenForSignUpButton);
	}
	
	public void addButtonScoresListener(ActionListener listenForScoresButton){
		scoresButton.addActionListener(listenForScoresButton);
	}
	
	public void displayMessage(String message){
		         
		JOptionPane.showMessageDialog(this, message);	 
	}
	
	public void setInvisible(){
		frame.setVisible(false);
	}
	public void setVisible(){
		frame.setVisible(true);
	}
	
	public void blockSignUp(){
		
		tfUsernameSignUp.setEditable(false);
		tfPasswordSignUp.setEditable(false);
		tfConfirmPassword.setEditable(false);
		buttonSignUp.setEnabled(false);
	}
}