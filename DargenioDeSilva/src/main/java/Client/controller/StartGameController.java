package Client.controller;



import it.polimi.ingsw.DargenioDeSilva.ConfigurationClass;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;


import Client.login.view.ChoiceInterfaceView;
import Client.login.view.LoginView;
import Client.login.view.ScoresView;
import Client.networking.*;



/**
 * @author Ernesto De Silva 
 * Controller class that links the ChoiceInterface view and the Login view to the network interface. 
 * It implements methods to allow the player to connect to the server through RMI or Socket
 * and methods to login an existent user and signup a new one.
 */
public class StartGameController {
	private ChoiceInterfaceView interfaceView;
	private LoginView loginView; 
	private GameController gameController;  
	public String networkChoice;
	public String loginOrSignUp;
	private MyMatch match;
	private static StartGameController instance; 


	private NetworkInterface ni; 

	public static StartGameController getInstance() {
		if (instance==null)
			instance= new StartGameController();

		return instance; 
	}


	public void setStartGameController(ChoiceInterfaceView interfaceView, LoginView loginView, GameController gameController){
		this.gameController=gameController;
		this.loginView=loginView; 
		this.interfaceView=interfaceView; 
		
		this.interfaceView.addSocketButtonListener(new SocketListener());
		this.interfaceView.addRMIButtonListener(new RMIListener());
		this.loginView.addButtonLoginListener(new LoginListener()); 
		this.loginView.addButtonSignUpListener(new SignUpListener());
		this.loginView.addButtonScoresListener(new ScoresListener());
	}

	/**
	 * @author Ernesto De Silva
	 * Inner class that implements the ActionListener for "Socket" button
	 * The actionPerformed when the "Socket" button is clicked in the view
	 * @override actionPerformed
	 */
	class SocketListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			ni = NetworkFactory.getInterface("Socket"); 
			connect(ni); 
		}
	}	
	
	/**
	 * @author Ernesto De Silva
	 * Inner class that implements ActionListener for "RMI" button
	 * The actionPerformed calls the factory method that returns the RMI network interface
	 * @override actionPerformed
	 */
	class RMIListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			ni=NetworkFactory.getInterface("RMI");
			connect(ni);
		}
	}
	

	private void connect(NetworkInterface ni){
	
			gameController.setNetworkInterface(ni); 
			if(ni.connect()) {
				interfaceView.setInvisible();
				loginView.setVisible(); 
				}
			}

	
	/**
	 * @author Ernesto De Silva
	 * Inner class that implements the ActionListener for the "Login" Button
	 * The actionPerformed calls the "login" method from NetworkInterface class and 
	 * analize returned map with login's outcome.  
	 */
	class LoginListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e){

			String username=loginView.getUsername();
			String password=loginView.getPassword(); 
			String preferredMap=loginView.getMapChoice(); 
			Map<String,String> loginData;

			if (username.length()>=15 || !isAlphaNumeric(username)) {
				loginView.displayMessage("Please insert an alphanumeric username with up yo 15 chars!");
				return; 
			}
			loginData=ni.login(username, password, preferredMap);

			String result=loginData.get("action");
			if (result.equalsIgnoreCase("logged")) {				
				loginView.displayMessage("Logged!");
				loginView.setInvisible();
				match = new MyMatch(Integer.parseInt(loginData.get("matchID")),username);
				match.setMapName(loginData.get("map"));
				match.setPassword(password);
				match.setPreferredMap(preferredMap);
				gameController.setMatch(match);
				ni.giveMeNotifier(ConfigurationClass.getClientAddress(),username);		
			}

			if(result.equalsIgnoreCase("wrongPassword")) 
				loginView.displayMessage("Wrong Password");
			if(result.equalsIgnoreCase("notExistentUser"))
				loginView.displayMessage("Not Existent User");
			if(result.equalsIgnoreCase("alreadyLogged"))
				loginView.displayMessage("User already logged!");	
		}
	}

	/**
	 * @author Ernesto De Silva
	 * Inner class that implements the ActionListener for the "SignUp" Button
	 * The actionPerformed calls the "SignUp" method from NetworkInterface class
	 * that returns the outcome of signUp. 
	 */
	class SignUpListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){

			String username=loginView.getUsernameSignUp();
			String password=loginView.getPasswordSignUp();
			String passwordConfirm=loginView.getConfirmPassword();

			if(!password.equals(passwordConfirm)){
				loginView.displayMessage("Passwords don't match, please retry!");
				return;
			}

			String result;
			result=ni.signUp(username, password);
			if (result.equalsIgnoreCase("created")) {
				loginView.displayMessage("Account created, please login");	
				loginView.blockSignUp();				 
			}
			if(result.equalsIgnoreCase("invalidUsername")) {
				loginView.displayMessage("Invalid Username, please retry");
			}
		}	
	}
	/**
	 * @author Ernesto De Silva
	 * Inner class that implements the ActionListener for the "Scores" Button
	 * The actionPerformed calls the "showScores" method from NetworkInterface class
	 * that returns a map containing the rank. 
	 */
	class ScoresListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e){
			Map <String, String> scores; 
			Object[][] data=new Object[10][4]; 
			scores=ni.showScores(); 
			
			int i=0;;
			int rowIndex=0;
			while(scores.get("param"+Integer.toString(i))!=null) {
				String key="param"+Integer.toString(i);
				Object[] row=scores.get(key).split(","); 
				data[rowIndex]=row; 
				rowIndex++;
				i++;		
			}
			
			ScoresView scoresView=new ScoresView(); 
			scoresView.loadAndShowTable(data);
		}
	}

	/**
	 * This method returns true if the param is alphanumeric, otherwise returns false
	 * @param username
	 * @return boolean
	 */
	private boolean isAlphaNumeric(String username){
		for (int i=0; i<username.length(); i++) {
			char c = username.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c))
				return false;     
		}
		return true; 
	}



}


