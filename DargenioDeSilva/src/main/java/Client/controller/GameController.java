package Client.controller;

import it.polimi.ingsw.DargenioDeSilva.ConfigurationClass;
import it.polimi.ingsw.DargenioDeSilva.MapManager;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTextField;

import Client.login.view.ScoresView;
import Client.main.view.DrawingPanel;
import Client.main.view.EndGameView;
import Client.main.view.Hexmech;
import Client.main.view.MapView;
import Client.networking.MyMatch;
import Client.networking.NetworkInterface;

/**
 * @author Ernesto De Silva 
 * Controller class that links the game view to the network interface. 
 * It implements methods to allow the player to play the match through the GameView
 * and methods to update the game view according to communication messages from the server. 
 *
 */
public class GameController {
	private static GameController instance=null;
	NetworkInterface ni; 
	private MapView mapView; 
	private EndGameView endGameView; 
	private MyMatch myMatch;
	private MapManager mapManager=MapManager.getInstance(); 
	private Map<String,String> sectorType=new HashMap<String,String>();
	private Map <String,Integer> gamersIndex=new HashMap<String,Integer>(); 
	private boolean setPositionEnabled=false; 
	private boolean sendnoiseEnabled=false; 
	private static final Logger log=Logger.getLogger("log");

	 

	/**
	 * Singleton method
	 * @return an instance of this controller.
	 */
	public static GameController getInstance(){
		if (instance==null) 
			instance=new GameController();
		return instance; 
		

	}

	public void setMapView(MapView mapView){
		this.mapView=mapView;
	} 


	public void setMatch(MyMatch match){
		myMatch=match;		
	} 

	public void setNetworkInterface(NetworkInterface ni){
		this.ni=ni; 
	}
	
	
	/**
	 * Method called when each client receive the StartGameCommand by server through the notifier.
	 * It create a new match loading the view and implementing actionPerformed for the elements of the view. 
	 * @param usernames, list of players for this match.
	 */
	public void startMatch(List<String> usernames) {
		loadSectors(); //load the Map<String,String> containing the type for each sector. 
		mapView=new MapView();
		endGameView=new EndGameView(); 

		this.mapView.addSectorListener(new SectorClickListener());
		this.mapView.addAttackButtonListener(new AttackButtonListener());
		this.mapView.addShiftTurnButtonListener(new ShiftTurnButtonListener());
		this.mapView.addDrawCardButtonListener(new DrawCardButtonListener());
		this.endGameView.addMovementsBoxListener(new MovementsBoxListener());
		this.endGameView.addRestartGameButtonListener(new PlayAgainButtonListener());
		this.endGameView.addScoresButtonListener(new ScoresButtonListener());
		
		String type = ni.giveMeCharacter(myMatch.getMatchID(), myMatch.getMyUsername()); // return if the client is human or alien
		mapView.setTypeCharachter(type);
		myMatch.setRole(type);
		
		if(type.equals("alien"))
			mapView.setVisibleAttackButton(true);
		
		setUserLabels(usernames); 	
		
	}
	
	/***********************************Methods to set and update the view*******************************/
	
	/**
	 * Write usernames into the label of the game view.
	 * @param usernames, list of players for this match.
	 */
	public void setUserLabels(List<String> usernames){
		String myUsername=myMatch.getMyUsername();
		int i=0; 
		int fieldIndex=0;

		for(i=0;i<usernames.size(); i++){ //loads own username in the text field in the bottom left-hand corner 
			if (usernames.get(i).equals(myUsername)) {
				mapView.getUsers()[0].setText(myUsername);
				mapView.getUsers()[0].setOpaque(true);
				mapView.getUsers()[0].setBackground(Color.DARK_GRAY);
				mapView.getStaticMessages()[0].setBackground(Color.DARK_GRAY);
				mapView.getMessages()[0].setBackground(Color.DARK_GRAY);
				gamersIndex.put(myUsername, 0);
				fieldIndex=1; 
				break; 	
			}
		}
		
		//loads circularly usernames from the text field of the own username
		for (int j=i+1; j<usernames.size();j++) { 
			mapView.getUsers()[fieldIndex].setText(usernames.get(j));
			mapView.getUsers()[fieldIndex].setOpaque(true);
			mapView.getUsers()[fieldIndex].setBackground(Color.DARK_GRAY);
			mapView.getStaticMessages()[fieldIndex].setBackground(Color.DARK_GRAY);
			mapView.getMessages()[fieldIndex].setBackground(Color.DARK_GRAY);
			gamersIndex.put(usernames.get(j), fieldIndex);
			fieldIndex++;
		}

		if (i!=0) {
			for (int k=0; k<i; k++){
				mapView.getUsers()[fieldIndex].setText(usernames.get(k));
				mapView.getUsers()[fieldIndex].setOpaque(true);
				mapView.getUsers()[fieldIndex].setBackground(Color.DARK_GRAY);
				mapView.getStaticMessages()[fieldIndex].setBackground(Color.DARK_GRAY);
				mapView.getMessages()[fieldIndex].setBackground(Color.DARK_GRAY);
				gamersIndex.put(usernames.get(k), fieldIndex);
				fieldIndex++;

			}
		}
	}

	/**
	 * Write messages from server in the text field of the parameterized username.
	 * Write also a short message in case of attack, reverse turn, noise in the static text field
	 * (the static text field is not clean after the end of each turn, it is overwritten only when the player 
	 * makes noise/attack/loses his turn) 
	 * @param username
	 * @param message
	 */
	public void setMessage(String username, String message, String shortMessage){
		int i=gamersIndex.get(username); 
		mapView.getMessages()[i].setText(message);
		
		if(shortMessage!=null) //null when the static field must not be filled
			mapView.getStaticMessages()[i].setText(shortMessage);
	}

	/**
	 * Load the SectorType map with the type for each sector of the game map.
	 */
	public void loadSectors(){
		String mapName=myMatch.getMapName();
		String path=mapManager.getFile().get(mapName);
		File fileMap=new File(path);
		try {

			Scanner in=new Scanner (fileMap);

			char x;
			Integer y;
			char type;

			while(in.next().equals(";")){

				x=in.next().charAt(0);
				y=Integer.parseInt(in.next());
				type=in.next().charAt(0);

				sectorType.put(x+y.toString(), Character.toString(type));

			} 
			in.close(); 
		}
		catch (FileNotFoundException FNFe) {
			log.log(Level.SEVERE,"File not found"+FNFe.getMessage());

		}
	}
	
	/**
	 * @param coord of a sector
	 * @return the type of the parameterized sector
	 */
	public String getType(String coord){
		if (sectorType.size()==0) return null;
		return sectorType.get(coord);
	}
	
	/**
	 * @author Ernesto De Silva
	 * Update the view according to the player who has to play in the turn. 
	 * @param username who has to play in the turn. 
	 */
	public void changeTurn(String username){ //initialize the color of the player which turn is ended
		
		if(myMatch.getCounterTurn()>0){ //only if it's NOT the first turn, otherwise getTurnOf is NULL!
			int j=gamersIndex.get(myMatch.getTurnOf());
			JTextField textOld=mapView.getUsers()[j];
			if(!textOld.getCaretColor().equals(Color.RED)) 
				textOld.setBackground(Color.DARK_GRAY);
		}
		
		int i=gamersIndex.get(username); 
		
		if (mapView.getStaticMessages()[i].getText().equalsIgnoreCase("turn lost"))
			mapView.getStaticMessages()[i].setText(""); 
		
		JTextField text=mapView.getUsers()[i];
		text.setBackground(new Color(172,191,0));
		myMatch.setTurnOf(username);
		myMatch.incrementCounterTurn();
		
		if (myMatch.isMyTurn()) {
			setPositionEnabled=true;	
			
			for (i=0; i<mapView.getMessages().length; i++){ //clean all message text fields
				mapView.getMessages()[i].setText("");
			}
			
			mapView.setLabelMessage("Move to a sector");
		}
	}

	/**Update the view according to the player who has lost the turn. 
	 * @param username who has lost the turn. 
	 */
	public void reverseToSpecificPlayer(String username){
		if (username.equals(myMatch.getMyUsername())) {
			if(setPositionEnabled==false){ //if the player changed his position, it will be restored to the previous one
				mapView.setLabelMessage("Position restored to "+myMatch.getPreviousSector());
				mapView.showIcon(myMatch.getPreviousMouseX(), myMatch.getPreviousMouseY(), myMatch.imHuman());	
			}
			else mapView.setLabelMessage("Turn timeout expired");
		}
	}
	
	/**Update the view according to the player who's deleted from the match. 
	 * @param username who's deleted. 
	 */
	public void playerDeleted(String username){
		int i=gamersIndex.get(username); 
		mapView.getUsers()[i].setBackground(Color.RED);
		mapView.getMessages()[i].setEnabled(false);
		
	}
	
	/**
	 * Show a view with the winners and all the movements of players in this match
	 * @param list of winners. 
	 */
	public void endMatch(List<String> winners, Map<String,String> movements){	
		
		endGameView.setVisible();
		endGameView.loadWinners(winners);	
		endGameView.loadMovements(movements);
		mapView.setInvisible();	
	}
	
	/********************************************END Methods*********************************************/
	

	/*********************Inner classes with actionPerformed for elements in this view*******************/
	/**
	 * @author Ernesto De Silva
	 * Inner class that implements the ActionListener for the click(s) on a sector
	 * The actionPerformed calls:
	 * the "setPosition" method from NetworkInterface class with ONE CLICK
	 * the "sendnoise" method with TWO CLICK.
 	 *
	 */
	class SectorClickListener extends MouseAdapter	{
		@Override
		public void mouseClicked(MouseEvent e) { 
			
			if (e.getClickCount()==1){ //setPosition
				Point p = new Point(Hexmech.pxtoHex(e.getX(),e.getY())); //converting the click coordinates to sector coordinates
				if (p.x < 0 || p.y < 0 || p.x >= DrawingPanel.COLUMNNUM || p.y >= DrawingPanel.ROWNUM) return;
				String clickedSector= (char)((p.x)+65)+Integer.toString(((p.y)+1)); //converting the sector coordinates to the string "char+int"
				String typeOfSector= getType (clickedSector);
				
				if (myMatch.isMyTurn()) { 
					if(setPositionEnabled) { 
						if(ni.setPosition(myMatch.getMatchID(), myMatch.getMyUsername(), clickedSector)) { // return false for not valid movement	
							setPositionEnabled=false; 
							mapView.showIcon(p.x,p.y, myMatch.imHuman()); 
							myMatch.storeAndSetMySector(clickedSector,p.x,p.y);		
							}

						else { 
							mapView.setLabelMessage("Invalid sector! Retry");
								   return;
							}

						if (typeOfSector.equalsIgnoreCase("S")) { 
							if(myMatch.imHuman())	{ //if human, he can only skip turn 
								mapView.setLabelMessage("Change turn!");
								mapView.setEnabledShiftTurnButton(true);
								}
							else { //if alien, he can attack or skip turn 
								mapView.setLabelMessage("Attack or skip turn");
								mapView.setEnabledShiftTurnButton(true);
								mapView.setEnabledAttackButton(true); 
								}
							}

						if(typeOfSector.equalsIgnoreCase("D")) {
							if(myMatch.imHuman()) { // if human, he can only draws card
								mapView.setLabelMessage("Draw card");
								mapView.setEnabledDrawCardButton(true);
								}
							else { //if alien, he can attack or draw card
								mapView.setLabelMessage("Attack or draw card");
								mapView.setEnabledShiftTurnButton(false); 
								mapView.setEnabledDrawCardButton(true);
								mapView.setEnabledAttackButton(true);
								}
							}
						}
					}		

				else mapView.displayMessage("Please wait for your turn!");
				}
				
			

			if (e.getClickCount()==2){ //noise
				Point p = new Point(Hexmech.pxtoHex(e.getX(),e.getY()) );

				if (p.x < 0 || p.y < 0 || p.x >= DrawingPanel.COLUMNNUM || p.y >= DrawingPanel.ROWNUM) return;
				String clickedSector= (char)((p.x)+65)+Integer.toString(((p.y)+1));

					if (myMatch.isMyTurn())
						sendnoise(clickedSector);
					
					else mapView.displayMessage("Wait for your turn");
				}
			}
		}

	
	/**
	 * @author Ernesto De Silva
	 * Inner class that implements the ActionListener for the "ShiftTurn" Button
	 * The actionPerformed updates the view to notify to the player to wait for his turn.
	 */
	class ShiftTurnButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){	
			
			if(ni.changeTurn(myMatch.getMatchID(), myMatch.getMyUsername())) {
				mapView.getUsers()[gamersIndex.get(myMatch.getMyUsername())].setBackground(Color.DARK_GRAY);
				mapView.setLabelMessage("Wait for you turn");
				mapView.setEnabledShiftTurnButton(false);
				mapView.setEnabledAttackButton(false);
				}
			}
		}
	
	/**
	 * @author Ernesto De Silva
	 * Inner class that implements the ActionListener for the "Attack" Button
	 * The actionPerformed calls the "attack" method from NetworkInterface class
	 * to allow the player to attack in his sector. 
	 */
	class AttackButtonListener implements ActionListener {
		@Override
		public void actionPerformed (ActionEvent e){
			
			if(ni.attack(myMatch.getMatchID(),myMatch.getMyUsername())) {
				sendnoiseEnabled=false;
				mapView.setLabelMessage("Wait for your turn");
				mapView.setEnabledAttackButton(false);
				mapView.setEnabledDrawCardButton(false);
				mapView.setEnabledShiftTurnButton(false);
				}
			else mapView.setLabelMessage("Not valid command");
			}
		}
	
	/**
	 * @author Ernesto De Silva
	 * Inner class that implements the ActionListener for the "DrawCard" Button
	 * The actionPerformed calls the "drawCard" method from NetworkInterface class
	 * to allow the player to draw a card (returned from server). 
	 */
	class DrawCardButtonListener implements ActionListener{
		@Override
		public void actionPerformed (ActionEvent e){		
			String drawnCard=ni.drawCard(myMatch.getMatchID(), myMatch.getMyUsername());
			mapView.setEnabledAttackButton(false);
			mapView.setEnabledDrawCardButton(false);
			if (drawnCard.equalsIgnoreCase("SilenceCard"))
				mapView.setLabelMessage("Silence card. Wait for your turn" );
			if (drawnCard.equalsIgnoreCase("noiseYoursCard"))
				mapView.setLabelMessage("Drawn a card: double click in your sector");
			if (drawnCard.equalsIgnoreCase("noiseAnyCard"))
				mapView.setLabelMessage("Drawn a card: double click in any sector");
			
			sendnoiseEnabled=true; 
				} 		
			}
	
	/**
	 * @author Ernesto De Silva
	 * Calls the "sendNoise" method from NetworkInterface to send the noise in the parameterized sector.
	 * @param noiseSector
	 */
	public void sendnoise(String noiseSector){
		
			if(sendnoiseEnabled){
				if (getType(noiseSector).equals("B")) {
					mapView.setLabelMessage("Blocked sector, retry!");					 
				}
				else {
					if (ni.sendNoise(myMatch.getMatchID(), myMatch.getMyUsername(), noiseSector)) {	//return true for valid noise, false if not				
					sendnoiseEnabled=false;
					mapView.setEnabledShiftTurnButton(false);
					mapView.setLabelMessage("Wait for your turn");
					
					}
					else mapView.setLabelMessage("You have to do noise in your sector!");
				}
			}
			else mapView.displayMessage("Not valid noise!"); //noise at wrong time
		}
	
	
	/**
	 * @author Ernesto De Silva
	 * Inner class that implements the ActionListener for the "Movements" ComboBox
	 * The actionPerformed calls the "showMovements" method from endGameView
	 * to show all movements in this match 
	 */
	class MovementsBoxListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String interval = (String)endGameView.getSelectedInterval();
			endGameView.showMovements(Integer.parseInt(interval.split("-")[0]), Integer.parseInt(interval.split("-")[1]));		
		}	
	}
	
	/**
	 * @author Ernesto De Silva
	 * Inner class that implements the ActionListener for the "PlayAgain" Button
	 * The actionPerformed calls the "login" and "giveMeNotifier" methods from NetworkInterface class
	 * to allow the player to play a new match. 
	 */
	class PlayAgainButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			Map<String, String> loginData=ni.login(myMatch.getMyUsername(), myMatch.getPassword(), myMatch.getPreferredMap()); 

			String result=loginData.get("action");
			if (result.equalsIgnoreCase("logged")){
				String password=myMatch.getPassword();
				String preferredMap=myMatch.getPreferredMap();
				myMatch = new MyMatch(Integer.parseInt(loginData.get("matchID")),myMatch.getMyUsername());
				myMatch.setMapName(loginData.get("map"));
				myMatch.setPassword(password);
				myMatch.setPreferredMap(preferredMap);
				ni.giveMeNotifier(ConfigurationClass.getClientAddress(),myMatch.getMyUsername());
				endGameView.setInvisible();
			}
		}
	}
	

	/**
	 * @author Ernesto De Silva
	 * Inner class that implements the ActionListener for the "Scores" Button
	 * The actionPerformed calls the "showScores" method from NetworkInterface class
	 * to show ranking.
	 */
	class ScoresButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
				Map <String, String> scores; 
				Object[][] data=new Object[10][4]; 
				scores=ni.showScores(); 
				
				int i=0;;
				int rowIndex=0;
				while(scores.get("param"+Integer.toString(i))!=null) {
					String key="param"+Integer.toString(i);
					Object[] row=scores.get(key).split(","); 
					data[rowIndex]=row; 
					rowIndex++;
					i++;		
				}
				
				ScoresView scoresView=new ScoresView(); 
				scoresView.loadAndShowTable(data);
			}
		}
	}
	
	/********************************************END actionPerformed********************************************/
	

