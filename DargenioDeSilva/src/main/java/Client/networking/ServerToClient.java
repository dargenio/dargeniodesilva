package Client.networking;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import Client.command.ClientCommand;
import Client.command.ClientCommandFactory;
import exceptions.NotCommandException;

/**
 * @author Ruggiero Dargenio
 * Class to open a reverse connection from server to client.
 * It creates with a new thread a ServerSocket waiting for connections on the port communicated 
 * by server to client during the login. 
 */
public class ServerToClient extends Thread {
	private int port;
	private ServerSocket clientSs;
	private ObjectOutputStream out;
	private Socket sc;
	private boolean getMessage;
	private Logger log=Logger.getLogger("log");
	private ObjectInputStream in;

	public ServerToClient(int port){
		this.port=port;
		getMessage=true;
	}

	@Override
	public void run(){
		try {
			clientSs=new ServerSocket(port);
			sc=clientSs.accept(); 
			out=new ObjectOutputStream(sc.getOutputStream());
			in=new ObjectInputStream(sc.getInputStream());
		}catch(IOException ex){
			
			
			log.log(Level.WARNING, "Error in opening socket");
		}
			

			while(getMessage){
				try{
					@SuppressWarnings("unchecked")
					Map<String,String> commands=(Map<String,String>)in.readObject();

					try{ 
						ClientCommand command=ClientCommandFactory.clientCommandFactory(commands.get("action"));
						command.action(commands);
					}catch(NotCommandException ex){
						log.log(Level.SEVERE,"Command not found: "+ex.getMessage());
					}
				}catch(IOException IOex){
					getMessage=false;
					log.log(Level.FINE,"Server disconnected");
				}catch(ClassNotFoundException CNFe){
					log.log(Level.SEVERE,"Class not found: "+CNFe.getMessage());
				}
			}
	}


	void stopListening(){
		getMessage=false;
		try{
			in.close();
			out.close();
			sc.close();
		}catch(IOException ex){
			System.err.println("Error in closing Notifier socket");
		}
	}
}
