package Client.networking;


import it.polimi.ingsw.DargenioDeSilva.ConfigurationClass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Ernesto De Silva
 * This class implements Network Interface.
 * It contains all the methods to allow the client to communicate with server 
 * over a socket connection
 */
public class SocketInterface implements NetworkInterface {
	
	private Socket s, ss;
	private ServerSocket clientSs; 
	private PrintWriter pw; 
	private BufferedReader br; 
	private String address;
	ObjectInputStream ios;
	ObjectOutputStream oos;
	private static final Logger log=Logger.getLogger("log");
	
	 /************************************Constructor*****************************************/
	 
	public SocketInterface() {
		address=ConfigurationClass.getServerAddress();
	} 
	 /***********************************end constructor**************************************/
	
	 /************************************Connection*****************************************/
	@Override
	public boolean connect() {
		try {
			s = new Socket(address, 4000);
			oos=new ObjectOutputStream(s.getOutputStream());
			ios=new ObjectInputStream(s.getInputStream());
		} catch (UnknownHostException e) {
			log.log(Level.SEVERE, "UnkownHostException"+e.getMessage());
			return false; 
		} catch (IOException IOe) {
			log.log(Level.SEVERE, "IOException"+IOe.getMessage());
			return false; 
		}
		
		
		return true; 
	}
	
	 /**********************************end connection*****************************************/
		
	 /****************************Login and signUp methods*************************************/
	@Override
	public Map<String,String> login(String username, String password, String preferredMap) {

		try {
			Map <String, String> commandOutput=new HashMap<String,String>(); 
			commandOutput.put("action", "login");
			commandOutput.put("username", username); 
			commandOutput.put("password", password);
			commandOutput.put("preferredMap", preferredMap);

			oos.writeObject (commandOutput);

			@SuppressWarnings("unchecked")
			Map<String,String> commandInputSock =(Map<String,String>)ios.readObject();

			if (commandInputSock.get("action").equalsIgnoreCase("logged")){
				ServerToClient serverToClient=new ServerToClient(Integer.parseInt(commandInputSock.get("port")));
				serverToClient.start();
				

			}
			return commandInputSock;
		}

		 catch (ClassNotFoundException CNFe) {
			log.log(Level.SEVERE, "ClassNotFoundException"+CNFe.getMessage());
		} catch (IOException IOe) {
			log.log(Level.SEVERE, "IOe"+IOe.getMessage());
		}
		return null;
	}
	
	
	public String signUp(String username, String password) {
		try{
			Map <String, String> commandOutput=new HashMap<String,String>(); 
			commandOutput.put("action", "signUp");
			commandOutput.put("username", username);
			commandOutput.put("password", password);
			
			oos.writeObject (commandOutput);
			@SuppressWarnings("unchecked")
			Map<String,String> commandInputSock =(Map<String,String>)ios.readObject();
			return commandInputSock.get("result");
			
		}
		catch(IOException IOe) {
			log.log(Level.SEVERE, "IOException"+IOe.getMessage());
			}
		catch(ClassNotFoundException CNFe){
			log.log(Level.SEVERE, "ClassNotFoundException"+CNFe.getMessage());
		}
			return ""; 
		
	}

	@Override
	public void giveMeNotifier(String address,String username) {
		Map <String, String> commandOutput=new HashMap<String,String>();
		
		commandOutput.put("action", "giveMeNotifier");
		commandOutput.put("username", username); 
		commandOutput.put("address",address);
		try{
			oos.writeObject(commandOutput);
		}catch (IOException IOe){
				log.log(Level.SEVERE, "IOException"+IOe.getMessage()); 
		}
		
		
	}
	
	@Override
	public Map<String, String> showScores() {
		try{
		Map <String, String> commandOutput=new HashMap<String,String>();	
		commandOutput.put("action", "showScores"); 	
		oos.writeObject (commandOutput);
		@SuppressWarnings("unchecked")
		Map<String,String> commandInputSock =(Map<String,String>)ios.readObject();
		return commandInputSock;
		}
		catch(IOException IOe) {
			log.log(Level.SEVERE, "IOException"+IOe.getMessage());
			}
		catch(ClassNotFoundException CNFe){
			log.log(Level.SEVERE, "ClassNotFoundException"+CNFe.getMessage());
		}
			return null; 
	}

	
	 /****************************end login and signUp methods*********************************/
	
	 
	/*************************************Gaming methods***************************************/
	@Override
	public boolean setPosition(int ID,String username, String sector)  {
			
			
				Map <String, String> commandOutput=new HashMap<String,String>();
				
				commandOutput.put("action", "setPosition");
				commandOutput.put("sector", sector);
				commandOutput.put("username", username);
				commandOutput.put("ID", Integer.toString(ID));
				
				return sendAndReadInputMap(commandOutput); 
		
	} 
	
	public boolean attack(int ID,String username) {
		
		
			
			Map <String, String> commandOutput=new HashMap<String,String>(); 
			commandOutput.put("action", "attack");
			commandOutput.put("username", username);
			commandOutput.put("ID", Integer.toString(ID));
			
			
			return sendAndReadInputMap(commandOutput); 
		}
		
	
	
	
	public boolean close() {
		try{
			br.close();
			pw.close();
			s.close();
			ss.close();
			clientSs.close();
			return true;
		} catch(IOException ex){
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String drawCard (int ID, String username){
		
		
		Map <String, String> commandOutput=new HashMap<String,String>();
		commandOutput.put("action", "drawCard");
		commandOutput.put("ID", Integer.toString(ID)); 
		commandOutput.put("username", username); 
		
		try{
			oos.writeObject(commandOutput);
			Map<String,String> result=new HashMap <String,String>(); 
			result=(Map<String,String>) ios.readObject(); 
			return result.get("result");
			} 
			catch (ClassNotFoundException CNFe) {
				log.log(Level.SEVERE, "ClassNotFoundException"+CNFe.getMessage());
				return null; 
			}
			catch (IOException IOe){
				log.log(Level.SEVERE, "IOException"+IOe.getMessage());
				return null; 
				}
			}
			
			
		
	
	
	@Override
	public boolean sendNoise(int ID, String username, String sector) {
	
			
		Map <String, String> commandOutput=new HashMap<String,String>();
		
		commandOutput.put("action", "noise");
		commandOutput.put("sector", sector); 
		commandOutput.put("username", username); 
		commandOutput.put("ID", Integer.toString(ID)); 
		
		return sendAndReadInputMap(commandOutput);
	}
	
	@Override
	public boolean changeTurn(int ID, String username)  {
		Map <String, String> commandOutput=new HashMap<String,String>();	
		commandOutput.put("action", "changeTurn");
		commandOutput.put("ID", Integer.toString(ID)); 
		commandOutput.put("username", username); 
		
		return sendAndReadInputMap(commandOutput);
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String giveMeCharacter(int ID, String username) {
		Map<String,String> commandOutput=new HashMap<String,String>();
		commandOutput.put("action", "typeCharacter"); 
		commandOutput.put("ID", Integer.toString(ID)); 
		commandOutput.put("username", username); 
		try{
			oos.writeObject(commandOutput);
			Map<String,String> result=new HashMap <String,String>(); 
			try{
				Object read=ios.readObject();
				result=(Map<String,String>) read; 
				return result.get("result"); 
		} catch(ClassNotFoundException CNFe){
			log.log(Level.SEVERE, "ClassNotFoundException"+CNFe.getMessage());
		}
			
	}
		catch (IOException IOe){
			log.log(Level.SEVERE, "IOException"+IOe.getMessage());
		}
		return null;
	}
	
	/************************************end gaming methods************************************/

	
	
	
	/**
	 * Method that send the map to the server and return the boolean value
	 * read on the answer map received from server.
	 * @param commandOutput, map to send to server
	 * @return boolean value read on the received map.
	 */
	@SuppressWarnings("unchecked")
	private synchronized Boolean sendAndReadInputMap(Map<String,String> commandOutput) {
		try{
			oos.writeObject(commandOutput);
			Map<String,String> result=new HashMap <String,String>(); 		
			result=(Map<String,String>) ios.readObject(); 
			return Boolean.parseBoolean(result.get("result")); 
		} 

		catch (IOException IOe){
			log.log(Level.SEVERE, "IOException"+IOe.getMessage()); 
			return false;
		}
		catch(ClassNotFoundException CNFe){
			log.log(Level.SEVERE, "ClassNotFoundException"+CNFe.getMessage());
			return false;
		}

	}

	
	
	
}

	
