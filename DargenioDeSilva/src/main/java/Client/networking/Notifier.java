package Client.networking;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import exceptions.NotCommandException;
import Client.command.ClientCommand;
import Client.command.ClientCommandFactory;
import Client.networking.rmi.RemoteNotifier;

/**
 * @author Ernesto De Silva
 * Class that implements RemoteNotifier interface.
 */
public class Notifier implements RemoteNotifier {
	private static final Logger log=Logger.getLogger("log");
	
	/* (non-Javadoc)
	 * @see Client.networking.rmi.RemoteNotifier#notifyMessage(java.util.Map)
	 */
	public void notifyMessage(Map <String, String> notifierCommand){
		try{ 
			ClientCommand command=ClientCommandFactory.clientCommandFactory(notifierCommand.get("action"));
			command.action(notifierCommand);
		}
		catch(NotCommandException ex){
			log.log(Level.SEVERE, "NotCommandException: "+ex.getMessage());
		}
	}
}
