package Client.networking.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;

/**
 * @author Ernesto De Silva
 * Interface exposed by the clients to allow the server to communicate with them sending messages.
 */
public interface RemoteNotifier extends Remote {
	/** 
	 * Method that calls a CommandFactory method on the field of param map with key "action". 
	 * It generate a new command.
	 * @param notifierCommand, map containing action to generate command and other params 
	 * related to the command that server wants to communicate to the client.
	 * @throws RemoteException
	 */
	void notifyMessage(Map <String,String> notifierCommand) throws RemoteException;
}
