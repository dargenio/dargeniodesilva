package Client.networking;


/**
 * @author ernestodesilva
 * Factory method class. It creates the correct network interface. 
 */
public class NetworkFactory {
	
	private NetworkFactory(){
		
	}
	
	 /**
	 * Static method that generate a new NetworkInterface. 
	 * @param A string that indicate the desired Newtwork Interface.
	 * @return A new network interface
	 */
	public static NetworkInterface getInterface(String param){
		if(param.equals("Socket")) return new SocketInterface();
		else return new RMIInterface();
		 
	}
}
