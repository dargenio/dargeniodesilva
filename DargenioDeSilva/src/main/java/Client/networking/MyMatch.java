package Client.networking;

/**
 * @author Ernesto De Silva
 * This class contains all useful data of a player during his match.
 * It contains methods to manage this data.
 */
public class MyMatch {
	
	private int matchID; 
	private String myUsername;
	private String mapName;
	private String turnOf; 
	private boolean human; 
	private int counterTurn=0; 
	private String previousSector;
	private String mySector="Start sector";
	private int previousMouseX, previousMouseY; 
	private int mouseX=100;  
	private int mouseY=100; 
	private String password; 
	private String preferredMap; 
	

	public MyMatch(int ID, String myUsername){
		matchID=ID;
		this.myUsername=myUsername; 
	}
	
	public void storeAndSetMySector(String mySector, int x, int y){
		this.previousSector=this.mySector; //save the previous sector before changing it
		this.previousMouseX=this.mouseX;  
		this.previousMouseY=this.mouseY;
		this.mySector=mySector; 
		this.mouseX=x;
		this.mouseY=y; 
	}
	
	public String getPreferredMap() {
		return preferredMap;
	}

	public void setPreferredMap(String preferredMap) {
		this.preferredMap=preferredMap;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getPreviousSector (){
		return this.previousSector;
	}
	public int getPreviousMouseX(){
		return this.previousMouseX; 
	}
	public int getPreviousMouseY(){
		return this.previousMouseY; 
	}
	public int getCounterTurn() {
		return counterTurn;
	}

	public void incrementCounterTurn() {
		this.counterTurn++;
	}

	public String getTurnOf() { 
		return turnOf;
	}
	
	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public int getMatchID() {
		return matchID;
	}
	
	public String getMyUsername() {
		return myUsername;
	}
	
	public void setTurnOf(String turnOf) {
		this.turnOf = turnOf;
	}
	
	public boolean isMyTurn(){
		if (this.turnOf.equalsIgnoreCase(myUsername))
			return true; 
		else return false; 
	}
	
	public boolean imHuman() {
		return human;
	}
	
	public void setRole(String role){
		if (role.equalsIgnoreCase("human"))
			this.human=true; 
		else 
			this.human=false; 
	}
	
}
