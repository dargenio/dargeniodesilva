package Client.networking;



import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import exceptions.NotCommandException;
import Client.command.ClientCommand;
import Client.command.ClientCommandFactory;


public class ClientHandler extends Thread{
	ObjectOutputStream out;
	ObjectInputStream in;
	private Socket sc;
	boolean getMessage=true;
	private static final Logger log=Logger.getLogger("log");
	
	public ClientHandler(Socket sc){
		this.sc=sc;
	}


	 @Override
	public void run(){
		System.out.println("Client avviato");

		try
		{
			startServerClient();
		}
		catch(Exception e)
		{
		log.log(Level.SEVERE, "Exception"+e.getMessage());
		}

	}

	/**
	 * This method handles commands that the server send back to the client. 
	 */
	public void startServerClient(){
		try{	
			System.out.println("Received connection");
			out=new ObjectOutputStream(sc.getOutputStream());
			in=new ObjectInputStream(sc.getInputStream());
			while(true){
				try{
		
					@SuppressWarnings("unchecked")
					Map<String,String> commands=(Map<String,String>)in.readObject();
					try{
						ClientCommand command=ClientCommandFactory.clientCommandFactory(commands.get("action"));
						command.action(commands);
					}catch(NotCommandException NCe){
						log.log(Level.SEVERE, "NotCommandException"+NCe.getMessage());
					}		

				}catch(IOException IOe){
					try{
						sleep(1000);
						log.log(Level.SEVERE, "IOException"+IOe.getMessage());}
					catch(InterruptedException IEe){
						log.log(Level.SEVERE, "InterruptedException"+IEe.getMessage());
					}
					System.out.println("Errore apertura socket");
				}
			}

		}catch(ClassNotFoundException CNFe){
			log.log(Level.SEVERE, "ClassNotFoundException"+CNFe.getMessage());
		}
		catch(IOException IOe){
			log.log(Level.SEVERE, "IOException"+IOe.getMessage());
		}
	}
	void stopListening(){
		getMessage=false;
		try{
			sc.close();
		}catch(IOException ex){
			System.err.println("Error in closing Notifier socket");
		}
	}
}
