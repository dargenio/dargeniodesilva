package Client.networking;


import it.polimi.ingsw.DargenioDeSilva.ConfigurationClass;

import java.io.IOException;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


import Client.networking.rmi.RemoteNotifier;
import Server.networking.rmi.*;

/**
 * @author Ernesto De Silva
 * This class implements NetworkInterface.
 * It implements all the methods to communicate through RMI protocol.
 */
public class RMIInterface implements NetworkInterface {
	private int registryPort=ConfigurationClass.getPortRegistry();
	RemoteLogin game;
	RemoteMatch match; 
	private static final Logger log=Logger.getLogger("log");
	
	/* (non-Javadoc)
	 * @see Client.networking.NetworkInterface#connect()
	 */
	@Override
	public boolean connect()  {
		String connectName = "Connect";
		Registry registry;
		try {
			registry = LocateRegistry.getRegistry(ConfigurationClass.getServerAddress(),registryPort);
		} catch (RemoteException Re) {			
			log.log(Level.SEVERE, "Exception getting registry"+Re.getMessage());
			return false; 
		}
		try {
			game = (RemoteLogin) registry.lookup(connectName);
			return true; 
		} catch (AccessException Ae) {
			log.log(Level.SEVERE, "Exception during lookup"+Ae.getMessage());
			return false;
		} catch (RemoteException Re) {
			log.log(Level.SEVERE, "RemoteException"+Re.getMessage());
			return false;
		} catch (NotBoundException NBe) {
			log.log(Level.SEVERE, "NotBoundException"+NBe.getMessage());
			return false; 
		}
		
		
	}
	
	public boolean searchMatch(String matchID){
		
		Registry registry;
		try{
			registry=LocateRegistry.getRegistry(5999);
		}
		catch (RemoteException Re) {			
			log.log(Level.SEVERE, "RemoteException"+Re.getMessage());
			return false; 
		}
		
		try {
			match= (RemoteMatch) registry.lookup(matchID); 
			return true; 
		}
		  catch (AccessException Ae) {
			log.log(Level.SEVERE, "Exception during lookup"+Ae.getMessage());
			return false;
		} catch (RemoteException Re){
			log.log(Level.SEVERE, "RemoteException"+Re.getMessage());
			return false;
		} catch (NotBoundException NBe) {
			log.log(Level.SEVERE, "NotBoundException"+NBe.getMessage());
			return false; 
		}
	}

	@Override
	public Map<String,String> login(String username, String password, String preferredMap) {
		
		Map<String, String> commandInputRMI;
		try {
			commandInputRMI = game.login(username, password, preferredMap);
			if (commandInputRMI.get("action").equalsIgnoreCase("logged")){
				serverToClient(Integer.parseInt(commandInputRMI.get("port"))); 
				searchMatch(commandInputRMI.get("matchID")); }
				return commandInputRMI;
				
		}catch (NumberFormatException NFe) {
			log.log(Level.SEVERE, "Error in parsing port number"+NFe.getMessage());
			return null;
		} catch (IOException IOe) {
			log.log(Level.SEVERE,  "IOException"+IOe.getMessage());
			return null; 
		}
		
		
			
		
		 
		
		
	}

	@Override
	public String signUp(String username, String password) {
		try {
			return game.signUp(username, password);
		} catch (RemoteException Re) {
			log.log(Level.SEVERE, "Remote exception"+Re.getMessage());
			return ""; 
		} 
		
	}
	
	//@Override
	public void serverToClient(int port) throws IOException {
		Registry registryNotifier =null; 
		String notifierName="Notifier"; 
		
		try{
		RemoteNotifier notifier=new Notifier(); 
		RemoteNotifier stub= (RemoteNotifier) UnicastRemoteObject.exportObject(notifier,0);
		registryNotifier=LocateRegistry.createRegistry(port);
		registryNotifier.bind(notifierName,stub); 
		}
		
		catch (Exception e) {
            System.err.println("Error in creating RMI registry");
            log.log(Level.SEVERE, e.getMessage());
            registryNotifier = null;
		
		}
		
	}
	
	@Override
	public boolean setPosition(int ID,String username, String sector) {
		try {
			return match.setPosition(username, sector);
		} catch (RemoteException Re) {
			log.log(Level.SEVERE, "RemoteException"+Re.getMessage());
			return false; 
		}
	}

	@Override
	public boolean attack(int ID,String username)  {
		try {
			return match.attack(username);
		} catch (RemoteException Re) {
			log.log(Level.SEVERE, "RemoteException"+Re.getMessage());
			return false; 
		}
		
	}

	@Override
	public boolean sendNoise(int ID, String username, String sector) {
		try {
			return match.noise(username, sector);
		} catch (RemoteException Re) {
			log.log(Level.SEVERE, "RemoteException"+Re.getMessage());
			return false; 
		} 
	
	}

	@Override
	public void giveMeNotifier(String address,String username) {
		try{
			game.giveMeNotifier(address,username);
		}catch(RemoteException ex){
			System.err.println("Not possible to link notifier, the game will not work");
			
		}
		
	}

	@Override
	public String drawCard(int ID, String username) {
		try {
			return match.drawCard(username);
		} catch (RemoteException Re) {
			log.log(Level.SEVERE, "RemoteException"+Re.getMessage());
			return ""; 
		} 
	
	}

	@Override
	public boolean changeTurn(int ID, String username)  {
		try {
			return match.changeTurn(username);
		} catch (RemoteException Re) {
			log.log(Level.SEVERE, "RemoteException"+Re.getMessage());
			return false;
		}

	}

	@Override
	public String giveMeCharacter(int ID, String username) {
		
		try {
			return match.giveMeCharacter(username);
		} catch (RemoteException Re) {
			log.log(Level.SEVERE, "RemoteException"+Re.getMessage());
			return ""; 
		}
	}

	@Override
	public Map<String, String> showScores() {
		try {
			return game.getRanking();
		} catch (RemoteException e) {
			
			log.log(Level.SEVERE, "RemoteException"+e.getMessage());
			return null; 
		} 
	}
}
