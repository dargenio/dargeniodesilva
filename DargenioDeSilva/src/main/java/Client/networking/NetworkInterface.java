package Client.networking;


import java.util.Map;

/**
 * This interface lists all the methods used by a client during the game to communicate with server.
 * It's implemented by RMIInterface and SocketInterface classes.
 * @author Ernesto De SIlva
 *
 */
public interface NetworkInterface {

	
	boolean connect();  
	
	Map<String,String> login (String username, String password, String preferredMap);
	
	String signUp (String username, String password);
	
	Map<String,String> showScores(); 
	
	void giveMeNotifier(String address,String username);
	
	boolean setPosition(int ID,String username, String sector); 
	
	boolean attack(int ID,String username);
	
	boolean sendNoise(int ID, String username, String sector); 
	
	String drawCard(int ID, String username); 
	
	boolean changeTurn(int ID, String username); 
	
	String giveMeCharacter(int ID, String username); 
	

}
