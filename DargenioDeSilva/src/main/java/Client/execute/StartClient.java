package Client.execute;

import Client.login.view.*;
import Client.main.view.MapView;
import Client.controller.*;

public class StartClient extends Thread{
 	ChoiceInterfaceView interfaceChoice;
 	LoginView loginView; 
 	ScoresView scoresView; 
 	MapView mapView;
 	GameController gameController;
 	StartGameController loginController;
 	
 	@Override
 	public void run(){
 		interfaceChoice=new ChoiceInterfaceView(); 
		loginView= new LoginView(); 
		gameController=GameController.getInstance();
		loginController=StartGameController.getInstance(); 
		
		loginController.setStartGameController(interfaceChoice, loginView, gameController);
		
 	}
	
	public static void main(String[] args) {
		
		StartClient client=new StartClient();
		client.start();
	}
	
	
}
