package Client.main.view;

import java.awt.*;

import javax.swing.*;



/**
 * 
 * Class that initializes a matrix of string and calls method in HexMechClass 
 * to draw the hexagonal map. 
 *
 */
public class DrawingPanel extends JPanel{
	
	//constants and global variables
	
	private static final long serialVersionUID = 8038543877198003586L;
	public final static int ROWNUM = 14; 
	public final static int COLUMNNUM = 23;
	private final int HEXSIZE = 37;	//hex size in pixels
	private final  int BORDERS = 18;  
	private Hexmech hexMech=new Hexmech(); 
	
	/*public Hexmech getHexMech() {
		return hexMech;
	}
*/

	String[][] board = new String[COLUMNNUM][ROWNUM];
	
	public DrawingPanel(){	
		setOpaque(false);
	}
	
	 	void initMap(){
 
		Hexmech.setXYasVertex(false); 
 
		Hexmech.setHeight(HEXSIZE); 
		Hexmech.setBorders(BORDERS);
 
		
		//fill the String board with all coordinates (from A1 to W14) 
		for (int i=0; i<COLUMNNUM;i++) {
			for (int j=0; j<ROWNUM; j++) {
				String coordinate=(char)(i+65)+Integer.toString(j+1);
				board[i][j] = coordinate; 
				}
			}
	 	}
		
	 	
		public void paintComponent(Graphics g)
		{	
			Graphics2D g2 = (Graphics2D)g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setFont(new Font("TimesRoman", Font.PLAIN, 15));
			super.paintComponent(g2);
			
			for (int i=0;i<COLUMNNUM;i++) {
				for (int j=0;j<ROWNUM;j++) {
					hexMech.drawHex(i,j,g2); //draw hexagons 
				}
			}	
			
			
			for (int i=0;i<COLUMNNUM;i++) {
				for (int j=0;j<ROWNUM;j++) {					
					hexMech.fillHex(i,j,board[i][j],g2); //write coordinates in all sectors
				}
			}
 
		}
 
		
	} 


