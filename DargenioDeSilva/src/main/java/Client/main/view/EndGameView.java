package Client.main.view;


import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * @author Ernesto De Silva
 * Class containing graphic elements of the view containing the winners 
 * and all movements of the match.
 *
 */
public class EndGameView extends JFrame {
	
	private static final long serialVersionUID = 5773653243154890380L;
	private JFrame frame = new JFrame("Match Terminated"); 
	private Container container=new Container(); 	
	private JLabel bg=new JLabel(new ImageIcon("images/bgWinners.jpg"));
	private JComboBox<String> movementsBox=new JComboBox<String>(); 
	private JTextField[] movementsTextField=new JTextField[39]; 
	private JTextField[] fieldsInView=new JTextField[5]; 
	private JLabel[] labelsWinners=new JLabel[4];
	private int numTurns; 
	private JButton playAgainButton=new JButton(new ImageIcon("images/playAgainButton.png")); 
	private JButton scoresButton=new JButton(new ImageIcon("images/highScoresButton.png"));
	
	
	public EndGameView(){
		
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.setBounds(0,0,1050,788);
		frame.setResizable(false);
		frame.setVisible(false);
		
		bg.setBounds(0, 0, 1050, 788); //background of the view
		
		playAgainButton.setBounds(550, 190, 60,60);
		playAgainButton.setOpaque(false);
		playAgainButton.setContentAreaFilled(false);
		playAgainButton.setBorderPainted(false);
		
		scoresButton.setBounds(650, 190, 60, 60); 
		scoresButton.setOpaque(false);
		scoresButton.setContentAreaFilled(false);
		scoresButton.setBorderPainted(false);
		
		
		int startMovementsCoord=350; 
		for (int i=0; i<5; i++){
			
			fieldsInView[i]=new JTextField(); //text fields containing the movements of one turn
			fieldsInView[i].setBounds(50, startMovementsCoord, 950, 40);
			fieldsInView[i].setVisible(false);
			fieldsInView[i].setBackground(new Color(0,0,0,0));
			fieldsInView[i].setOpaque(false);
			fieldsInView[i].setForeground(Color.WHITE);
			fieldsInView[i].setEditable(false); 
			container.add(fieldsInView[i]);
			
			startMovementsCoord+=30;
		}
		
		int startLabelsCoord=150;
		for(int i=0; i<4; i++){			
			labelsWinners[i]=new JLabel(); //label containing the name of the winners 
			labelsWinners[i].setBounds(300, startLabelsCoord, 500, 40);
			labelsWinners[i].setVisible(false);
			labelsWinners[i].setForeground(Color.YELLOW);
			labelsWinners[i].setFont(new Font("Bold", Font.BOLD,20));
			container.add(labelsWinners[i]);
			
			startLabelsCoord+=50;
		}
		container.add(playAgainButton);
		container.add(scoresButton);
		container.add(movementsBox);
		container.setLayout(null);
		container.setBounds(0,0, 900,675);
		container.add(bg);
		frame.add(container);
		
		
	}
	 
	/**
	 * Set the text of labels with winners' usernames.
	 */
	public void loadWinners(final List<String> winners){
		SwingUtilities.invokeLater(new Runnable() {
			public void run(){
				int i=0; 
				for (String winner:winners){
					labelsWinners[i].setText(winner);
					labelsWinners[i].setVisible(true);
					i++; 
			}	
			}
			}); 
		}
	
	
	/**
	 * Set the text of the text fields with the movements of this match.
	 * The params indicate the interval of turns of wich show the movements.
	 * @param from
	 * @param to
	 */
	public void showMovements(int from, int to){
		int j;  
		for(j=0; j<5; j++) {
			fieldsInView[j].setVisible(false);
		}
		j=0; 
		for (int i=from-1; i<to; i++) {
			fieldsInView[j].setText(movementsTextField[i].getText());
			fieldsInView[j].setVisible(true);
			j++;
		}
		
	}

	/**
	 * Load the movements of all the turn of the match.
	 * Each turn is loaded in a text field of the array.
	 * @param movements, all movements of the match
	 */
	public void loadMovements( Map<String,String> movements){
			int i=1; 
			while(movements.get("turn"+Integer.toString(i))!=null) {
				String listMov=movements.get("turn"+Integer.toString(i));
				movementsTextField[i-1]=new JTextField(); 
				movementsTextField[i-1].setText("Turn"+Integer.toString(i)+": "+listMov);
				i++;  		
			}
			this.numTurns=i-1;
			int numIntervals=numTurns/5; 
			int lastIndex=numTurns%5; 
			int j=1; 
			for (i=1; i<=numIntervals; i++){
				movementsBox.addItem(Integer.toString(j)+"-"+Integer.toString(j+4));
				j+=5;
				}
			if (lastIndex!=0)
				movementsBox.addItem(Integer.toString(j)+"-"+Integer.toString(j-1+lastIndex));
			
			movementsBox.setBackground(Color.WHITE);
			movementsBox.setBounds(600, 300,100,30);
			
			
	}
	
	public void addScoresButtonListener(ActionListener listenerForScoresButton){
		scoresButton.addActionListener(listenerForScoresButton);
	}
	
	public void addMovementsBoxListener(ActionListener listenerForMovementsBox) {
		movementsBox.addActionListener(listenerForMovementsBox);
	}
	
	public void addRestartGameButtonListener(ActionListener listenerForRestartButton){
		playAgainButton.addActionListener(listenerForRestartButton);
	}
	
	public String getSelectedInterval(){
		return (String) movementsBox.getSelectedItem();	
		}

	
	public void setVisible(){
		frame.setVisible(true);
	}
	
	public void setInvisible(){
		frame.setVisible(false); 
	}
}
	