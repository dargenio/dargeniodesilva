package Client.main.view;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.*;


/**
 * @author Ernesto De Silva
 * Class containing graphic elements of the main game view 
 * 
 */
public class MapView extends JFrame {
	
	private static final long serialVersionUID = 5863073037949684585L;
	private JFrame frame = new JFrame("Escape from Aliens"); 
	private Container container=new Container(); 
	private DrawingPanel map;
	private JLabel bgMap=new JLabel(new ImageIcon("images/bgMap.jpg")); //background of this view
	private JTextField[] users=new JTextField[8]; //array of text fields containing usernames
	private JTextField[] messages=new JTextField[8]; //array of text fields containing messages
	private JTextField[] staticMessages=new JTextField[8]; //array of text fields containing static messages
	private JOptionPane messagePopUp=new JOptionPane();
	private JButton attackButton=new JButton(new ImageIcon("images/bomb.png"));
	private JButton shiftTurnButton=new JButton(new ImageIcon("images/changeTurnIcon.png"));
	private JButton drawCardButton=new JButton(new ImageIcon("images/drawCardButton.png"));
	private JLabel youAre=new JLabel(); //label containing the role of the player (human/alien)
	private JLabel labelMessage=new JLabel(); 
	private JLabel alienIcon=new JLabel(new ImageIcon("images/alienIcon.png")); 
	private JLabel humanIcon=new JLabel(new ImageIcon("images/humanIcon.png"));

	
	/***************************Adding and positioning elements in the view*****************************/
	public MapView() {

		map=new DrawingPanel();
		map.initMap();
		map.setBounds(280,110,750,560);
		map.setVisible(true);
		container.add(alienIcon);
		container.add(humanIcon); 
		container.add(map);
		map.setVisible(true);

		for (int i=0;i<8;i++){
			users[i]=new JTextField(20);
			users[i].setEditable(false);
			users[i].setBackground(new Color(0, 0, 0, 0));
			users[i].setForeground(Color.WHITE);
			users[i].setOpaque(false);
			users[i].setBorder(null);
			messages[i]=new JTextField(20);
			messages[i].setEditable(false);
			messages[i].setBackground(new Color(0, 0, 0, 0));
			messages[i].setForeground(Color.WHITE);
			messages[i].setOpaque(false);
			messages[i].setBorder(null);
			staticMessages[i]=new JTextField(10); 
			staticMessages[i].setEditable(false);
			staticMessages[i].setBackground(new Color(0,0,0,0));
			staticMessages[i].setForeground(Color.WHITE);
			staticMessages[i].setBorder(null);

		}	

		youAre.setBounds(20, 310, 180,40); 
		youAre.setForeground(new Color(42,121,225));
		youAre.setFont(new Font("Bold", Font.BOLD,14));
		youAre.setVisible(true);

		labelMessage.setBounds(27,430,300,40);
		labelMessage.setForeground(new Color(42,121,225));
		labelMessage.setFont(new Font("Bold", Font.BOLD,12));
		labelMessage.setVisible(true);
		labelMessage.setSize(100,50);

		users[0].setBounds(17,353,160,40);
		staticMessages[0].setBounds(183, 353, 100, 40);
		messages[0].setBounds(17, 393, 260, 40);
		users[0].setVisible(true);
		messages[0].setVisible(true);

		users[1].setBounds(17,193,160,40);
		staticMessages[1].setBounds(183, 193, 100, 40);
		messages[1].setBounds(17,233,260,40);
		users[1].setVisible(true);
		messages[1].setVisible(true);

		users[2].setBounds(17, 33, 160, 40);
		staticMessages[2].setBounds(183, 33, 100, 40);
		messages[2].setBounds(17, 73, 260, 40);
		users[2].setVisible(true);
		messages[2].setVisible(true); 

		users[3].setBounds(350,33,160,40);
		staticMessages[3].setBounds(515, 33, 100, 40);
		messages[3].setBounds(350,73,260,40);
		users[3].setVisible(true);
		messages[3].setVisible(true);

		users[4].setBounds(681,33,160,40);
		staticMessages[4].setBounds(846, 33, 100, 40);
		messages[4].setBounds(681,73,260,40);
		users[4].setVisible(true);
		messages[4].setVisible(true);

		users[5].setBounds(1030,33,160,40);
		staticMessages[5].setBounds(1195, 33, 100, 40);
		messages[5].setBounds(1030, 73, 260, 40);
		users[5].setVisible(true);
		messages[5].setVisible(true);

		users[6].setBounds(1030,193,160,40);
		staticMessages[6].setBounds(1195, 193, 100, 40);
		messages[6].setBounds(1030,233,245,40);
		users[6].setVisible(true);
		messages[6].setVisible(true);

		users[7].setBounds(1030,353,160,40);
		staticMessages[7].setBounds(1195, 353, 100, 40);
		messages[7].setBounds(1030,393,245,40);
		users[7].setVisible(true);
		messages[7].setVisible(true);

		attackButton.setBounds(20, 460, 60, 60);
		attackButton.setVisible(false);
		attackButton.setEnabled(false);
		attackButton.setOpaque(false);
		attackButton.setContentAreaFilled(false);
		attackButton.setBorderPainted(false);

		drawCardButton.setBounds(100, 460, 60, 60);
		drawCardButton.setVisible(true);
		drawCardButton.setEnabled(false); 
		drawCardButton.setBackground(Color.BLACK);
		drawCardButton.setOpaque(false);
		drawCardButton.setContentAreaFilled(false);
		drawCardButton.setBorderPainted(false);

		shiftTurnButton.setBounds(180, 460, 60, 60);
		shiftTurnButton.setVisible(true);
		shiftTurnButton.setEnabled(false);
		shiftTurnButton.setOpaque(false);
		shiftTurnButton.setContentAreaFilled(false);
		shiftTurnButton.setBorderPainted(false);

	//	startAudio.setBounds(17,360,30,30);
	//	startAudio.setVisible(true);

		bgMap.setBounds(0,0,1280,720);
		bgMap.setVisible(true);

		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.setBounds(0,0,1280,720);
		frame.setResizable(false);
		frame.setVisible(true);

		container.setLayout(null);
		container.setVisible(true);
		container.setBounds(0,0, 1280,720);

		for (int i=0; i<8; i++) {
			container.add(users[i]);
			container.add(messages[i]);
			container.add(staticMessages[i]); 
		}

		//container.add(startAudio);
		container.add(shiftTurnButton);
		container.add(drawCardButton);
		container.add(labelMessage);
		container.add(youAre); 
		container.add(messagePopUp); 
		container.add(attackButton);
		container.add(bgMap);


		frame.add(container);

		frame.setResizable(false);
		frame.setVisible(true); 
		
		
	}
	
	/*******************************************End method*************************************************/

	
	
	/*************************************Adding ActionListener method**************************************/
	public void addDrawCardButtonListener(ActionListener listenForDrawCardButton){
		drawCardButton.addActionListener(listenForDrawCardButton); 
	}

	public void addAttackButtonListener (ActionListener listenForAttackButton){
		attackButton.addActionListener(listenForAttackButton);
	}

	public void addShiftTurnButtonListener(ActionListener listenForShiftTurnButtonButton){
		shiftTurnButton.addActionListener(listenForShiftTurnButtonButton);
	}
	
	/*public void addStartAudioButtonListener(ActionListener listenForStartAudioButton){
		startAudio.addActionListener(listenForStartAudioButton);
	}*/

	public void addSectorListener (MouseListener listenForMapSector){
		map.addMouseListener(listenForMapSector);
	}
	
	/*************************************End ActionListener method****************************************/
	
	
	
	/********************************Methods to modify the element in this view****************************/
	
	public void setInvisible(){
		frame.setVisible(false);
	}

	public void setVisible(){
		frame.setVisible (true); 
	}
	
	public void setEnabledAttackButton(final boolean status){
		java.awt.EventQueue.invokeLater(new Runnable()  {
			public void run() {
				attackButton.setEnabled(status);
			}
		});

	}
	public void setEnabledShiftTurnButton(final boolean status){
		java.awt.EventQueue.invokeLater(new Runnable(){
			public void run(){
				shiftTurnButton.setEnabled(status);
			}
		});

	}

	public void setEnabledDrawCardButton(final boolean status){
		java.awt.EventQueue.invokeLater(new Runnable(){
			public void run(){
				drawCardButton.setEnabled(status);
			}
		});
	}

	public void setVisibleAttackButton(final boolean status) {
		java.awt.EventQueue.invokeLater(new Runnable()  {
			public void run() {
				attackButton.setVisible(status);

			}
		});

	}
	public void setLabelMessage(final String message){
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				labelMessage.setText(message);
				labelMessage.setSize(labelMessage.getPreferredSize());
			}
		});
	}

	public void setTypeCharachter(String type){
		this.youAre.setText("YOU ARE: " +type.toUpperCase());
	}

	public void showIcon(final int xCoord, final int yCoord, final boolean human) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run(){	

				if (xCoord%2==0) {
					if (human==false)
						alienIcon.setBounds(37+(xCoord*31)+265, 34+(yCoord*37)+95, 35, 35); 
					else 
						humanIcon.setBounds(37+(xCoord*31)+265, 34+(yCoord*37)+95, 35, 35); 
				}

				else {
					if (human==false) {
						alienIcon.setBounds(69+(xCoord-1)*31+265, 52+(yCoord*37)+95, 35, 35); 
						alienIcon.setVisible(true);
					}
					else {
						humanIcon.setBounds(69+(xCoord-1)*31+265, 52+(yCoord*37)+95, 35, 35);  
						humanIcon.setVisible(true); 
					}
				}
			}
		});	
	}

	/**
	 * Create a popUp containing the parameterized message
	 * @param message
	 */
	public void displayMessage(String message){

		JOptionPane.showMessageDialog(this, message);

	}
	
	/******************************End methods to modify the view***************************************/
	
	
	
	/******************************************Getters**************************************************/
	public JTextField[] getUsers() {
		return users;
	}

	public JTextField[] getMessages() {
		return messages;
	}

	public JTextField[] getStaticMessages(){
		return staticMessages; 
	}

	/******************************************End Getters**************************************************/
	
}
